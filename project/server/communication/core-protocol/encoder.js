
'use strict';

const EncoderConst = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/encoder-const');


class Encoder {
  static GuidSize = EncoderConst.GuidSize;
  static Int8Size = EncoderConst.Int8Size;
  static Int16Size = EncoderConst.Int16Size;
  static Int32Size = EncoderConst.Int32Size;
  static Int64Size = EncoderConst.Int64Size;
  static Uint8Size = EncoderConst.Uint8Size;
  static Uint16Size = EncoderConst.Uint16Size;
  static Uint32Size = EncoderConst.Uint32Size;
  static Uint64Size = EncoderConst.Uint64Size;
  static Float32Size = EncoderConst.Float32Size;
  static Float64Size = EncoderConst.Float64Size;
  static CtSize = EncoderConst.CtSize;
  static Uint8ArraySize = EncoderConst.Uint8ArraySize;
  
  
  static ASCII = [
    -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1,
     0,  1,  2,  3,  4,  5,  6,  7,
     8,  9, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1,
    -1, 10, 11, 12, 13, 14, 15, -1
  ];
  
  constructor(textCache) {
    this.textCache = textCache;
    this.buffer = null;
    this.offset = 0;
    this.doCalculate = false;
  }
  
  clear() {
    this.buffer = null;
    this.offset = 0;
  }
  
  createBuffer(size) {
    this.buffer = Buffer.allocUnsafe(size);
    return this.buffer;
  }
  
  calculate(doCalculate) {
    this.doCalculate = doCalculate;
    this.offset = 0;
  }
    
  calculateBytesFromString(s) {
    if(!s) {
      return 0;
    }
    return Buffer.byteLength(s);
  }
  
  calculateDynamicBytes(size) {
    if(size <= 253) {
      return 1;
    }
    else if(size <= 65535) {
      return 3;
    }
    else {
      return 5;
    }
  }
  
  calculateBytesFromBin(b) {
    return b.byteLength;
  }
  
  getStringBytes(s) {
    const size = this.calculateBytesFromString(s);
    return size + this.calculateDynamicBytes(size);
  }
  
  getStringArrayBytes(array) {
    let size = this.calculateDynamicBytes(array.length);
    for(let i = 0; i < array.length; ++i) {
      size += this.getStringBytes(array[i]);
    }
    return size;
  }
  
  getUint8ArrayBytes(array) {
    let size = 2; // TODO: use dyamic bytes - this.calculateDynamicBytes(array.length);
    size += array.length;
    return size;
  }
  
  getGuidArrayBytes(array) {
    return this.calculateDynamicBytes(array.length) + (array.length * EncoderConst.GuidSize);
  }
  
  setDynamicBytes(size) {
    const sizeBytes = this.calculateDynamicBytes(size);
    if(1 === sizeBytes) {
      this.setUint8(size);
    }
    else if(3 === sizeBytes) {
      this.setUint8(254);
      this.setUint16(size);
    }
    else {
      this.setUint8(255);
      this.setUint32(size);
    }
  }
  
  setInt8(value) {
    if(!this.doCalculate) {
      this.buffer.writeInt8(value, this.offset);
    }
    this.offset += Encoder.Int8Size;
  }
  
  setInt16(value) {
    if(!this.doCalculate) {
      this.buffer.writeInt16BE(value, this.offset);
    }
    this.offset += Encoder.Int16Size;
  }
  
  setInt32(value) {
    if(!this.doCalculate) {
      this.buffer.writeInt32BE(value, this.offset);
    }
    this.offset += Encoder.Int32Size;
  }
  
  setBigInt64(value) {
    if(!this.doCalculate) {
      this.buffer.writeBigInt64BE(value, this.offset);
    }
    this.offset += Encoder.Int64Size;
  }
  
  setUint1_0(value, ready) {
    if(!this.doCalculate) {
      const v = 0x7f & this.buffer.readUInt8(this.offset);
      this.buffer.writeUInt8(v + ((0x1 & value) << 7), this.offset);
      if(ready) {
      }
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint1_1(value, ready) {
    if(!this.doCalculate) {
      const v = 0xbf & this.buffer.readUInt8(this.offset);
      this.buffer.writeUInt8(v + ((0x1 & value) << 6), this.offset);
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint1_2(value, ready) {
    if(!this.doCalculate) {
      const v = 0xdf & this.buffer.readUInt8(this.offset);
      this.buffer.writeUInt8(v + ((0x1 & value) << 5), this.offset);
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint1_3(value, ready) {
    if(!this.doCalculate) {
      const v = 0xef & this.buffer.readUInt8(this.offset);
      this.buffer.writeUInt8(v + ((0x1 & value) << 4), this.offset);
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint1_4(value, ready) {
    if(!this.doCalculate) {
      const v = 0xf7 & this.buffer.readUInt8(this.offset);
      this.buffer.writeUInt8(v + ((0x1 & value) << 3), this.offset);
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint1_5(value, ready) {
    if(!this.doCalculate) {
      const v = 0xfb & this.buffer.readUInt8(this.offset);
      this.buffer.writeUInt8(v + ((0x1 & value) << 2), this.offset);
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint1_6(value, ready) {
    if(!this.doCalculate) {
      const v = 0xfd & this.buffer.readUInt8(this.offset);
      this.buffer.writeUInt8(v + ((0x1 & value) << 1), this.offset);
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint1_7(value, ready) {
    if(!this.doCalculate) {
      const v = 0xfe & this.buffer.readUInt8(this.offset);
      this.buffer.writeUInt8(v + (0x1 & value), this.offset);
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint2_0(value, ready) {
    if(!this.doCalculate) {
      const v = 0x3f & this.buffer.readUInt8(this.offset);
      this.buffer.writeUInt8(v + ((0x3 & value) << 6), this.offset);
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint2_1(value, ready) {
    if(!this.doCalculate) {
      const v =  0xcf & this.buffer.readUInt8(this.offset);
      this.buffer.writeUInt8(v + ((0x3 & value) << 4), this.offset);
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint2_2(value, ready) {
    if(!this.doCalculate) {
      const v = 0xf3 & this.buffer.readUInt8(this.offset);
      this.buffer.writeUInt8(v + ((0x3 & value) << 2), this.offset);
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint2_3(value, ready) {
    if(!this.doCalculate) {
      const v = 0xfc & this.buffer.readUInt8(this.offset);
      this.buffer.writeUInt8(v + (0x3 & value), this.offset);
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint4_0(value, ready) {
    if(!this.doCalculate) {
      const v = 0xf & this.buffer.readUInt8(this.offset);
      this.buffer.writeUInt8(v + ((0xf & value) << 4), this.offset);
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint4_1(value, ready) {
    if(!this.doCalculate) {
      const v = 0xf0 & this.buffer.readUInt8(this.offset);
      this.buffer.writeUInt8(v + (0xf & value), this.offset);
    }
    if(ready) {
      this.offset += Encoder.Int8Size;
    }
  }
  
  setUint8(value) {
    if(!this.doCalculate) {
      this.buffer.writeUInt8(value, this.offset);
    }
    this.offset += Encoder.Uint8Size;
  }
  
  setUint16(value) {
    if(!this.doCalculate) {
      this.buffer.writeUInt16BE(value, this.offset);
    }
    this.offset += Encoder.Uint16Size;
  }
  
  setUint32(value) {
    if(!this.doCalculate) {
      this.buffer.writeUInt32BE(value, this.offset);
    }
    this.offset += Encoder.Uint32Size;
  }
  
  setBigUint64(value) {
    if(!this.doCalculate) {
      this.buffer.writeBigUInt64BE(value, this.offset);
    }
    this.offset += Encoder.Uint64Size;
  }
  
  setFloat32(value) {
    if(!this.doCalculate) {
      this.buffer.writeFloatBE(value, this.offset);
    }
    this.offset += Encoder.Float32Size;
  }
  
  setFloat64(value) {
    if(!this.doCalculate) {
      this.buffer.writeDoubleBE(value, this.offset);
    }
    this.offset += Encoder.Float364Size;
  }
  
  setBool1_0(value, ready) {
    this.setUint1_0(value ? 0x1 : 0x0, ready);
  }
  
  setBool1_1(value, ready) {
    this.setUint1_1(value ? 0x1 : 0x0, ready);
  }
  
  setBool1_2(value, ready) {
    this.setUint1_2(value ? 0x1 : 0x0, ready);
  }
  
  setBool1_3(value, ready) {
    this.setUint1_3(value ? 0x1 : 0x0, ready);
  }
  
  setBool1_4(value, ready) {
    this.setUint1_4(value ? 0x1 : 0x0, ready);
  }
  
  setBool1_5(value, ready) {
    this.setUint1_5(value ? 0x1 : 0x0, ready);
  }
  
  setBool1_6(value, ready) {
    this.setUint1_6(value ? 0x1 : 0x0, ready);
  }
  
  setBool1_7(value, ready) {
    this.setUint1_7(value ? 0x1 : 0x0, ready);
  }
  
  _setCt(value) {
    if(!this.doCalculate) {
      this.buffer.writeInt16BE(value, this.offset);
    }
    this.offset += Encoder.CtSize;
  }
  
  setCtString(text, cachedTexts) {
    if(!this.doCalculate) {
      if(!text) {
        text = '';
      }
      const ctId = this.textCache.setExternal(text, cachedTexts);
      this._setCt(ctId);
    }
    else {
      this.offset += Encoder.CtSize;
    }
  }
  
  setCtStringArray(array, cachedTexts) {
    this.setUint16(array.length);
    for(let i = 0; i < array.length; ++i) {
      this.setCtString(array[i], cachedTexts);
    }
  }
  
  setCtStringInternal(text) {
    if(!this.doCalculate) {
      const ctId = this.textCache.setInternal(text);
      this._setCt(ctId);
    }
    else {
      this.offset += Encoder.CtSize;
    }
  }
  
  setCtStringArrayInternal(array) {
    this.setUint16(array.length);
    for(let i = 0; i < array.length; ++i) {
      this.setCtStringInternal(array[i]);
    }
  }
  
  setRawString(value, size) {
    if(!this.doCalculate) {
      if(0 !== size) {
        this.offset += this.buffer.write(value, this.offset, size);
      }
    }
    else {
      this.offset += size;
    }
  }
  
  setString(value) {
    const stringBytes = this.calculateBytesFromString(value);
    this.setDynamicBytes(stringBytes);
    this.setRawString(value, stringBytes);
  }
  
  setStringArray(array) {
    this.setDynamicBytes(array.length);
    for(let i = 0; i < array.length; ++i) {
      this.setString(array[i]);
    }
  }
  
  setRawBinary(value, size) {
    if(!this.doCalculate) {
      this.offset += value.copy(this.buffer, this.offset, 0, size);
    }
    else {
      this.offset += size;
    }
  }
  
  setBinary(value) {
    const binaryBytes = value.byteLength;
    this.setDynamicBytes(binaryBytes);
    this.setRawBinary(value, binaryBytes);
  }
  
  setUint8Array(array) {
    this.setUint16(array.length);
    for(let i = 0; i < array.length; ++i) {
      this.setUint8(array[i]);
    }
  }
  
  setGuid(value) {
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(0)]);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(1)], true);
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(2)]);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(3)], true);
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(4)]);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(5)], true);
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(6)]);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(7)], true);
    
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(9)]);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(10)], true);
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(11)]);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(12)], true);
    
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(14)]);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(15)], true);
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(16)]);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(17)], true);
        
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(19)]);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(20)], true);
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(21)]);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(22)], true);
    
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(24)]);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(25)], true);
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(26)]);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(27)], true);
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(28)]);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(29)], true);
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(30)]);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(31)], true);
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(32)]);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(33)], true);
    this.setUint4_0(Encoder.ASCII[value.charCodeAt(34)]);
    this.setUint4_1(Encoder.ASCII[value.charCodeAt(35)], true);
  }
  
  setGuidArray(array) {
    this.setDynamicBytes(array.length);
    for(let i = 0; i < array.length; ++i) {
      this.setGuid(array[i]);
    }
  }
}


module.exports = Encoder;
