
'use strict';


class Decoder {
  static GuidSize = 16;
  static guidLookup = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102];
  
  constructor(textCache) {
    this.type = 'server';
    this.textCache = textCache;
    this.buffers = [];
    this.buffer = null;
    this.offset = 0;
  }
  
  inc(i) {
    this.offset += i;
  }
  
  guidSize() {
    return Decoder.GuidSize;
  }
  
  clear() {
    this.offset = 0;
    this.buffers = [];
    this.buffer = null;
  }
  
  add(buffer) {
    if(buffer) {
      this.buffers.push(buffer);
    }
  }
  
  parse(jsonFunc) {
    try {
      const data = JSON.parse(this.buffer, jsonFunc);
      this.clear();
      return data;
    }
    catch(err) {
      return null;
    }
  }
  
  has(size) {
    if(!this.buffer) {
      if(0 !== this.buffers.length) {
        this.buffer = this.buffers.shift();
      }
      else {
        return false;
      }
    }
    return size <= this.buffer.length - this.offset;
  }
  
  got() {
    if(this.buffer.length === this.offset) {
      this.offset = 0;
      this.buffer = null;
    }
  }
  
  calculateDynamicBytes() {
    const size = this.buffer.readUInt8(this.offset);
    if(size <= 253) {
      return 1;
    }
    else if(size <= 65535) {
      return 3;
    }
    else {
      return 5;
    }
  }
  
  getDynamicBytes() {
    const size = this.getUint8();
    if(size <= 253) {
      return size;
    }
    if(254 === size) {
      return this.getUint16();
    }
    else if(255 === size) {
      return this.getUint32();
    }
  }
  
  getInt8() {
    const value = this.buffer.readInt8(this.offset);
    this.offset += 1;
    return value;
  }
  
  getInt16() {
    const value = this.buffer.readInt16BE(this.offset);
    this.offset += 2;
    return value;
  }
  
  getInt32() {
    const value = this.buffer.readInt32BE(this.offset);
    this.offset += 4;
    return value;
  }
  
  getBigInt64() {
    const value = this.buffer.readBigUInt64BE(this.offset);
    this.offset += 8;
    return value;
  }
  
  getUint1_0(ready) {
    const value = this.buffer.readUInt8(this.offset);
    if(ready) {
      this.offset += 1;
    }
    return (0x1 & (value >> 7));
  }
  
  getUint1_1(ready) {
    const value = this.buffer.readUInt8(this.offset);
    if(ready) {
      this.offset += 1;
    }
    return (0x1 & (value >> 6));
  }
  
  getUint1_2(ready) {
    const value = this.buffer.readUInt8(this.offset);
    if(ready) {
      this.offset += 1;
    }
    return (0x1 & (value >> 5));
  }
  
  getUint1_3(ready) {
    const value = this.buffer.readUInt8(this.offset);
    if(ready) {
      this.offset += 1;
    }
    return (0x1 & (value >> 4));
  }
  
  getUint1_4(ready) {
    const value = this.buffer.readUInt8(this.offset);
    if(ready) {
      this.offset += 1;
    }
    return (0x1 & (value >> 3));
  }
  
  getUint1_5(ready) {
    const value = this.buffer.readUInt8(this.offset);
    if(ready) {
      this.offset += 1;
    }
    return (0x1 & (value >> 2));
  }
  
  getUint1_6(ready) {
    const value = this.buffer.readUInt8(this.offset);
    if(ready) {
      this.offset += 1;
    }
    return (0x1 & (value >> 1));
  }
  
  getUint1_7(ready) {
    const value = this.buffer.readUInt8(this.offset);
    if(ready) {
      this.offset += 1;
    }
    return (0x1 & value);
  }
  
  getUint2_0(ready) {
    const value = this.buffer.readUInt8(this.offset);
    if(ready) {
      this.offset += 1;
    }
    return (0x3 & (value >> 6));
  }
  
  getUint2_1(ready) {
    const value = this.buffer.readUInt8(this.offset);
    if(ready) {
      this.offset += 1;
    }
    return (0x3 & (value >> 4));
  }
  
  getUint2_2(ready) {
    const value = this.buffer.readUInt8(this.offset);
    if(ready) {
      this.offset += 1;
    }
    return (0x3 & (value >> 2));
  }
  
  getUint2_3(ready) {
    const value = this.buffer.readUInt8(this.offset);
    if(ready) {
      this.offset += 1;
    }
    return (0x3 & value);
  }
  
  getUint4_0(ready) {
    const value = this.buffer.readUInt8(this.offset);
    if(ready) {
      this.offset += 1;
    }
    return (0xf & (value >> 4));
  }
  
  getUint4_1() {
    const value = this.buffer.readUInt8(this.offset);
    this.offset += 1;
    return 0xf & value;
  }
  
  getUint8() {
    const value = this.buffer.readUInt8(this.offset);
    this.offset += 1;
    return value;
  }
  
  getUint16() {
    const value = this.buffer.readUInt16BE(this.offset);
    this.offset += 2;
    return value;
  }
  
  getUint32() {
    const value = this.buffer.readUInt32BE(this.offset);
    this.offset += 4;
    return value;
  }
  
  getBigUint64() {
    const value = this.buffer.readBigUInt64BE(this.offset);
    this.offset += 8;
    return value;
  }
  
  getFloat32() {
    const value = this.buffer.readFloatBE(this.offset);
    this.offset += 4;
    return value;
  }
  
  getFloat64() {
    const value = this.buffer.readDoubleBE(this.offset);
    this.offset += 8;
    return value;
  }
  
  getBool1_0(ready) {
    return !!this.getUint1_0(ready);
  }
  
  getBool1_1(ready) {
    return !!this.getUint1_1(ready);
  }
  
  getBool1_2(ready) {
    return !!this.getUint1_2(ready);
  }
  
  getBool1_3(ready) {
    return !!this.getUint1_3(ready);
  }
  
  getBool1_4(ready) {
    return !!this.getUint1_4(ready);
  }
  
  getBool1_5(ready) {
    return !!this.getUint1_5(ready);
  }
  
  getBool1_6(ready) {
    return !!this.getUint1_6(ready);
  }
  
  getBool1_7(ready) {
    return !!this.getUint1_7(ready);
  }
  
  getCt() {
    const value = this.getInt16();
    return value;
  }
  
  getCtString() {
    const valueCt = this.getCt();
    return this.textCache.getText(valueCt);
  }
  
  getCtStringArray() {
    const strings = [];
    const nbrStrings = this.getUint16();
    for(let i = 0; i < nbrStrings; ++i) {
      const valueCt = this.getCt();
      strings.push(this.textCache.getText(valueCt));
    }
    return strings;
  }
  
  getSizedString(size) {
    if(0 === size) {
      return '';
    }
    else {
      const offset = this.offset;
      this.offset += size;
      return this.buffer.toString('utf8', offset, this.offset);
    }
  }
  
  getString() {
    const size = this.getDynamicBytes();
    return this.getSizedString(size);
  }
  
  getStringArray() {
    const size = this.getDynamicBytes();
    const strings = [];
    for(let i = 0; i < size; ++i) {
      strings.push(this.getString());
    }
    return strings;
  }
  
  getSizedBinary(size) {
    if(0 === size) {
      return new Uint8Array(0);
    }
    else {
      const offset = this.offset;
      this.offset += size;
      const array = this.buffer.subarray(offset, this.offset);
      return array;
    }
  }
  
  getBinary() {
    const size = this.getDynamicBytes();
    return this.getSizedBinary(size);
  }
  
  getUint8Array(size) {
    if(undefined === size) {
      size = this.getUint16();
    }
    return this.getSizedBinary(size);
  }
  
  getGuid() {
    const r = new Array(36);
    r[8] = 45;
	  r[13] = 45;
	  r[18] = 45;
	  r[23] = 45;
    for(let i = 0; i < 8; i+=2) {
      r[i] = Decoder.guidLookup[this.getUint4_0()];
      r[i+1] = Decoder.guidLookup[this.getUint4_1(true)];
    }
    for(let i = 9; i < 13; i+=2) {
      r[i] = Decoder.guidLookup[this.getUint4_0()];
      r[i+1] = Decoder.guidLookup[this.getUint4_1(true)];
    }
    for(let i = 14; i < 18; i+=2) {
      r[i] = Decoder.guidLookup[this.getUint4_0()];
      r[i+1] = Decoder.guidLookup[this.getUint4_1(true)];
    }
    for(let i = 19; i < 23; i+=2) {
      r[i] = Decoder.guidLookup[this.getUint4_0()];
      r[i+1] = Decoder.guidLookup[this.getUint4_1(true)];
    }
    for(let i = 24; i < 36; i+=2) {
      r[i] = Decoder.guidLookup[this.getUint4_0()];
      r[i+1] = Decoder.guidLookup[this.getUint4_1(true)];
    }
    return String.fromCharCode(...r);
  }
  
  getGuidArray() {
    const size = this.getDynamicBytes();
    const guids = [];
    for(let i = 0; i < size; ++i) {
      guids.push(this.getGuid());
    }
    return guids;
  }
}


module.exports = Decoder;
