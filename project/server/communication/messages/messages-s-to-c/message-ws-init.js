
'use strict';

const CoreProtocolConst = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/core-protocol-const');


class MessageWsInit {
  constructor() {
    this.msgId = CoreProtocolConst.WS_INIT;
  }
}


module.exports = MessageWsInit;