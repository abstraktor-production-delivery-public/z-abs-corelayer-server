
'use strict';

const CoreProtocolConst = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/core-protocol-const');


class MessagePersistentPublish {
  constructor(storeName, data) {
    this.msgId = CoreProtocolConst.PERSISTENT_PUBLISH;
    this.storeName = storeName;
    this.data = data;
  }
}


module.exports = MessagePersistentPublish;
