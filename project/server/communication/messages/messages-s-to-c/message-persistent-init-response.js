
'use strict';

const CoreProtocolConst = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/core-protocol-const');


class MessagePersistentInitResponse {
  constructor() {
    this.msgId = CoreProtocolConst.PERSISTENT_INIT_RESPONSE;
  }
}


module.exports = MessagePersistentInitResponse;
