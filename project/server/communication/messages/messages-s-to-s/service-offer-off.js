
'use strict';

const CoreProtocolConst = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/core-protocol-const');


class ServiceOfferOff {
  constructor(serviceExport) {
    this.msgId = CoreProtocolConst.SERVICE_OFFER_OFF;
    this.serviceExport = serviceExport;
  }
}


module.exports = ServiceOfferOff;
