
'use strict';

const CoreProtocolConst = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/core-protocol-const');


class ServiceInitRequest {
  constructor(nodeName, nodeId, nodeType) {
    this.msgId = CoreProtocolConst.SERVICE_INIT_REQUEST;
    this.nodeName = nodeName;
    this.nodeId = nodeId;
    this.nodeType = nodeType;
  }
}


module.exports = ServiceInitRequest;
