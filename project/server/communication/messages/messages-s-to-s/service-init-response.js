
'use strict';

const CoreProtocolConst = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/core-protocol-const');


class ServiceInitResponse {
  constructor(nodeName, nodeId, nodeType, serviceExport) {
    this.msgId = CoreProtocolConst.SERVICE_INIT_RESPONSE;
    this.nodeName = nodeName;
    this.nodeId = nodeId;;
    this.nodeType = nodeType;
    this.serviceExport = serviceExport;
  }
}


module.exports = ServiceInitResponse;
