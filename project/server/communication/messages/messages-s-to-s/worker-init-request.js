
'use strict';

const CoreProtocolConst = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/core-protocol-const');


class WorkerInitRequest {
constructor(port) {
    this.msgId = CoreProtocolConst.WORKER_INIT_REQUEST;
    this.port = port;
  }
}


module.exports = WorkerInitRequest;
