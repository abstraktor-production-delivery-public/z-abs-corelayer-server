
'use strict';

const CoreProtocolConst = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/core-protocol-const');


class ServiceOfferOn {
  constructor(serviceExport) {
    this.msgId = CoreProtocolConst.SERVICE_OFFER_ON;
    this.serviceExport = serviceExport;
  }
}


module.exports = ServiceOfferOn;
