
'use strict';

const Responses = require('./responses');


class ResponseQueue {
  static id = 0;

  constructor(cb, externalCb, id) {
    this.instanceId = ++ResponseQueue.id;
    this.calls = 0;
    this.cb = cb;
    this.externalCb = externalCb;
    this.id = id;
    this.responses = new Responses(id);
    this.token = null;
  }
  
  add() {
    ++this.calls;
  }
  
  remove() {
    if(0 == --this.calls) {
      this.cb(this.responses, this.externalCb);
    }
  }
  
  setToken(token) {
    this.token = token;
  }
  
  done(response) {
    this.responses.add(response);
    if(0 === --this.calls) {
      this.responses.responses.sort((a, b) => {
        return a.index - b.index;
      });
      this.cb(this.responses, this.externalCb, this.token);
    }
  }
}


module.exports = ResponseQueue;
