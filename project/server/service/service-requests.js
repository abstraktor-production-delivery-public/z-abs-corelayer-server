
'use strict';


class ServiceRequests {
  constructor() {
    this.serviceRequests = new Map();
  }
  
  addRequest(serviceName, requestId, sessionId, pluginService, cb) {
    const requestDatas = this.serviceRequests.get(serviceName);
    if(!requestDatas) {
      this.serviceRequests.set(serviceName, new Map([[requestId, {requestId, sessionId, pluginService, cb}]]));
    }
    else {
      requestDatas.set(requestId, {requestId, sessionId, pluginService, cb});
    }
  }
  
  popRequest(serviceName, requestId) {
    const requestDatas = this.serviceRequests.get(serviceName);
    if(!requestDatas) {
      return null;
    }
    const requestData = requestDatas.get(requestId);
    requestDatas.delete(requestId);
    return requestData;
  }
  
  onExternalService(serviceName, has) {
    if(has) {
      if(!this.serviceRequests.has(serviceName)) {
        this.serviceRequests.set(serviceName, new Map());
      }
    }
    else {
      const requestDatas = this.serviceRequests.get(serviceName);
      if(requestDatas) {
        this.serviceRequests.delete(serviceName);
        requestDatas.forEach((requestData) => {
          requestData.cb(new Error('Service is down.'));
        });
      }
    }
  }
}


module.exports = ServiceRequests;
