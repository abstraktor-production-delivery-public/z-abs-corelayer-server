
'use strict';


class ServiceExport {
  static EMPTY = null;
  static {
    ServiceExport.EMPTY = new ServiceExport([], [], []);
  }
  
  constructor(frontendServices, backendServices, surveillanceServices) {
    this.frontendServices = frontendServices;
    this.backendServices = backendServices;
    this.surveillanceServices = surveillanceServices;
  }
  
  isEmpty() {
    return 0 === this.frontendServices.length && 0 === this.backendServices.length &&  0 === this.surveillanceServices.length;
  }
  
  printOfferOn(offerData, nodeName, nodeId) {
    if(0 !== this.frontendServices.length || 0 !== this.backendServices.length || 0 !== this.surveillanceServices.length) {
      const frontend = this.frontendServices.join(', ');
      const backend = this.backendServices.join(', ');
      const surveillance = this.surveillanceServices.join(', ');
      const offer = `frontend: [${frontend}], backend: [${backend}], surveillance: [${surveillance}]`;
      if(offerData.offer) {
        if(offerData.isDependent) {
          ddb.writelnTime(ddb.greenBright('          Offer sent to: '), nodeName, ddb.yellow('::'), nodeId, ' ', ddb.yellow(offerData.connectedDependencies), ' of ', ddb.yellow(offerData.dependencies), ' dependent nodes have connected. ', offer);
        }
        else {
          ddb.writelnTime(ddb.greenBright('          Offer sent to: '), nodeName, ddb.yellow('::'), nodeId, ' which is an independent node.', offer);
        }
      }
      else {
        ddb.writelnTime(ddb.yellowBright('          Offer not sent to: '), nodeName, ddb.yellow('::'), nodeId, ' ', ddb.yellow(offerData.connectedDependencies), ' of ', ddb.yellow(offerData.dependencies), ' dependent nodes have connected. ', offer);
      }
    }
  }
  
  printOfferOff(offerData, nodeName, nodeId) {
    console.log('printOfferOff');
  }
}


module.exports = ServiceExport;
