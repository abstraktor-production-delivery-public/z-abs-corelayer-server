
'use strict';

const PluginFactory = require('../plugin-factory');
const ActorPathDist = require('../path/actor-path-dist');
const ActionRequest = require('z-abs-corelayer-cs/clientServer/communication/action-request');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');
const Envelope = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/envelope');
const Path = require('path');


class PluginService {
  static STATE_NONE = 0;
  static STATE_INITIALIZING = 1;
  static STATE_RUNNING = 2;
  static STATE_EXITING = 3;
  static STATE_DONE = 4;
  
  static OFFER_LOCAL = 0;
  static OFFER_FRONTEND = 1;
  static OFFER_BACKEND = 2;
  static OFFER_SURVEILLANCE = 3;
  
  constructor(exportType) {
    this.exportType = exportType;
    this.serviceName = this.constructor.name;
    this.subscriptionName = '';
    this.dataResponse = null;
    this.serviceManager = null;
    this.serviceRequests = null;
    this.pluginComponents = [];
    this.pluginComponentParams = [this];
    this.pluginComponentFactory = new PluginFactory('component', this.pluginComponentParams);
    this.components = new Map();
    this.state = PluginService.STATE_NONE;
    this.hasException = null;
    this.cb = null;
    if(undefined === exportType || 'boolean' === typeof exportType) {
      ddb.error('Wrong export type:', exportType, this.serviceName);
    }
  }
  
  initData(dataResponse, serviceManager, serviceRequests, pluginComponents) {
    this.dataResponse = dataResponse;
    this.node = this.dataResponse.getNode();
    this.serviceManager = serviceManager;
    this.serviceRequests = serviceRequests;
    this.pluginComponents = pluginComponents;
  }
  
  init(cb) {
    this.cb = cb;
    this.state = PluginService.STATE_INITIALIZING;
    try {
      this.pluginComponentFactory.setPaths(this.pluginComponents);
      this.pluginComponentFactory.load(() => {
        if(this.hasException) {
          cb();
          return;
        }
        this.pluginComponentFactory.plugins.forEach((pluginComponent, key) => {
          const pathParts = pluginComponent.factoryPath.split(Path.sep);
          if(pathParts && 0 !== pathParts.length && pathParts[pathParts.length - 1].startsWith(`plugin_component_${this.serviceName}_`)) {
            const pluginComponent = this.pluginComponentFactory.create(key);
            this.components.set(key, {pluginComponent, initialized:false});
          }
        });
        let pendings = this.components.size;
        if(0 === pendings) {
          try {
            this.onInit();
          }
          catch(err) {
            cb(err);
          }
        }
        else {
          this.components.forEach((component, key) => {
            component.pluginComponent.init(() => {
              if(0 === --pendings) {
                if(this.hasException) {
                  cb();
                  return;
                }
                try {
                  this.onInit();
                }
                catch(err) {
                  cb(err);
                }
               }
            });
          });
        }
      }, ActorPathDist.getActorDistServerPluginComponentPath());
    }
    catch(err) {
      consoe.log('CATCH service init');
      process.nextTick(cb, err);
    }
  }
  
  exit(hasException, cb) {
    this.hasException = hasException;
    this.cb = cb;
    if(!hasException) {
      this.state = PluginService.STATE_EXITING;
      try {
        this.onExit(false);
      }
      catch(err) {
        cb(err);
      }
    }
    else {
      this.unhandledException(cb);
    }
  }
  
  done(err) {
    if(this?.cb) {
      const cb = this.cb;
      this.cb = null;
      if(PluginService.STATE_INITIALIZING === this.state) {
        this.state = PluginService.STATE_RUNNING;
      }
      else if(PluginService.STATE_EXITING === this.state) {
        this.state = PluginService.STATE_DONE;
      }
      else {
        ddb.error('WRONG STATE:');
      }
      if(!this.hasException) {
        process.nextTick(cb, err);
      }
      else {
        cb(err);
      }
    }
    else {
      ddb.error('done called on a ServicePlugin without being pending.'.red);
    }
  }
  
  subscribe() {
    if(this.onSubscribe) {
      this.onSubscribe();
    }
  }
  
  subscribeOnExternalService(serviceName, cb) {
    this.serviceManager.subscribeOnExternalService(serviceName, this, cb);
  }
  
  subscribeOnNodeStarted(cb) {
    this.node.subscribeOnNodeStarted(cb);
  }
  
  getComponent(name) {
    if(name) {
      return this.components.get(name);
    }
    else {
      return this.components[0];
    }
  }
  
  getService(name) {
    return this.serviceManager.getService(name);
  }
  
  subscribeOnConnectionEvents(guid, eventType, name, cb) {
    this.node.subscribeOnConnectionEvents(guid, eventType, name, cb);
  }
  
  unsubscribeOnConnectionEvents(guid, eventType, name) {
    this.node.unsubscribeOnConnectionEvents(guid, eventType, name);
  }
  
  sendToService(serviceAction, sessionId, cb) {
    const requestId = GuidGenerator.create();
    if('function' === typeof sessionId) {
      cb = sessionId;
      serviceAction.setIds(requestId);
    }
    else {
      serviceAction.setIds(requestId, sessionId);
    }
    const connectionData = this.serviceManager.externalServices.getConnectionData(serviceAction.serviceName);
    if(connectionData) {
      this.serviceRequests.addRequest(serviceAction.serviceName, serviceAction.actionRequest.id, sessionId, this, cb)
      connectionData.channelOutput.send(serviceAction.actionRequest, null, false, new Envelope(serviceAction.serviceName, requestId));
    }
    else {
      //ActionResponseError
      process.nextTick(cb, new Error('Service Not Found.'));
    }
  }
  
  receiveResponseFromService(requestId, sessionId, serviceName, responses, cb) {
    if(1 === responses.length) {
      if('success' === responses[0].result.code) {
        cb(null, sessionId, responses[0].data);
      }
      else {
        cb(new Error(responses[0].result.msg), sessionId);
      }
    }
    else {
      cb(new Error('Wrong response format'));
    }
  }
  
  /*sendMessageToPersistentStore(message, storeName) {
    return this.dataResponse.sendMessageToPersistentStore(message, storeName);
  }*/
  
  publish(dataPluginName, cb, ...params) {
    this.dataResponse.publish(dataPluginName, this.subscriptionName, cb, ...params);
  }
  
  unhandledException(cb) {
    let cancelId = null;
    this.cb = (err) => {
      if(cancelId) {
        clearTimeout(cancelId);
      }
      this.cb = null;
      cb(err);
    };
    switch(this.state) {
      case PluginService.STATE_NONE: {
        cb();
        break;
      }
      case PluginService.STATE_INITIALIZING: {
        ddb.error('unhandledException in PluginService.STATE_EXITING');
        break;
      }
      case PluginService.STATE_RUNNING: {
        this.state = PluginService.STATE_EXITING;
        this.onExit(true);
        if(PluginService.STATE_DONE !== this.state) {
          cancelId = setTimeout(() => {
            this.cb();
            this.cb = (err) => {};
          }, 1000);
        }
        break;
      }
      case PluginService.STATE_EXITING: {
        ddb.error('unhandledException in PluginService.STATE_EXITING');
        break;
      }
      case PluginService.STATE_DONE: {
        cb();
        break;
      }
    };
  }
}


module.exports = PluginService;
