
'use strict';

const ActorPathDist = require('../path/actor-path-dist');
const Const = require('../node/const');
const PluginFactory = require('../plugin-factory');
const PluginService = require('./plugin-service');
const ServiceExport = require('./service-export');
const ServiceRequests = require('./service-requests');


class ServiceManager {
  constructor(dataResponse) {
    this.externalServices = dataResponse.externalServices;
    //this.localServices = new Map();
    this.frontendServices = new Map();
    this.backendServices = new Map();
    this.surveillanceServices = new Map();
    this.pluginServiceParams = [dataResponse, this];
    this.pluginServiceFactory = new PluginFactory('service', this.pluginServiceParams);
    this.servicePlugins = null;
    this.serviceRequests = new ServiceRequests();
  }
  
  init(pluginComponents, pluginServices, cb) {
    this.pluginServiceParams.push(this.serviceRequests);
    this.pluginServiceParams.push(pluginComponents);
    this.pluginServiceFactory.setPaths(pluginServices);
    this.pluginServiceFactory.load((size) => {
      ddb.writelnTime(ddb.yellowBright('Loaded:   '), ddb.yellow(size), ' ', ddb.greenBright('service plugins from:'.padEnd(23 - size.toString().length)), pluginServices.join(', '));
      cb();
    }, ActorPathDist.getActorDistServerPluginServicePath());
  }
  
  startServices(cb) {
    this.createServices();
    if(0 === this.servicePlugins.size) {
      process.nextTick(cb);
      return;
    }
    let currentPriority = 0;
    let pendingsPriority = 0;
    let priorityQueue = [];
    this.servicePlugins.forEach((service) => {
      if(currentPriority !== service.object.priority) {
        currentPriority = service.object.priority;
        priorityQueue.push([service.object]);
      }
      else {
        priorityQueue[priorityQueue.length - 1].push(service.object);
      }
    });
    this._startServices(priorityQueue, cb);
  }
  
  stopServices(hasException, cb) {
    let pendings = this.servicePlugins.size;
    if(0 === pendings) {
      cb();
    }
    this.servicePlugins.forEach((service) => {
      this.stopService(service.object, hasException, () => {
        if(0 === --pendings) {
          cb();
        }
      });
    });
  }
  
  getService(name) {
    const service = this.servicePlugins.get(name);
    if(!service) {
      ddb.writelnTime(ddb.red(`Could not find Service: '${name}'.`));
      ddb.writelnTime(ddb.yellow('Existing services:'));
      this.servicePlugins.forEach((servicePlugin) => {
        ddb.writelnTime(ddb.yellow('*'), ddb.green(servicePlugin.name));
      });
      return;
    }
    return service.object;
  }
  
  subscripeOnLocalService(serviceName) {
    const service = this.servicePlugins.get(serviceName);
    service.object.subscribe();
  }
  
  subscribeOnExternalService(serviceName, servicePlugin, cb) {
    this.externalServices.on(serviceName, servicePlugin, (connectionData, has) => {
      this.serviceRequests.onExternalService(serviceName, has);
      cb(connectionData, has);
    });
  }
  
  _createServicePlugin(servicePlugins, path, name) {
    try {
      const service = this.pluginServiceFactory.create(name);
      if(service) {
        const lowerCaseName = name.toLowerCase(name);
        service.subscriptionName = lowerCaseName;
        const serviceData = {
          name: name,
          object: service
        };
        servicePlugins.push([lowerCaseName, serviceData]);
        if(PluginService.OFFER_BACKEND === service.exportType) {
          this.addBackendService(lowerCaseName, serviceData);
        }
        else if(PluginService.OFFER_FRONTEND === service.exportType) {
          this.addFrontendService(lowerCaseName, serviceData);
        }
        else if(PluginService.OFFER_SURVEILLANCE === service.exportType) {
          this.addSurveillanceService(lowerCaseName, serviceData);
        }
      }
    }
    catch(err) {
      ddb.error(ddb.red('Could not create service: '), name, ddb.red('.'));
      console.log(err); 
    }
  }
  
  createServices() {
    const servicePlugins = [];
    this.pluginServiceFactory.plugins.forEach((value, key) => {
      this._createServicePlugin(servicePlugins, value, key);
    });
    servicePlugins.sort((a, b) => {
      if(a[1].object.priority < b[1].object.priority) {
        return -1;
      }
      else if (a[1].object.priority > b[1].object.priority) {
        return 1;
      }
      else {
        return 0;
      }
    });
    this.servicePlugins = new Map(servicePlugins);
  }
  
  startService(service, cb) {
    process.nextTick((service, ddb) => {
      service.init((err) => {
        if(err) {
          ddb.writelnTime(ddb.cyanBright('Service:  '), service.serviceName, ddb.red(' did not start.'));
          console.log(err);
        }
        else {
          ddb.writelnTime(ddb.cyanBright('Service:  '), service.serviceName, ddb.green(' started.'));
        }
        cb();
      });
    }, service, ddb);
  }
  
  stopService(service, hasException, cb) {
    if(!hasException) {
      process.nextTick(this._stopService, service, hasException, cb);
    }
    else {
      this._stopService(service, hasException, cb);
    }
  }
  
  _stopService(service, hasException, cb) {
    if(service) {
      service.exit(hasException, (err) => {
        if(err) {
          ddb.writelnTime(ddb.cyanBright('Service:  '), service.serviceName, ddb.red(' did not stop.'));
        }
        else {
          ddb.writelnTime(ddb.cyanBright('Service:  '), service.serviceName, ddb.green(' stopped.'));
        }
        cb(err);
      });
    }
  }
  
  popRequest(serviceName, requestId) {
    return this.serviceRequests.popRequest(serviceName, requestId);
  }
  
  __startServices(services, cb) {
    let pendings = services.length;
    services.forEach((service) => {
      this.startService(service, () => {
        if(0 === --pendings) {
          cb();
        }
      });
    });
  }
  
  _startServices(priorityQueue, cb) {
    const services = priorityQueue.shift();
    if(services) {
      this.__startServices(services, () => {
        this._startServices(priorityQueue, cb) ;
      }) ;
    }
    else {
      cb();
    }
  }
  
  getServices() {
    if(0 !== this.servicePlugins.size) {
      return [...this.servicePlugins.keys()];
    }
    else {
      return [];
    }
  }
  
  addLocalService() {
    
  }
  
  addFrontendService(name, service) {
    this.frontendServices.set(name, service);
  }
  
  addBackendService(name, service) {
    this.backendServices.set(name, service);
  }
  
  addSurveillanceService(name, service) {
    this.surveillanceServices.set(name, service);
  }
    
  getServiceExport(fromNodeType, toNodeType) {
    if(fromNodeType === Const.NODE_TYPE_BACKEND) {
      if(toNodeType === Const.NODE_TYPE_BACKEND) {
        return new ServiceExport([], this._getServiceExport(this.backendServices), []);
      }
      else if(toNodeType === Const.NODE_TYPE_PROXY) {
        return new ServiceExport(this._getServiceExport(this.frontendServices), this._getServiceExport(this.backendServices), []);
      }
      else if(toNodeType === Const.NODE_TYPE_SURVEILLANCE) {
        return new ServiceExport([], [], this._getServiceExport(this.surveillanceServices));
      }
    }
    else if(fromNodeType === Const.NODE_TYPE_FRONTEND) {}
    else if(fromNodeType === Const.NODE_TYPE_PROXY) {
      if(toNodeType === Const.NODE_TYPE_SURVEILLANCE) {
        return new ServiceExport([], [], this._getServiceExport(this.surveillanceServices));
      }
    }
    else if(fromNodeType === Const.NODE_TYPE_SURVEILLANCE) {
      if(toNodeType === Const.NODE_TYPE_BACKEND || toNodeType === Const.NODE_TYPE_PROXY) {
        return new ServiceExport([], this._getServiceExport(this.backendServices), []);
      }
      return new ServiceExport([], [], []);
    }
    return new ServiceExport([], [], []);
  }
  
  _getServiceExport(services) {
    if(0 !== services.size) {
      return [...services.keys()];
    }
    else {
      return [];
    }
  }
}


module.exports = ServiceManager;
