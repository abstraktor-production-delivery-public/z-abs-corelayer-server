
'use strict';

let Clc = require('cli-color');


class LogConfig {
  constructor() {
    this.config = [
      {
        name: 'ENGINE',
        type: 'ENGINE ',
        color: [
          Clc.black.bgWhiteBright,
          Clc.black.bgWhite
        ],
        groups: [
          {
            name: 'AS',
            enabled: false,
            displayName: 'Actor State '
          },
          {
            name: 'AR',
            enabled: false,
            displayName: 'Actor Result'
          },
          {
            name: 'DP',
            enabled: false,
            displayName: 'Data Plugin '
          },
          {
            name: 'PTH',
            enabled: false,
            displayName: 'Actor Paths '
          },
          {
            name: 'BLD',
            enabled: false,
            displayName: 'Build Clean '
          },
          {
            name: 'MSG',
            enabled: false,
            displayName: 'Message     '
          },
          {
            name: 'TC',
            enabled: false,
            displayName: 'Test Case   '
          }
        ],
        groupAll: false
      },
      {
        name: 'DEBUG',
        type: 'DEBUG  ',
        color: [
          Clc.magentaBright.bgWhiteBright,
          Clc.magentaBright.bgWhite
        ],
        groups: [
          {
            name: 'AC',
            enabled: false,
            displayName: 'Actor'
          },
          {
            name: 'AM',
            enabled: false,
            displayName: 'Actor Method'
          },
          {
            name: 'DE',
            enabled: false,
            displayName: 'Debug Output'
          }
        ],
        groupAll: false
      },
      {
        name: 'ERROR',
        type: 'ERROR  ',
        color: [
          Clc.redBright.bgWhiteBright,
          Clc.redBright.bgWhite
        ],
        groups: [
          {
            name: 'ERR',
            enabled: true,
            displayName: 'error       '
          },
          {
            name: 'CATCH',
            enabled: true,
            displayName: 'Catch(e)    '
          }
        ],
        groupAll: true
      },
    ];
  }
}

module.exports = new LogConfig();
