
'use strict';

const LogConfig = require('./log-config');
const LogTypes = require('./log-types');
const HighResolutionDate = require('z-abs-corelayer-cs/clientServer/time/high-resolution-date');
const Clc = require('cli-color');


class LoggerConsole {
  constructor() {
    this.id = 0;
  }

  log(msg) {
    let id = this.id = LoggerConsole.IDS[this.id];
//    setImmediate(() => {
      if(msg.log.length > LoggerConsole.LOG_SIZE) {
        console.log(LogConfig.config[msg.type].color[id](`${LogTypes.name[msg.type]} : ${new HighResolutionDate(msg.date).getDateMilliSeconds()} : ${LoggerConsole.PID_ZEROS[`${msg.pid}`.length]}${msg.pid} : ${msg.log.substring(0, LoggerConsole.LOG_SIZE - 3)}... : ${LogConfig.config[msg.type].groups[msg.group].displayName} : ${msg.fileName}`));
      }
      else {
        console.log(LogConfig.config[msg.type].color[id](`${LogTypes.name[msg.type]} : ${new HighResolutionDate(msg.date).getDateMilliSeconds()} : ${LoggerConsole.PID_ZEROS[`${msg.pid}`.length]}${msg.pid} : ${msg.log}${LoggerConsole.SPACE_STORE.substring(0, LoggerConsole.LOG_SIZE - msg.log.length)} : ${LogConfig.config[msg.type].groups[msg.group].displayName} : ${msg.fileName}`));
      }
      if(undefined !== msg.err) {
        console.log(LogConfig.config[msg.type].color[id](msg.err));
      }
//    });
  }
}

LoggerConsole.IDS = [1, 0];
LoggerConsole.SPACE_STORE = '                                                                                                                                                                                                                                                                                                                                              ';
LoggerConsole.LOG_SIZE = 256;
LoggerConsole.PID_ZEROS = ['', '     ', '    ', '   ', '  ',  ' ', ''];

module.exports = LoggerConsole;
