
'use strict';

const LogConfig = require('./log-config');
const LogTypes = require('./log-types');
const HighResolutionTimestamp = require('../high-resolution-timestamp');
const LoggerConsole = require('./logger-console');


class Logger {
  constructor() {
    this.listener = null;
    this.loggerImpl = new LoggerConsole();
    // Create Constants from LogConfig. To avoid double code or exported dependencies.
    LogConfig.config.forEach((config, index) => {
      const groups = {}
      config.groups.forEach((group, index) => {
        Reflect.set(groups, group.name, index);
      });
      Reflect.set(this, config.name, groups);
    });
  }
  
  engine(group, log, fileName) {
    this._log(LogTypes.ENGINE, group, log, undefined, fileName);
  }

  debug(group, log, fileName) {
    this._log(LogTypes.DEBUG, group, log, undefined, fileName);
  }

  error(group, log, err, fileName) {
    this._log(LogTypes.ERROR, group, log, err, fileName);
  }

  isLogEngine(group) {
    return (LogConfig.config[LogTypes.ENGINE].groupAll) || (LogConfig.config[LogTypes.ENGINE].groups[group].enabled);
  }
  
  isLogDebug(group) {
    return (LogConfig.config[LogTypes.DEBUG].groupAll) || (LogConfig.config[LogTypes.DEBUG].groups[group].enabled);
  }
  
  isLogError(group) {
    return (LogConfig.config[LogTypes.ERROR].groupAll) || (LogConfig.config[LogTypes.ERROR].groups[group].enabled);
  }
  
  addListener(listener) {
    this.listener = listener;
  }
  
  clearListener() {
    this.listener = null;
  }
  
  _log(type, group, log, err, fileName) {
    if((LogConfig.config[type].groupAll) || (LogConfig.config[type].groups[group].enabled)) {
      let date = HighResolutionTimestamp.getHighResolutionDate();
      let logData = {
        type: type,
        date: date,
        pid: process.pid,
        log: log,
        group: group,
        err: undefined !== err ? err.message : undefined,
        fileName: fileName
      };
      this.loggerImpl.log(logData);
      if(null !== this.listener) {
        this.listener(logData);
      }
    }
  }

  initTestCase(name, timestamp) {
    let logTc = {
      name: name,
      start: HighResolutionTimestamp.getHighResolutionDate(timestamp),
      stop: undefined
    };
    /*setImmediate(() => {
      this.loggerImpl.tcStart(logTc);
    });*/
    return logTc;
  }

  exitTestCase(logTc, timestamp) {
    /*logTc.stop = HighResolutionTimestamp.getHighResolutionDate(timestamp);
    setImmediate(() => {
      this.loggerImpl.tcStop(logTc);
    });*/
  }
  
  initTestSuite(name, timestamp) {
    let logTs = {
      name: name,
      start: HighResolutionTimestamp.getHighResolutionDate(timestamp),
      stop: undefined
    };
    /*setImmediate(() => {
      this.loggerImpl.tsStart(logTs);
    });*/
    return logTs;
  }

  exitTestSuite(logTs, timestamp) {
    /*logTs.stop = HighResolutionTimestamp.getHighResolutionDate(timestamp);
    setImmediate(() => {
      this.loggerImpl.tsStop(logTs);
    });*/
  }
}

module.exports = new Logger();
