
'use strict';


class LogTypes {
  constructor() {
    this.ENGINE = 0;
    this.DEBUG = 1;
    this.ERROR = 2;
    this.name = ['Engine', 'Debug ', 'Error '];
  }
}

module.exports = new LogTypes();
