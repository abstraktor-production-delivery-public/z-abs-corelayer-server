
'use strict';

const Const = require('./const');
const HttpCache = require('./http-cache');
const HttpServer = require('./http-server');
const HttpsServer = require('./https-server');
const Http2Server = require('./http2-server');
const WsServer = require('./ws-server');
const WssServer = require('./wss-server');
const WsWebServer = require('./ws-web-server');
const TcpServer = tryRequire('z-abs-corelayer-bs/server/node/tcp-server');
const TlsServer = tryRequire('z-abs-corelayer-bs/server/node/tls-server');
const IpProxy = tryRequire('z-abs-corelayer-bs/server/node/ip-proxy');
const HttpProxy = tryRequire('z-abs-corelayer-bs/server/node/http-proxy');
const HttpProxyServer = tryRequire('z-abs-corelayer-bs/server/node/http-proxy-server');
const HttpsProxyServer = tryRequire('z-abs-corelayer-bs/server/node/https-proxy-server');
const Http2ProxyServer = tryRequire('z-abs-corelayer-bs/server/node/http2-proxy-server');


class Servers {
  constructor(nodeName, nodeId, nodeSettings, defaultSite, dataResponse) {
    this.nodeName = nodeName;
    this.nodeId = nodeId;
    this.nodeSettings = nodeSettings;
    this.defaultSite = defaultSite;
    this.dataResponse = dataResponse;
    this.proxyServerDatas = [];
    this.httpCache = new HttpCache();
    this.servers = new Map();
  }
  
  create() {
    if(0 === this.nodeSettings.wsWebServers.length && 0 === this.nodeSettings.httpServers.length && 0 === this.nodeSettings.httpsServers.length && 0 === this.nodeSettings.http2Servers.length && 0 === this.nodeSettings.tcpServers.length && 0 === this.nodeSettings.tlsServers.length && 0 === this.nodeSettings.wsServers.length && 0 === this.nodeSettings.wssServers.length && 0 === this.nodeSettings.ipProxyServers.length) {
      return;
    }
    if(0 !== this.nodeSettings.httpServers.length) {
      this.nodeSettings.httpServers.forEach((httpServerData) => {
        const httpServer = new HttpServer(httpServerData, this.nodeName, this.nodeId, this.httpCache, this.dataResponse, this.defaultSite);
        this.servers.set(httpServerData.name, httpServer);
      });
    }
    if(0 !== this.nodeSettings.httpsServers.length) {
      this.nodeSettings.httpsServers.forEach((httpsServerData) => {
        const httpsServer = new HttpsServer(httpsServerData, this.nodeName, this.nodeId, this.httpCache, this.dataResponse, this.localServices, this.defaultSite);
        this.servers.set(httpsServerData.name, httpsServer);
      });
    }
    if(0 !== this.nodeSettings.http2Servers.length) {
      this.nodeSettings.http2Servers.forEach((http2ServerData) => {
        const httpsServer = new Http2Server(http2ServerData, this.nodeName, this.nodeId, this.httpCache, this.dataResponse, this.localServices, this.defaultSite);
        this.servers.set(http2ServerData.name, httpsServer);
      });
    }
    if(TcpServer && 0 !== this.nodeSettings.tcpServers.length) {
      this.nodeSettings.tcpServers.forEach((tcpServerData) => {
        this._createIpServer(Const.TRANSPORT_TCP, this.dataResponse, tcpServerData);
      });
    }
    if(TlsServer && 0 !== this.nodeSettings.tlsServers.length) {
      this.nodeSettings.tlsServers.forEach((tlsServerData) => {
        this._createIpServer(Const.TRANSPORT_TLS, this.dataResponse, tlsServerData);
      });
    }
    if(0 !== this.nodeSettings.wsServers.length) {
      this.nodeSettings.wsServers.forEach((wsServerData) => {
        this._createIpServer(Const.TRANSPORT_WS, this.dataResponse, wsServerData);
      });
    }
    if(0 !== this.nodeSettings.wssServers.length) {
      this.nodeSettings.wssServers.forEach((wssServerData) => {
        this._createIpServer(Const.TRANSPORT_WSS, this.dataResponse, wssServerData);
      });
    }
    if(IpProxy && 0 !== this.nodeSettings.ipProxyServers.length) {
      this.nodeSettings.ipProxyServers.forEach((proxyServerData) => {
        const ipProxy = new IpProxy(this.nodeName, this.nodeId);
        this._createIpServer(proxyServerData.frontend.type, ipProxy.ipProxyFrontend, proxyServerData.frontend.settings);
        this._createIpServer(proxyServerData.backend.type, ipProxy.ipProxyBackend, proxyServerData.backend.settings);
      });
    }
    if(HttpProxy && 0 !== this.nodeSettings.httpProxyServers.length) {
      this.nodeSettings.httpProxyServers.forEach((proxyServerData) => {
        const httpProxy = new HttpProxy(this.nodeName, this.nodeId, proxyServerData.backends);
        this._createHttpProxyServer(proxyServerData.frontend.type, httpProxy, proxyServerData.frontend.settings);
      });
    }
    if(0 !== this.nodeSettings.wsWebServers.length) {
      this.nodeSettings.wsWebServers.forEach((wsWebServerData) => {
    		const wsWebServer = new WsWebServer(wsWebServerData, this.nodeName, this.nodeId, this.dataResponse);
		    this.servers.set(wsWebServerData.name, wsWebServer);
      });
    }
  }
  
  start(cb) {
    let pendings = this.servers.size;
    if(0 === pendings) {
      process.nextTick(cb);
      return;
    }
    this.servers.forEach((server) => {
      server.start(server.serverData, () => {
        if(0 === --pendings) {
          cb();
        }
      });
    });
  }
  
  subscribeOnConnectionEvents(guid, eventType, name, cb) {
    const server = this.servers.get(name);
    if(server) {
      server.ipServer.dataResponse.ipSubscription.subscribeOnConnectionEvents(guid, eventType, name, cb);
    }
  }
  
  unsubscribeOnConnectionEvents(guid, eventType, name) {
    const server = this.servers.get(name);
    if(server) {
      server.ipServer.dataResponse.ipSubscription.unsubscribeOnConnectionEvents(guid, eventType, name);
    }
  }
  
  _createIpServer(type, channel, serverData) {
    let server = null;
    if(Const.TRANSPORT_TCP === type) {
      server = new TcpServer(serverData.name, serverData, this.nodeName, this.nodeId, channel);
    }
    else if(Const.TRANSPORT_TLS === type) {
      server = new TlsServer(serverData.name, serverData, this.nodeName, this.nodeId, channel);
    }
    else if(Const.TRANSPORT_WS === type) {
      server = new WsServer(serverData.name, serverData, this.nodeName, this.nodeId, channel);
    }
    else if(Const.TRANSPORT_WSS === type) {
      server = new WssServer(serverData.name, serverData, this.nodeName, this.nodeId, channel);
    }
    if(server) {
      this.servers.set(serverData.name, server);
    }
    else {
      console.log('ERROR');
    }
  }
  
  _createHttpProxyServer(type, httpProxy, serverData) {
    let server = null;
    if(Const.TRANSPORT_HTTP === type) {
      server = new HttpProxyServer(serverData.name, serverData, httpProxy);
    }
    else if(Const.TRANSPORT_HTTPS === type) {
      server = new HttpsProxyServer(serverData.name, serverData, httpProxy);
    }
    else if(Const.TRANSPORT_HTTP2 === type) {
      server = new Http2ProxyServer(serverData.name, serverData, httpProxy);
    }
    if(server) {
      this.servers.set(serverData.name, server);
    }
    else {
      console.log('ERROR');
    }
  }
}


module.exports = Servers;
