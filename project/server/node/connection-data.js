
'use strict';

const ChannelInput = require('./channel-input');
const ChannelOutput = require('./channel-output');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');


class ConnectionData {
  static id = 0;
  
  constructor(channelInputName, channelOutputName, outputCbMessage) {
    this.id = GuidGenerator.create();
    this.sessionCache = new Map();
    this.channelInput = channelInputName ? new ChannelInput(channelInputName) : null;
    this.channelOutput = channelOutputName ? new ChannelOutput(channelOutputName, outputCbMessage) : null;
  }
  
  print() {
    console.log(this.initialized, this.nodeName, this.serviceNames);
  }
}


module.exports = ConnectionData;
