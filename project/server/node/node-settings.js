
'use strict';

const Const = require('./const');


class NodeSettings {
  static CONFIG_DEFUALT = 'default';
  
  constructor(releaseData, dynamicConfig, originalSettings) {
    this.releaseData = releaseData;
    this.dynamicConfig = dynamicConfig;
    this.originalSettings = originalSettings;
    this.settings = {
      httpServers: [],
      http2Servers: [],
      httpsServers: [],
      tcpServers: [],
      tlsServers: [],
      wsServers: [],
      wssServers: [],
      tcpClients: [],
      tlsClients: [],
      wsClients: [],
      wssClients: [],
      ipProxyServers: [],
      httpProxyServers: [],
      wsWebServers: []
    };
    let config = NodeSettings.CONFIG_DEFUALT;
    for(let i = 0; i < originalSettings.configurations.length; ++i) {
      const hasConfig = this._hasConfig(originalSettings.configurations[i]);
      if(hasConfig) {
        config = originalSettings.configurations[i];
      }
    }
    this._parse(config);
  }
  
  _parse(config) {
    const settings = this.originalSettings;
    if(settings.httpServers) {
      settings.httpServers.forEach((httpServer, index) => {
        const setting = {name:httpServer.name, type:httpServer.type, host:this._getHost(config, index, httpServer), port:this._getPort(config, index, httpServer), directories: httpServer.directories};
        this.settings.httpServers.push(setting);
      });
    }
    if(settings.http2Servers) {
      settings.http2Servers.forEach((http2Server, index) => {
        const setting = {name:http2Server.name, type:http2Server.type, host:this._getHost(config, index, http2Server), port:this._getPort(config, index, http2Server), directories: http2Server.directories, keyFile:http2Server.keyFile, certFile:http2Server.certFile};
        this.settings.http2Servers.push(setting);
      });
    }
    if(settings.httpsServers) {
      settings.httpsServers.forEach((httpsServer, index) => {
        const setting = {name:httpsServer.name, type:httpsServer.type, host:this._getHost(config, index, httpsServer), port:this._getPort(config, index, httpsServer), directories: httpsServer.directories, keyFile:httpsServer.keyFile, certFile:httpsServer.certFile};
        this.settings.httpsServers.push(setting);
      });
    }
    if(settings.tcpServers) {
      settings.tcpServers.forEach((tcpServer, index) => {
        const setting = {name:tcpServer.name, type:tcpServer.type, host:this._getHost(config, index, tcpServer), port:this._getPort(config, index, tcpServer), protocols:tcpServer.protocols};
        this.settings.tcpServers.push(setting);
      });
    }
    if(settings.tlsServers) {
      settings.tlsServers.forEach((tlsServer, index) => {
        const setting = {name:tlsServer.name, type:tlsServer.type, host:this._getHost(config, index, tlsServer), port:this._getPort(config, index, tlsServer), protocols:tlsServer.protocols, keyFile:tlsServer.keyFile, certFile:tlsServer.certFile};
        this.settings.tlsServers.push(setting);
      });
    }
    if(settings.wsServers) {
      settings.wsServers.forEach((wsServer, index) => {
        const setting = {name:wsServer.name, type:wsServer.type, host:this._getHost(config, index, wsServer), port:this._getPort(config, index, wsServer), protocols:wsServer.protocols};
        this.settings.wsServers.push(setting);
      });
    }
    if(settings.wssServers) {
      settings.wssServers.forEach((wssServer, index) => {
        const setting = {name:wssServer.name, type:wssServer.type, host:this._getHost(config, index, wssServer), port:this._getPort(config, index, wssServer), protocols:wsServer.protocols, keyFile:wssServer.keyFile, certFile:wssServer.certFile};
        this.settings.wssServers.push(setting);
      });
    }
    if(settings.tcpClients) {
      settings.tcpClients.forEach((tcpClient) => {
        const setting = {name:tcpClient.name, type:tcpClient.type, host:this._getHost(config, -1, tcpClient), port:this._getPort(config, -1, tcpClient), dependency:tcpClient.dependency, protocols:tcpClient.protocols};
        this.settings.tcpClients.push(setting);
      });
    }
    if(settings.tlsClients) {
      settings.tlsClients.forEach((tlsClient) => {
        const setting = {name:tlsClient.name, type:tlsClient.type, host:this._getHost(config, -1, tlsClient), port:this._getPort(config, -1, tlsClient), dependency:tlsClient.dependency, protocols:tlsClient.protocols, keyFile:tlsClient.keyFile, certFile:tlsClient.certFile};
        this.settings.tlsClients.push(setting);
      });
    }
    if(settings.wsClients) {
      settings.wsClients.forEach((wsClient) => {
        const setting = {name:wsClient.name, type:wsClient.type, host:this._getHosts(config, -1, wsClient), port:this._getPort(config, -1, wsClient), dependency:wsClient.dependency, protocols:wsClient.protocols};
        this.settings.wsClients.push(setting);
      });
    }
    if(settings.wssClients) {
      settings.wssClients.forEach((wssClient) => {
        const setting = {name:wssClient.name, type:wssClient.type, host:this._getHost(config, -1, wssClient), port:this._getPort(config, -1, wssClient), dependency:wssClient.dependency, protocols:wssClient.protocols, keyFile:wssClient.keyFile, certFile:wssClient.certFile};
        this.settings.wssClients.push(setting);
      });
    }
    if(settings.ipProxyServers) {
      settings.ipProxyServers.forEach((ipProxyServer) => {
        let frontendType = null;
        const setting = {
          frontend: {
            type: 0,
            settings: null
          },
          backend: {
            type: 0,
            settings: null
          }
        };
        this.settings.ipProxyServers.push(setting);
        this._getIpProxyType(ipProxyServer.frontend, setting.frontend, config, -1);
        this._getIpProxyType(ipProxyServer.backend, setting.backend, config, -1);
      });
    }
    if(settings.httpProxyServers) {
      settings.httpProxyServers.forEach((httpProxyServer) => {
        let frontendType = null;
        const setting = {
          frontend: {
            type: 0,
            settings: null
          },
          backends: []
        };
        this.settings.httpProxyServers.push(setting);
        this._getHttpProxyServerType(httpProxyServer.frontend, setting.frontend, config, -1);
        this._getHttpProxyClientType(httpProxyServer.backends, setting.backends, config, -1);
      });
    }
    if(settings.wsWebServers) {
      settings.wsWebServers.forEach((wsWebServer, index) => {
        const setting = {name:wsWebServer.name, type:wsWebServer.type, host:this._getHost(config, index, wsWebServer), port:this._getPort(config, index, wsWebServer), protocols:wsWebServer.protocols};
        this.settings.wsWebServers.push(setting);
      });
    }
  }
  
  _getHttpProxyServerType(frontendOriginalSettings, frontendParsedSettings, config, index) {
    let endInstance = null;
    if(frontendOriginalSettings.httpProxyServer) {
      endInstance = frontendOriginalSettings.httpProxyServer;
      frontendParsedSettings.type = Const.TRANSPORT_HTTP;
      frontendParsedSettings.settings = {name:endInstance.name, type:endInstance.type, host:this._getHost(config, index, endInstance), port:this._getPort(config, index, endInstance), protocols:endInstance.protocols};
    }
    else if(frontendOriginalSettings.httpsProxyServer) {
      endInstance = frontendOriginalSettings.tlsServer;
      frontendParsedSettings.type = Const.TRANSPORT_HTTPS;
      frontendParsedSettings.settings = {name:endInstance.name, type:endInstance.type, host:this._getHost(config, index, endInstance), port:this._getPort(config, index, endInstance), protocols:endInstance.protocols, keyFile:endInstance.keyFile, certFile:endInstance.certFile};
    }
    else if(frontendOriginalSettings.wsServer) {
      endInstance = frontendOriginalSettings.wsServer;
      frontendParsedSettings.type = Const.TRANSPORT_HTTP2;
      frontendParsedSettings.settings = {name:endInstance.name, type:endInstance.type, host:this._getHost(config, index, endInstance), port:this._getPort(config, index, endInstance), protocols:endInstance.protocols, keyFile:endInstance.keyFile, certFile:endInstance.certFile};
    }
    else {
      return;
    }
    if(endInstance.protocols) {
      if(endInstance.protocols.client) {
        frontendParsedSettings.settings.protocols.client = endInstance.protocols.client;
      }
      if(endInstance.protocols.server) {
        frontendParsedSettings.settings.protocols.server = endInstance.protocols.server;
      }
    }
  }
  
  _getHttpProxyClientType(backendOriginalSettings, backendParsedSettings, config, index) {
    backendOriginalSettings.forEach((backendOriginalSetting) => {
      const backend = {
        type: 0,
        urls: null,
        settings: null
      };
      let endInstance = null;
      backendParsedSettings.push(backend);
      if(backendOriginalSetting.tcpClient) {
        endInstance = backendOriginalSetting.tcpClient;
        backend.type = Const.TRANSPORT_TCP;
        backend.settings = {name:endInstance.name, type:endInstance.type, host:this._getHost(config, index, endInstance), port:this._getPort(config, index, endInstance), protocols:endInstance.protocols};
      }
      else if(backendOriginalSetting.tlsClient) {
        endInstance = backendOriginalSetting.tlsClient;
        backend.type = Const.TRANSPORT_TLS;
        backend.settings = {name:endInstance.name, type:endInstance.type, host:this._getHost(config, index, endInstance), port:this._getPort(config, index, endInstance), protocols:endInstance.protocols, keyFile:endInstance.keyFile, certFile:endInstance.certFile};
      }
      else if(backendOriginalSetting.wsClient) {
        endInstance = backendOriginalSetting.wsClient;
        backend.type = Const.TRANSPORT_WS;
        backend.settings = {name:endInstance.name, type:endInstance.type, host:this._getHost(config, index, endInstance), port:this._getPort(config, index, endInstance), protocols:endInstance.protocols};
      }
      else if(backendOriginalSetting.wssClient) {
        endInstance = backendOriginalSetting.wssClient;
        backend.type = Const.TRANSPORT_WSS;
        backend.settings = {name:endInstance.name, type:endInstance.type, host:this._getHost(config, index, endInstance), port:this._getPort(config, index, endInstance), protocols:endInstance.protocols, keyFile:endInstance.keyFile, certFile:endInstance.certFile};
      }
      backend.urls = endInstance.urls;
    });
  }
  
  _getIpProxyType(endPart, settingsEnd, config, index) {
    let endInstance = null;
    if(endPart.tcpServer) {
      endInstance = endPart.tcpServer;
      settingsEnd.type = Const.TRANSPORT_TCP;
      settingsEnd.settings = {name:endInstance.name, type:endInstance.type, host:this._getHost(config, index, endInstance), port:this._getPort(config, index, endInstance), protocols:endInstance.protocols};
    }
    else if(endPart.tlsServer) {
      endInstance = endPart.tlsServer;
      settingsEnd.type = Const.TRANSPORT_TLS;
      settingsEnd.settings = {name:endInstance.name, type:endInstance.type, host:this._getHost(config, index, endInstance), port:this._getPort(config, index, endInstance), protocols:endInstance.protocols, keyFile:endInstance.keyFile, certFile:endInstance.certFile};
    }
    else if(endPart.wsServer) {
      endInstance = endPart.wsServer;
      settingsEnd.type = Const.TRANSPORT_WS;
      settingsEnd.settings = {name:endInstance.name, type:endInstance.type, host:this._getHost(config, index, endInstance), port:this._getPort(config, index, endInstance), protocols:endInstance.protocols};
    }
    else if(endPart.wssServer) {
      endInstance = endPart.wssServer;
      settingsEnd.type = Const.TRANSPORT_WSS;
      settingsEnd.settings = {name:endInstance.name, type:endInstance.type, host:this._getHost(config, index, endInstance), port:this._getPort(config, index, endInstance), protocols:endInstance.protocols, keyFile:endInstance.keyFile, certFile:endInstance.certFile};
    }
    else {
      return;
    }
    if(endInstance.protocols) {
      if(endInstance.protocols.client) {
        settingsEnd.settings.protocols.client = endInstance.protocols.client;
      }
      if(endInstance.protocols.server) {
        settingsEnd.settings.protocols.server = endInstance.protocols.server;
      }
    }
  }
  
  _hasConfig(name, index) {
    let value = '';
    if(!index) {
      value = this.dynamicConfig.parameters.has(name);
    }
    else {
      value = this.dynamicConfig.parameters.has(`${name}${index + 1}`);
    }
    return value;
  }
  
  _getConfig(name, index) {
    let value = '';
    if(!index) {
      value = this.dynamicConfig.parameters.get(name);
    }
    else {
      value = this.dynamicConfig.parameters.get(`${name}${index + 1}`);
    }
    return value;
  }
  
  _getHost(config, index, parameters) {
    if(-1 !== index) {
      let host = '';
      if('http-server' === parameters.name) {
        host = this._getConfig('httph', index);
      }
      else if('https-server' === parameters.name) {
        host = this._getConfig('httpsh', index);
      }
      else if('http2-server' === parameters.name) {
        host = this._getConfig('http2h', index);
      }
      else if('tcp-server' === parameters.name) {
        host = this._getConfig('tcph', index);
      }
      else if('tls-server' === parameters.name) {
        host = this._getConfig('tlsh', index);
      }
      else if('ws-server' === parameters.name) {
        host = this._getConfig('wsh', index);
      }
      else if('wss-server' === parameters.name) {
        host = this._getConfig('wssh', index);
      }
      else if('ws-web-server' === parameters.name) {
        host = this._getConfig('wswebh', index);
      }
      if(host) {
        return host;
      }
    }
    if(NodeSettings.CONFIG_DEFUALT === config) {
      return parameters.host;
    }
    else {
      return Reflect.get(parameters, `${config}Host`);
    }
  }
  
  _getPort(config, index, parameters) {
    if(-1 !== index) {
      let port = '';
      if('http-server' === parameters.name) {
        port = this._getConfig('httpp', index);
      }
      else if('https-server' === parameters.name) {
        port = this._getConfig('httpsp', index);
      }
      else if('http2-server' === parameters.name) {
        port = this._getConfig('http2p', index);
      }
      else if('tcp-server' === parameters.name) {
        port = this._getConfig('tcpp', index);
      }
      else if('tls-server' === parameters.name) {
        port = this._getConfig('tlsp', index);
      }
      else if('ws-server' === parameters.name) {
        port = this._getConfig('wsp', index);
      }
      else if('wss-server' === parameters.name) {
        port = this._getConfig('wssp', index);
      }
      else if('ws-web-server' === parameters.name) {
        port = this._getConfig('wswebp', index);
      }
      else if('wss-web-server' === parameters.name) {
        port = this._getConfig('wsswebp', index);
      }
      if(port) {
        return Number.parseInt(port);
      }
    }
    if(NodeSettings.CONFIG_DEFUALT === config) {
      return parameters.port;
    }
    else {
      return Reflect.get(parameters, `${config}Port`);
    }
  }
}


module.exports = NodeSettings;
