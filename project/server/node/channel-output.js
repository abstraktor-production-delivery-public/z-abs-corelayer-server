
'use strict';

const Encoder = require('../communication/core-protocol/encoder');
const TextCache = require('z-abs-corelayer-cs/clientServer/communication/cache/text-cache');
const Serializer = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/serializer');


class ChannelOutput {
  constructor(name, channel) {
    this.name = name;
    this.textCache = new TextCache(name, process.pid);
    this.channel = channel;
    this.serializer = new Serializer(Encoder, this.textCache);
  }
  
  register(id, serializer) {
    this.serializer.register(id, serializer);
  }
  
  send(msg, dataBuffers, debug, envelope) {
    if(undefined === dataBuffers) {
      console.log('ChannelOutput, undefined === dataBuffers');
    }
    if(undefined === debug) {
      console.log('ChannelOutput, undefined === debug');
    }
    const buffers = this.serializer.do(msg, dataBuffers, envelope);
    if(!debug) {
      process.nextTick((buffers) => {
        buffers.forEach((buffer) => {
          this.channel(buffer);
        });
      }, buffers);
    }
    else {
      buffers.forEach((buffer) => {
        this.channel(buffer);
      });
    }
  }
}


module.exports = ChannelOutput;
