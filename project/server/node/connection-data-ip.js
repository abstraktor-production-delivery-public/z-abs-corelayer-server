
'use strict';

const Const = require('./const');
const ChannelInput = require('./channel-input');
const ChannelOutput = require('./channel-output');
const HighResolutionTimestamp = require('../high-resolution-timestamp');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');


class ConnectionDataIp {
  static id = 0;
  
  constructor(channelInputName, channelOutputName, outputCbMessage) {
    this.id = GuidGenerator.create();
    this.sessionCache = new Map();
    this.channelInput = channelInputName ? new ChannelInput(channelInputName) : null;
    this.channelOutput = channelOutputName ? new ChannelOutput(channelOutputName, outputCbMessage) : null;
    
    this.name = '';
    this.connectionAddress = null;
    this.timestampConnect = HighResolutionTimestamp.getHighResolutionDate();
    this.timestampDisconnect = null;
    
    this.initialized = false;
    this.nodeName = '';
    this.nodeId = '';
    this.nodeType = -1;
    this.nodeSymbol = null;
    this.frontendServices = new Set();
    this.backendServices = new Set();
    this.surveillanceServices = new Set();
  }
  
  setConnectionInit(name, connectionAddress) {
    this.name = name;
    this.connectionAddress = connectionAddress;
  }
  
  setServiceInit(nodeName, nodeId, nodeType) {
    this.initialized = true;
    this.nodeName = nodeName;
    this.nodeId = nodeId;
    this.nodeType = nodeType;
    this.nodeSymbol = Symbol(`${this.nodeName}::${this.nodeId}`);
  }
  
  closed() {
    this.timestampDisconnect = HighResolutionTimestamp.getHighResolutionDate();
  }
  
  addService(serviceName, offerType) {
    if(Const.OFFER_FRONTEND === offerType) {
      this.frontendServices.add(serviceName);
    }
    else if(Const.OFFER_BACKEND === offerType) {
      this.backendServices.add(serviceName);
    }
    else if(Const.OFFER_SURVEILLANCE === offerType) {
      this.surveillanceServices.add(serviceName);
    }
  }
  
  deleteService(serviceName, offerType) {
    if(Const.OFFER_FRONTEND === offerType) {
      this.frontendServices.delete(serviceName);
    }
    else if(Const.OFFER_BACKEND === offerType) {
      this.backendServices.delete(serviceName);
    }
    else if(Const.OFFER_SURVEILLANCE === offerType) {
      this.surveillanceServices.delete(serviceName);
    }
  }
  
  print() {
    console.log(this.initialized, this.nodeName, this.serviceNames);
  }
}


module.exports = ConnectionDataIp;
