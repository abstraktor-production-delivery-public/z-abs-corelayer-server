
'use strict';

const Const = require('./const');
const ConnectionAddress = require('./connection-address');
const IpServer = require('./ip-server');
const PluginFactoryProtocol = require('../plugin-factor-protocol');


class WsServer {
  constructor(name, serverData, nodeName, nodeId, dataResponse) {
  	this.transport = Const.TRANSPORT_WS;
    this.name = name;
    this.serverData = serverData;
    this.ipServer = new IpServer(this, nodeName, nodeId, dataResponse, serverData.protocols);
  }
}


module.exports = WsServer;
