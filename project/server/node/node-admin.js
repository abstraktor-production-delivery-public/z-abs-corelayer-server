
'use strict';


const NodeData = require('./node-data');
const ActorPath = require('../path/actor-path');
const ActorPathGenerated = require('../path/actor-path-generated');
const MessageChannel = require('./message-channel');
const ServiceAction = require('z-abs-corelayer-cs/clientServer/communication/service-action');
const CoreProtocolConst = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/core-protocol-const');
require('z-abs-corelayer-cs/clientServer/debug-dashboard/log')
const ChildProcess = require('child_process');
const Fs = require('fs');
const Net = require('net');
const Path = require('path');


class NodeAdmin {
  constructor(host, port, cbStatus) {
    this.host = host;
    this.port = port;
    this.cbStatus = cbStatus;
    this.messageChannels = new Map();
    this.serverStatus = {
      up: false,
      upDate: null,
      connected: false,
      connectedName: ''
    };
    this.server = null;
    this.serverUpDate = null;
    this.serverDownReason = '';
    this.nodeDatas = [];
    this.nodesPersistent = new Map();
    this.supervisorChildProcess = null;
    this.nodesAtom = {
      lock: false,
      queue: []
    };
    this.initialized = false;
  }
  
  check(cb) {
    let topPendings = 2;
    let nodesStatus = [];
    const nodesFound = [];
    Fs.readFile(ActorPathGenerated.getNodesFile(), 'utf8', (err, data) => {
      if(err) {
        // TODO: add the file if it does not exist.
        return cb(err);
      }
      const nodeDatas = JSON.parse(data).nodeDatas;
      nodeDatas.forEach((node) => {
        nodesStatus.push(NodeData.from(node));
      });
      if(0 === --topPendings) {
        if(cb) {
          this._calculateNodes(nodesStatus, nodesFound, cb);
        }
      }
    });
    const basePath = Path.resolve(`${ActorPath.getActorPath()}${Path.sep}..`);
    Fs.readdir(basePath, (err, files) => {
      if(err) {
        return done(err);
      }
      if(undefined === files || 0 === files.length) {
        return done();
      }
      let firstError = false;
      let pendings = files.length;
      const addFoundNode = (nodeDatas, name, shortName) => {
        if(name.startsWith('actor')) {
          nodeDatas.push(new NodeData(name, shortName, 'local'));
        }
        else if(name.startsWith('node-')) {
          nodeDatas.push(new NodeData(name, shortName, 'node'));
        }
        else if(name.startsWith('persistant-node-')) {
          nodeDatas.push(new NodeData(name, shortName, 'persistant-node', 'auto'));
        }
      };
      files.forEach((dir) => {
        Fs.lstat(`${basePath}${Path.sep}${dir}`, (err, stat) => {
          if(!firstError) {
            if(stat.isDirectory()) {
              if(dir.startsWith('actor') || dir.startsWith('node-') || dir.startsWith('persistant-node-')) {
                let sp = this.nodesPersistent.get(dir);
                if(undefined === sp) {
                  ++pendings;
                  Fs.readFile(`${basePath}${Path.sep}${dir}${Path.sep}package.json`, 'utf8', (err, data) => {
                    if(!err) {
                      const packageJson = JSON.parse(data);
                      this.nodesPersistent.set(dir, packageJson);
                      addFoundNode(nodesFound, dir, packageJson.shortname);
                    }
                    if(0 === --pendings) {
                      if(0 === --topPendings) {
                        if(cb) {
                          this._calculateNodes(nodesStatus, nodesFound, cb);
                        }
                      }
                    }
                  });
                }
                else {
                  addFoundNode(nodesFound, dir, sp.shortname);
                }
              }
            }
            if(0 === --pendings) {
              if(0 === --topPendings) {
                if(cb) {
                  this._calculateNodes(nodesStatus, nodesFound, cb);
                }
              }
            }
          }
        });
      });
    });
  }
  
  start(cbStatus) {
    const supervisor = this._getSupervisor();
    if('stopped' === supervisor.state) {
      supervisor.state = 'starting';
      cbStatus();
      this._startSuperVisor((err) => {
        const supervisor = this._getSupervisor();
        if(err) {
          supervisor.status = 'failure';
        }
        this._saveStatus((err) => {
          cbStatus();
        });
      }, () => {
        this._saveStatus((err) => {
          cbStatus();
        });
      });
    }
    else {
      ddb.warning('DROPPED START');
    }
  }
  
  stop(cbStatus) {
    const supervisor = this._getSupervisor();
    if('started' === supervisor.state) {
      supervisor.state = 'stopping';
      cbStatus();
      process.nextTick(() => {
        const supervisor = this._getSupervisor();
        const messageChannel = this._getMessageChannel(supervisor.name);
        if(messageChannel) {
          const request = messageChannel.request(new ServiceAction().addRequest('NodeSurveillanceStop', this.nodeDatas), undefined, (response) => {
            response.responses.forEach((resp) => {
              resp.data.forEach((d) => {
                const foundNodeData = this.nodeDatas.find((nodeData) => {
                  return nodeData.name === d.name;
                });
                if(undefined !== foundNodeData) {
                  foundNodeData.pid = d.pid;
                  foundNodeData.state = -1 === d.pid ? 'stopped' : 'started';
                }
              });
              this._saveStatus((err) => {
                this.cbStatus();
              });
            });
            try {
              process.kill(supervisor.pid);
            }
            catch(e) {}
            supervisor.state = 'stopped';
            supervisor.pid = -1;
            supervisor.ping = 0;
            this._saveStatus((err) => {
              cbStatus();
            });
          });
        }
      });
    }
    else {
      ddb.warning('DROPPED STOP');
    }
  }
  
  command(nodeName, nodeStatus, cbStatus) {
    console.log(nodeName, nodeStatus);
    const foundNodeData = this.nodeDatas.find((nodeData) => {
      return nodeData.shortName === nodeName || nodeData.name === nodeName;
    });
    if(undefined !== foundNodeData) {
      if(foundNodeData.status !== nodeStatus) {
        if('on' === nodeStatus || 'off' === nodeStatus) {
          foundNodeData.status = nodeStatus;
          this._saveStatus((err) => {
            cbStatus();
          });
        }
      }
    }
    else {
      ddb.writelnTime(ddb.red('Unknown node:'), `'${nodeName}'`);
    }
  }
  
  _getNode(name) {
    return this.nodeDatas.find((nodeData) => {
      return name === nodeData.name;
    });
  }
  
  _getSupervisor() {
    return this._getNode('persistant-node-supervisor');
  }
  
  _handleStatusQueue(cbContinue) {
    if(!this.nodesAtom.lock) {
      this.nodesAtom.lock = true;
      const done = () => {
        if(0 !== this.nodesAtom.queue.length) {
          process.nextTick(() => {
            try {
              this.nodesAtom.queue.shift()(done);
            } catch(a) {ddb.error(a);}
          });
        }
        else {
          this.nodesAtom.lock = false;
        }
      };
      cbContinue(done);
    }
    else {
      this.nodesAtom.queue.push(cbContinue);
    }
  }
  
  _readStatus(cb) {
    this._handleStatusQueue((cbDone) => {
      Fs.readFile(ActorPathGenerated.getNodesFile(), 'utf8', (err, data) => {
        cb(err, data);
        cbDone();
      });
    });
  }
    
   
  _saveStatus(cb) {
    this._handleStatusQueue((cbDone) => {
      Fs.writeFile(ActorPathGenerated.getNodesFile(), JSON.stringify({
        nodeDatas: this.nodeDatas
      }, null, 2), 'utf8', (err) => {
        cb(err);
        cbDone();
      });
    });
  }
  
  _mergeNodeData(sStatus, sFound) {
    if(undefined === sStatus) {
      this.nodeDatas.push(sFound);
    }
    else if(undefined === sFound) {
      this.nodeDatas.push(sStatus);
      sStatus.status = 'no repo';
    }
    else {
      this.nodeDatas.push(sStatus);
    }
  }
  
  _mergeNodeDatas(nodesStatus, nodesFound) {
    while(0 !== nodesStatus.length) {
      const sStatus = nodesStatus.shift();
      const sFoundIndex = nodesFound.findIndex((s) => {
        return s.name === sStatus.name;
      });
      if(-1 !== sFoundIndex) {
        const sFound = nodesFound[sFoundIndex];
        nodesFound.splice(sFoundIndex, 1);
        this._mergeNodeData(sStatus, sFound);
      }
      else {
        this._mergeNodeData(sStatus);
      }
    }
    while(0 !== nodesFound.length) {
      const sFound = nodesFound.shift();
      const sStatusIndex = nodesStatus.findIndex((s) => {
        return s.name === sFound.name;
      });
      if(-1 !== sStatusIndex) {
        const sStatus = nodesStatus[sStatusIndex];
        nodesStatus.splice(sStatusIndex, 1);
        this._mergeNodeData(sStatus, sFound);
      }
      else {
        this._mergeNodeData(undefined, sFound);
      }
    }
    this.nodeDatas.sort((a, b) => {
      if(a.name > b.name) {
        return 1;
      }
      else if(a.name < b.name) {
        return -1;
      }
      else {
        return 0;
      }
    });
  }
  
  _getMessageChannel(name) {
    const messageChannel = this.messageChannels.get(name);
    if(!messageChannel) {
      ddb.error(`Could not found message channel to: '${name}'. Message dropped.`);
    }
    return messageChannel;
  }
  
  _startedState() {
    if(1 !== this.messageChannels.size) {
      if(0 === this.messageChannels.size) {
        this.serverStatus.connected = false;
      }
      return;
    }
    const supervisor = this._getSupervisor();
    const messageChannel = this._getMessageChannel(supervisor.name);
    if(messageChannel) {
      const request = messageChannel.request(new ServiceAction('NodeSurveillanceInfo', this.nodeDatas), undefined, (response) => {
        response.responses.forEach((resp) => {
          resp.data.forEach((d) => {
            const foundNodeData = this.nodeDatas.find((nodeData) => {
              return nodeData.name === d.name;
            });
            if(undefined !== foundNodeData) {
              foundNodeData.pid = d.pid;
              foundNodeData.state = -1 === d.pid ? 'stopped' : 'started';
            }
          });
          this._saveStatus((err) => {
            this.cbStatus();
          });
        });
      });
    }
  }
  
  _stoppedState() {
    
  }
  
  _calculateNodes(nodesStatus, nodesFound, cb) {
    this.nodeDatas = [];
    try {
      this._mergeNodeDatas(nodesStatus, nodesFound);
      const supervisor = this._getSupervisor();
      switch(supervisor.state) {
        case 'stopped':
          this._stoppedState();
          break;
        case 'stopping':
          break;
        case 'started':
          this._startedState();
          break;
        case 'starting':
          break;
      };
    }
    catch(err) {
      ddb.error('err', err);
    }
    process.nextTick(() => {
      cb();
    });
  }
  
  _startSuperVisor(cb, cbStatus) {
    try {
      const supervisor = this._getSupervisor();
      supervisor.state = 'starting';
      cbStatus();
     // ddb.writeln'START', Path.resolve(`${ActorPath.getActorPath()}${Path.sep}..${Path.sep}persistant-node-supervisor`), `zass --dev --pp ${this.port} --ph ${this.host}`);
      /*this.supervisorChildProcess = ChildProcess.exec(`${supervisor.shortName} --dev --pp ${this.port} --ph ${this.host}`, {
        cwd: Path.resolve(`${ActorPath.getActorPath()}${Path.sep}..${Path.sep}${supervisor.name}`)
      });*/
    
    const cwd = Path.resolve(`${ActorPath.getActorPath()}${Path.sep}..${Path.sep}${supervisor.name}`);
    this.supervisorChildProcess = ChildProcess.spawn('node', [
        require.resolve('.'),
        '--main', Path.join(cwd, 'main.js'),
        '--cwd', cwd,
        '--dev',
        '--pp',  `${this.port}`,
        '--ph',  this.host
      ],
      {
        detached: true,
        stdio: 'ignore',
        cwd: cwd
      });
      this.supervisorChildProcess.unref();
      cbStatus();
      this.supervisorChildProcess.on('exit', (number, signal) => {
        this.supervisorChildProcess = null;
      });
      process.nextTick(() => {
        cb();
      });
    }
    catch(a) {
      ddb.error('a');
    }
  }
  
  _jsonParse(s) {
    try {
      return JSON.parse(s, null, 2);
    }
    catch(err) {
      ddb.error(ddb.red('\r\r\r\rDROPPED incomming message. JSON.parse Error:'), err);
    }
  }
  
  startListen(cb) {
    try {
      if(null !== this.server) {
        return process.nextTick(() => {
          cb();
        });
      }
      this.server = Net.createServer({allowHalfOpen: true}, (socket) => {
        const socketData = {
          name: 'unknown'
        };
        socket.on('data', (data) => {
          const fromChild = this._jsonParse(data);
          if(undefined === fromChild) {
            return;
          }
          if(this.initialized) {
            if(CoreProtocolConst.RESPONSE === fromChild.type) {
              this.messageChannel = this._getMessageChannel(socketData.name);
              if(this.messageChannel) {
                this.messageChannel.response(fromChild);
              }
            }
            else {
              ddb.warning(ddb.yellow('Messages is not iplemented yet.'));
            }
          }
          else {
            if('initialize' === fromChild.type) {
              this.serverStatus.connected = true;
              this.serverStatus.connectedName = fromChild.name;
              socketData.name = fromChild.name;
              this.messageChannels.set(socketData.name, new MessageChannel(socket));
              const supervisor = this._getSupervisor();
              supervisor.state = 'started';
              supervisor.pid = fromChild.pid;
              supervisor.ping = 0;
              socket.write(JSON.stringify({type: 'initialize', result: true}));
              this.initialized = true;
              this._saveStatus((err) => {
                this.cbStatus();
              });
            }
          }
        });
        socket.on('error', (err) => {
        });
        socket.on('close', () => {
          this.initialized = false;
          this.messageChannels.delete(socketData.name);
          this.serverStatus.connected = false;
          this.serverStatus.connectedName = '';
          const supervisor = this._getSupervisor();
          supervisor.pid = -1;
          this._saveStatus((err) => {
                this.cbStatus();
              });
        });
      });
      this.server.on('error', (err) => {
        this.serverUpDate = null;
        this.serverDownReason = err.message;
        this.server.close();
        this.server = null;
        cb();
      });
      this.server.listen({
        host: this.host,
        port: this.port,
        exclusive: true
      }, () => {
        this.serverUpDate = new Date();
        cb();  
      });
    }
    catch(err) {
      ddb.error(err);
      cb(err);
    }
  }
}
    

module.exports = NodeAdmin;
