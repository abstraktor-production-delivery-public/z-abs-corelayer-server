
'use strict';


class IpSubscription {
  constructor() {
    this.subscribers = new Map();
  }
  
  subscribeOnConnectionEvents(guid, eventType, name, cb) {
    const key = `${eventType}_${name}`;
    if(!this.subscribers.has(key)) {
      this.subscribers.set(key, new Map([[guid, cb]]));
    }
    else {
      const subscribers = this.subscribers.get(key);
      if(subscribers) {
        subscribers.set(guid, cb);
      }
    }
  }
  
  unsubscribeOnConnectionEvents(guid, eventType, name) {
    const key = `${eventType}_${name}`;
    const subscribers = this.subscribers.get(key);
    if(subscribers) {
      subscribers.delete(guid);
      if(0 === subscribers.size) {
        this.subscribers.delete(key);
      }
    }
  }
  
  event(eventType, name, eventId, data) {
    const key = `${eventType}_${name}`;
    const subscribers = this.subscribers.get(key);
    if(subscribers) {
      subscribers.forEach((cb) => {
        cb(eventId, data);
      });
    }
  }
}


module.exports = IpSubscription;
