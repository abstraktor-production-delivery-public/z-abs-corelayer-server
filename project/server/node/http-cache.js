
'use strict';

const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');
const MimeTypes = require('mime-types');
const Fs = require('fs');
const Path = require('path');


class HttpCache {
  constructor() {
    this.pathsArray = [];
    this.cacheContent = new Map();
    this.watchPaths = new Set();
  }
  
  setPaths(watchPaths) {
    this.pathsArray.push(watchPaths); 
    watchPaths.forEach((watchPath) => {
      if(!this.watchPaths.has(watchPath)) {
        Fs.watch(watchPath, {persistent: false, recursive: true}, (eventType, filename) => {
          if(filename && 'change' === eventType) {
            const file = `${watchPath}/${filename.replace(/\\/g, '/')}`;
            this.cacheContent.delete(file);
          }
        });
        this.watchPaths.add(watchPath);
      }
    });
    return this.pathsArray.length - 1;
  }
  
  get(pathIndex, req, file, compressed, cb) {
    const paths = this.pathsArray[pathIndex];
    if(0 == paths.length) {
      cb(false, null);
    }
    else {
      const pragma = req.headers['pragma'];
      if('no-cache' === pragma) {
        this.cacheContent.delete(file);
      }
      else {
        
      }
      this._getContent(file, paths, compressed, (data) => {
        if(!data) {
          cb(true, null);
          return;
        }
        const ifNoneMatch = req.headers['if-none-match'];
        if(ifNoneMatch === data.eTag) {
          cb(false, data);
          return;
        }
        const ifModifiedSince = req.headers['if-modified-since'];
        if(ifModifiedSince === data.lastModified) {
          cb(false, data);
          return;
        }
        cb(true, data);
      });
    }
  }
  
  _getContent(file, paths, compressed, cb) {
    const content = this.cacheContent.get(file);
    if(content) {
      cb(content);
    }
    else {
      let found = false;
      let index = 0;
      let pendings = paths.length;
      while(!found && index < paths.length) {
        const fileAndPath = `${paths[index++]}${file}${compressed ? '.gzip' : ''}`;
        Fs.readFile(fileAndPath, (err, data) => {
          if(!err) {
            if(!found) {
              found = true;
              const foundContent = {
                content: data,
                contentLength: Buffer.byteLength(data),
                mimeType: MimeTypes.lookup(file),
                lastModified: new Date().toUTCString(),
                eTag: `W/"${GuidGenerator.create()}"`
              };
              this.cacheContent.set(file, foundContent);
              cb(foundContent);
            }
            --pendings;
          }
          else {
            if(0 === --pendings) {
              if(!found) {
                cb(null);
              }
            }
          }
        });
      }
    }
  }
}


module.exports = HttpCache;
