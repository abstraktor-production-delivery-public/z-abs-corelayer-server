
'use strict';

const HttpRequestHandler = require('./http-request-handler');
const Http = require('http');


class HttpServer {
  constructor(serverData, nodeName, nodeId, httpCache, dataResponse, defaultSite) {
    this.serverData = serverData;
    this.nodeName = nodeName;
    this.nodeId = nodeId;
    this.httpRequestHandler = new HttpRequestHandler(httpCache, dataResponse, defaultSite);
    this.pathsId = -1;
  }
  
  start(httpData, cb) {
    this.httpRequestHandler.init(httpData, false);
    const server = Http.createServer((req, res) => {
      this.httpRequestHandler.handle(req, res);
    });
    server.on('listening', () => {
      ddb.writelnTime(ddb.cyan('Server:   '), `${this.nodeName}-${httpData.name} `, ddb.green('started at '), `http://${httpData.host}:${httpData.port}`);
      cb();
    });
	  server.on('error', (err) => {
      ddb.writelnTimeError(err, ddb.red('Server: '), `'${this.nodeName}-${httpData.name}' `, ddb.red('did not start at '), `http://${httpData.host}:${httpData.port }`, ddb.red(',error: '), err.message);
      cb(err);
    });
    server.listen(httpData.port, httpData.host);
  }
}


module.exports = HttpServer;
