
'use strict';

const Const = require('./const');
const DataResponse = require('../data-response');
const PluginFactoryProtocol = require('../plugin-factor-protocol');


class IpClient {
  static STATE_CONNECTING = 1;
  static STATE_CONNECTED = 2;
  static STATE_NOT_CONNECTED = 3;
  
  constructor(client, nodeName, nodeId, dataResponse, protocols) {
    this.client = client;
    this.nodeName = nodeName;
    this.nodeId = nodeId;
    this.dataResponse = dataResponse;
    this.protocols = protocols ? protocols : [];
    this.pluginFactoryProtocol = new PluginFactoryProtocol();
    this.data = null;
    this.state = IpClient.STATE_NOT_CONNECTED;
    this.timerId = -1;
    this.downTicks = 0;
    this.connectionData = null;
  }
  
  start(data, cb) {
    this.data = data;
    this.pluginFactoryProtocol.load(this.protocols, () => {
      this._connectionHandler(cb);
    });
  }
  
  stop(cb) {
    if(-1 !== this.timerId) {
      clearTimeout(this.timerId);
      this.timerId = -1;
    }
    this.data = null;
    process.nextTick(cb);
  }
  
  _connectionHandler(cb) {
    if(IpClient.STATE_NOT_CONNECTED === this.state) {
      this.state = IpClient.STATE_CONNECTING;
      this._connect((connected) => {
        if(!connected) {
          this.state = IpClient.STATE_NOT_CONNECTED;
          this.downTicks += 1;
          this.timerId = setTimeout(() => {
            this.timerId = -1;
            this._connectionHandler();
          }, 1000);
        }
        else {
          this.state = IpClient.STATE_CONNECTED;
          this.downTicks = 0;
        }
        if(cb) {
          cb();
        }
      });
    }
  }
  
  _connect(cbConnected) {
    this.client.onConnect(this.data, cbConnected);
  }
  
  onConnected(connectionAddress) {
    //ddb.writelnTime(ddb.blueBright('Client:   '), ddb.green('connected '), this.nodeName, ddb.yellow('::'), this.nodeId, ddb.green(' to '), this.data.name, ddb.green(' at'), ` tcp://${this.data.host}:${this.data.port}`);
    this.connectionData = this.dataResponse.onConnectionConnected(Const.CONNECTION_TYPE_CLIENT_IP, (buffer) => {
      this.client.onSend(buffer);
    });
    connectionAddress.setData(this.connectionData.id, this.client.transport);
    this.connectionData.setConnectionInit(this.client.name, connectionAddress);
    this.pluginFactoryProtocol.register(this.connectionData.channelInput, this.connectionData.channelOutput);
  }
  
  onData(msgIn) {
    this.dataResponse.onMessage(msgIn, this.connectionData);
  }
  
  onClose() {
    //ddb.writelnTime(ddb.blueBright('Client:   '), ddb.red('disconnected '), this.nodeName, ddb.yellow('::'), this.nodeId, ddb.red(' from '), this.data.name, ddb.red(' at'), ` tcp://${this.data.host}:${this.data.port}`);
    this.connectionData.closed();
    this.dataResponse.onConnectionClose(Const.CONNECTION_TYPE_CLIENT_IP, this.connectionData);
    this.state = IpClient.STATE_NOT_CONNECTED;
  }

  onError(err) {
    this.state = IpClient.STATE_NOT_CONNECTED;
  }
}


module.exports = IpClient;
