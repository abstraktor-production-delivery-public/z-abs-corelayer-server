
'use strict';

const Const = require('./const');
const DataResponse = require('../data-response');
const PluginFactoryProtocol = require('../plugin-factor-protocol');


class IpServer {
  constructor(server, nodeName, nodeId, dataResponse, protocols) {
    this.server = server;
    this.nodeName = nodeName;
    this.nodeId = nodeId;
    this.dataResponse = dataResponse;
    this.protocols = protocols ? protocols : [];
    this.pluginFactoryProtocol = new PluginFactoryProtocol();
    this.data = null;
  }
  
  start(data, cb) {
    this.data = data;
    this.pluginFactoryProtocol.load(this.protocols, () => {
      this.server.onListen(this.data, cb);
    });
  }
  
  stop(cb) {
    this.data = null;
    process.nextTick(cb);
  }
  
  onListen(transport, err) {
    if(!err) {
      ddb.writelnTime(ddb.cyan('Server:   '), this.nodeName, ddb.yellow('::'), this.nodeId, ddb.yellow('::'), this.data.name, ddb.green(' started at '), `${Const.TRANSPORT_AS_TEXT[transport]}://${this.data.host}:${this.data.port}`);
    }
    else {
      ddb.writelnTimeError(err, ddb.cyan('Server:   '), this.nodeName, ddb.yellow('::'), this.nodeId, ddb.yellow('::'), this.data.name, ddb.red(' did not start at'), ` ${Const.TRANSPORT_AS_TEXT[transport]}://${this.data.host}:${this.data.port}`);
    }
  }
  
  onConnected(connectionAddress, cbMessage) {
    const connectionData = this.dataResponse.onConnectionConnected(Const.CONNECTION_TYPE_SERVER_IP, (buffer) => {
      cbMessage(buffer);
    });
    connectionAddress.setData(connectionData.id, this.server.transport);
    connectionData.setConnectionInit(this.server.name, connectionAddress);
    this.pluginFactoryProtocol.register(connectionData.channelInput, connectionData.channelOutput);
    this.dataResponse.sendServiceInitRequest(connectionData);
    return connectionData;
  }
  
  onClose(connectionData) {
    connectionData.closed();
    this.dataResponse.onConnectionClose(Const.CONNECTION_TYPE_SERVER_IP, connectionData);
  }
  
  onData(msgIn, connectionData) {
    this.dataResponse.onMessage(msgIn, connectionData);
  }
}


module.exports = IpServer;
