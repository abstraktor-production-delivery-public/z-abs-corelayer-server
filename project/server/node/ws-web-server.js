
'use strict';

const Const = require('./const');
const DataResponse = require('../data-response');
const PluginFactoryProtocol = require('../plugin-factor-protocol');
const WebSocketServer = require('ws').Server;


class WsWebServer {
  constructor(serverData, nodeName, nodeId, dataResponse) {
    this.serverData = serverData;
    this.nodeName = nodeName;
    this.nodeId = nodeId;
    this.dataResponse = dataResponse;
    this.protocols = serverData.protocols;
    this.pluginFactoryProtocol = new PluginFactoryProtocol();
  }
  
  start(wsData, cb) {
    this.pluginFactoryProtocol.load(this.protocols, () => {
      wsData.server = new WebSocketServer({
        perMessageDeflate: false,
        host: wsData.host,
        port: wsData.port
      }, () => {
        ddb.writelnTime(ddb.cyan('Server:   '), `${this.nodeName}-${wsData.name}`, ddb.green(' started at'), ` http://${wsData.host}:${wsData.port}`);
        cb();
      });
      wsData.server.on('connection', (ws) => {
        ws.on('message', (msgIn) => {
          this.dataResponse.onMessage(msgIn, connectionData);
        });
        ws.on('close', () => {
          this.dataResponse.onConnectionClose(Const.CONNECTION_TYPE_SERVER_WEB, connectionData);
        });
        const connectionData = this.dataResponse.onConnectionConnected(Const.CONNECTION_TYPE_SERVER_WEB, (buffer) => {
          ws.send(buffer);
        });
        this.pluginFactoryProtocol.register(connectionData.channelInput, connectionData.channelOutput);
      });
      wsData.server.on('error', (err) => {
        const date = new Date();
        ddb.writelnTimeError(err, ddb.red('Server:'), ` '${this.nodeName}-${wsData.name}'`, ddb.red(' did not start at'), ` http://${wsData.host}:${wsData.port}` + ddb.red('. Error: '), err.message);
        cb(err);
      });
    });
  }
}


module.exports = WsWebServer;
