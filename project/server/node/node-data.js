
'use strict';


class NodeData {
  constructor(name, shortName, type, status='off', state='stopped', pid=-1, ping=0, statusTicks=0, servers='0 - 0', clients='0 - 0') {
    this.name = name;
    this.shortName = shortName;
    this.type = type;
    this.state = state;
    this.pid = pid,
    this.servers = servers;
    this.clients = clients;
    this.status = status;
    this.ping = ping;
    this.statusTicks = statusTicks;
  }
  
  static from(nodeData) {
    return new NodeData(nodeData.name, nodeData.shortName, nodeData.type, nodeData.status, nodeData.state, nodeData.pid, nodeData.ping, nodeData.statusTicks, nodeData.servers, nodeData.clients);
  }
}


module.exports = NodeData;
