
'use strict';

const Const = require('./const');
const TcpClient = tryRequire('z-abs-corelayer-bs/server/node/tcp-client');


class Clients {
  constructor(nodeName, nodeId, nodeSettings, dataResponse) {
    this.nodeName = nodeName;
    this.nodeId = nodeId;
    this.nodeSettings = nodeSettings;
    this.dataResponse = dataResponse;
    this.clients = new Map();
  }
  
  create() {
    if(0 === this.nodeSettings.tcpClients.length) {
      return;
    }
    if(TcpClient && 0 !== this.nodeSettings.tcpClients.length) {
      this.nodeSettings.tcpClients.forEach((tcpClientData) => {
        this._createIpClient(Const.TRANSPORT_TCP, this.dataResponse, tcpClientData);
      });
    }
    const dependencyServiceNames = [];
    this.clients.forEach((client, key) => {
      if(client.serverData.dependency) {
        dependencyServiceNames.push(key);
      }
    });
    dependencyServiceNames.sort();
    this.dataResponse.externalServices.addDependencyNodeNames(dependencyServiceNames);
  }
  
  start(cb) {
    let pendings = this.clients.size;
    if(0 === pendings) {
      process.nextTick(cb);
      return;
    }
    this.clients.forEach((client) => {
      client.start(client.serverData, () => {
        if(0 === --pendings) {
          cb();
        }
      });
    });
  }
  
  subscribeOnConnectionEvents(guid, eventType, name, cb) {
    const client = this.clients.get(name);
    if(client) {
      client.ipServer.dataResponse.ipSubscription.subscribeOnConnectionEvents(guid, eventType, name, cb);
    }
  }
  
  unsubscribeOnConnectionEvents(guid, eventType, name) {
    const client = this.clients.get(name);
    if(client) {
      client.ipServer.dataResponse.ipSubscription.unsubscribeOnConnectionEvents(guid, eventType, name);
    }
  }
  
  _createIpClient(type, channel, clientData) {
    let client = null;
    if(Const.TRANSPORT_TCP === type) {
      client = new TcpClient(clientData.name, this.nodeName, this.nodeId, channel, clientData);
    }
    if(client) {
      this.clients.set(clientData.name, client);
	}
	else {
      console.log('ERROR');
	}
  }
}


module.exports = Clients;
