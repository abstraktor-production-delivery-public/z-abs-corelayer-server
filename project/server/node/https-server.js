
'use strict';

const HttpRequestHandler = require('./http-request-handler');
const Fs = require('fs');
const Https = require('https');


class HttpsServer {
  constructor(serverData, nodeName, nodeId, httpCache, dataResponse, defaultSite) {
    this.serverData = serverData;
    this.nodeName = nodeName;
    this.nodeId = nodeId;
    this.httpRequestHandler = new HttpRequestHandler(httpCache, dataResponse, defaultSite);
  }
  
  start(httpsData, cb) {
    this.httpRequestHandler.init(httpsData, true);
    const options = this._readKeys(httpsData);
    if(options) {
      const server = Https.createServer(options, (req, res) => {
        this.httpRequestHandler.handle(req, res);
      });
      server.on('listening', () => {
        ddb.writelnTime(ddb.cyan('Server:   '), ` ${this.nodeName}-${httpsData.name}`, ddb.green(' started at'), ` http://${httpsData.host}:${httpsData.port}`);
        cb();
      });
	    server.on('error', (err) => {
        ddb.writelnTimeError(err, ddb.red('Server:'), ` '${this.nodeName}-${httpsData.name}'`, ddb.red(' did not start at'), ` http://${httpsData.host}:${httpsData.port}` + ddb.red('. Error: '), err.message);
        cb(err);
      });
      server.listen(httpsData.port, httpsData.host);
    }
    else {
      const err = new Error('-----');
      ddb.writelnTimeError(err, ddb.red('Server:'), ` '${this.nodeName}-${httpsData.name}'`, ddb.red(' did not start at'), ` http://${httpsData.host}:${httpsData.port}` + ddb.red('. No SSL keys.'));
      cb(err);
    }
  }
  
  _readKeys(httpsData) {
    if(httpsData.keyFile && httpsData.certFile) {
      try {
        const keyFile = Fs.readFileSync(httpsData.keyFile);
        const certFile = Fs.readFileSync(httpsData.certFile);
        return {
          key: keyFile,
          cert: certFile
        };
      }
      catch(err) {
        ddb.writelnTimeError(ddb.red(`Could not get cert and key file, error:`), err);
        return null;
      }
    }
    else {
      return null;
    }
  }
}


module.exports = HttpsServer;
