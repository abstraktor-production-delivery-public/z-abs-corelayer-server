
'use strict';


class HttpRequestHandler {
  constructor(httpCache, dataResponse, defaultSite) {
    this.httpCache = httpCache;
    this.dataResponse = dataResponse;
    this.defaultSite  = defaultSite.startsWith('/') ? defaultSite : `/${defaultSite}`;
    this.baseUrl = '';
    this.pathId = -1;
    this.authenticationToken = `token-${this.dataResponse.appName}=`
    this.production = 'production' === process.env.NODE_ENV;
    this.content = [
      {
        search: '/assets/',
        route: '',
        remove: ''
      },
      {
        search: '/skin-win8/',
        route: '',
        remove: '/skin-win8'
      },
      {
        search: '/fonts/',
        route: '',
        remove: '/fonts'
      }
    ];
  }
  
  _getToken(cookie) {
     let token = '';
     if(cookie) {
      let startIndex = cookie.indexOf(this.authenticationToken);
      if(-1 !== startIndex) {
        startIndex += this.authenticationToken.length;
        const stopIndex = cookie.indexOf(';', startIndex);
        token = cookie.substring(startIndex, -1 !== stopIndex ? stopIndex : undefined);
      }
    }
  }
  
  handle(req, res) {
    const myURL = new URL(req.url, this.baseUrl);
    if(myURL.pathname.startsWith('/abs-data/')) {
      try {
        if(req.method == 'POST') {
          const rawBody = [];
          req.on('data', (chunk) => {
            rawBody.push(chunk);
          });
          req.on('end', () => {
            const token = this._getToken(req.headers.cookie);
            const requestObject = this._safeJsonParse(Buffer.concat(rawBody).toString());
            if(null !== requestObject) {
              this.dataResponse.handleRequests(requestObject, token, null, (response, dataBuffers, debug, token) => {
                if(!token) {
                  this.jsonResponseHttp(res, JSON.stringify(response));
                }
                else {
                  this.jsonLoginResponseHttp(res, token, JSON.stringify(response));
                }
              }, (message) => {
                ddb.warning('DROPPED MSG:');
              });
            }
            else {
              ddb.error('BAD REQ:');
              res.writeHead(400, 'Bad Request', {'Content-Length': '0'});
              res.end();
            }
          });
          return;
        }
      }
      catch(err) {
        ddb.error(err);
        this.notFound(res);
        return;
      }
      this.notImplementedServerError(res);
    }
    else if(myURL.pathname.startsWith('/abs-scripts/')) {
      this.contentResponse(req, res, `/${myURL.pathname.substring('/abs-scripts/'.length)}`, true);
    }
    else if(myURL.pathname.startsWith('/abs-css/')) {
      this.contentResponse(req, res, `/${myURL.pathname.substring('/abs-css/'.length)}`, true);
    }
    else if(myURL.pathname.startsWith('/abs-images/')) {
      this.contentResponse(req, res, `/${myURL.pathname.substring('/abs-images/'.length)}`);
    }
    else {
      const foundIndex = this.content.findIndex((content) => (-1 != myURL.pathname.indexOf(content.search)));
      if(-1 !== foundIndex) {
        const content = this.content[foundIndex];
        const url = `${content.route}${myURL.pathname.substring(myURL.pathname.indexOf(content.search) + content.remove.length)}`;
        this.contentResponse(req, res, url);
      }
      else if(myURL.pathname === '/favicon.ico') {
        this.contentResponse(req, res, '/svg/AbstraktorA.svg');
      }
      else if(myURL.pathname === '/') {
        this.redirect(res, this.defaultSite);
      }
      else if(myURL.pathname.startsWith('/abs-api/')) {
        const rawBody = [];
        req.on('data', (chunk) => {
          rawBody.push(chunk);
        });
        req.on('end', () => {
          const token = this._getToken(req.headers.cookie);
          this.dataResponse._handleRest(req.method, myURL.pathname, req.headers['content-type'], rawBody, token, (statusCode, reasonPhrase, contentType, content) => {
            this.restResponse(res, statusCode, reasonPhrase, contentType, content)
          });
        });
      }
      else {
        this.contentResponse(req, res, '/index.html', false, true);
      }
    }
  }
  
  init(data, tls) {
    this.baseUrl = `http${tls ? 's' : ''}://${data.host}:${data.port}`;
    this.domain = data.host;
    this.pathId = this.httpCache.setPaths(data.directories);
  }
  
  contentResponse(req, res, file, compressed, forced) {
    const useCompressed = !!compressed && this.production;
    this.httpCache.get(this.pathId, req, file, useCompressed, (modified, data) => {
      if(data) {
        if(modified || forced) {
          res.setHeader('Accept-Ranges', 'bytes');
          res.setHeader('Cache-Control', 'public, max-age=604800');
          res.setHeader('ETag', data.eTag);
          res.setHeader('Content-Type', data.mimeType + '; charset=utf-8');
          res.setHeader('Date', new Date().toUTCString());
          if(useCompressed) {
            res.setHeader('Content-Encoding', 'gzip');
          }
          res.write(data.content);
          res.end();
        }
        else {
          res.writeHead(304, 'Not Modified', {
            'Cache-Control': 'public, max-age=604800',
            'ETag': data.eTag,
            'Date': new Date().toUTCString()
          });
          res.end();
        }
      }
      else {
        ddb.error('not found:', file);
        this.notFound(res);
      }
    });
  }
  
  jsonResponseHttp(res, data) {
    res.setHeader('Content-Type', 'application/json; charset=utf-8');
    res.write(data);
    res.end();
  }
  
  jsonLoginResponseHttp(res, token, data) {
    res.setHeader('Content-Type', 'application/json; charset=utf-8');
    res.setHeader('Set-Cookie', `${this.authenticationToken}${'-1' === token ? '' : token}; Domain=${this.domain}; Max-Age=${'-1' === token ? '0' : '604800'}; HttpOnly`);
    res.write(data);
    res.end();
  }
  
  restResponse(res, statusCode, reasonPhrase, contentType, content) {
    res.writeHead(statusCode, reasonPhrase);
    if(content) {
      res.setHeader('Content-Type', contentType + '; charset=utf-8');
      if('application/json' === contentType) {
        res.write(JSON.stringify(content, null, 2));
      }
      else {
        res.write(content);
      }
    }
    res.end();
  }
  
  redirect(res, to) {
    res.writeHead(302, {'Location': to});
    res.end();
  }
  
  notImplementedServerError(res) {
    res.writeHead(501, "Not implemented", {'Content-Type': 'text/html; charset=utf-8'});
    res.end(this.errorHtml('501 - Not implemented', '<h1>Not implemented!</h1>'));
  }
  
  notFound(res) {
    res.writeHead(404, 'Not Found', {"Content-Type": "text/html; charset=utf-8"});
    res.end(this.errorHtml('404 - Not Found', '<h1>Not Found!</h1>'));
  }
  
  errorHtml(title, bodyText) {
    return '<!DOCTYPE html><html lang="en-US"><head><title>' + title + '</title><meta charset="UTF-8"></head><body>' + bodyText + '</body></html>';
  }
  
  _safeJsonParse(string) {
    try {
      return JSON.parse(string);
    }
    catch(err) {
      return null;
    }
  }
}

module.exports = HttpRequestHandler;
