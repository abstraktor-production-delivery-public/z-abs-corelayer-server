
'use strict';

const Const = require('./const');
const DataResponse = require('../data-response');
const PluginFactoryProtocol = require('../plugin-factor-protocol');
const WebSocketServer = require('ws').Server;
const Https = require ('https');


class WssWebServer {
  constructor(serverData, nodeName, nodeId, dataResponse) {
    this.serverData = serverData;
    this.nodeName = nodeName;
    this.nodeId = nodeId;
    this.dataResponse = dataResponse;
    this.protocols = serverData.protocols;
    this.pluginFactoryProtocol = new PluginFactoryProtocol();
  }
  
  start(wssData, cb) {
    this.pluginFactoryProtocol.load(this.protocols, () => {
      const options = this._readKeys(wssData);
      if(options) {
        const httpsServer = https.createServer(options);
        server.listen(wssData.port, wssData.host);
        wssData.server = new WebSocketServer({
          perMessageDeflate: false,
          server: httpsServer
        }, () => {
          ddb.writelnTime(ddb.cyan('Server:   '), `${this.nodeName}-${wssData.name}`, ddb.green(' started at'), ` https://${wssData.host}:${wssData.port}`);
          cb();
        });
        wssData.server.on('connection', (ws) => {
          ws.on('message', (msgIn) => {
            this.dataResponse.onMessage(msgIn, connectionData);
          });
          ws.on('close', () => {
            this.dataResponse.onConnectionClose(Const.CONNECTION_TYPE_SERVER_WEB, connectionData);
          });
          const connectionData = this.dataResponse.onConnectionConnected(Const.CONNECTION_TYPE_SERVER_WEB, (buffer) => {
            ws.send(buffer);
          });
          this.pluginFactoryProtocol.register(connectionData.channelInput, connectionData.channelOutput);
        });
        wssData.server.on('error', (err) => {
          const date = new Date();
          ddb.writelnTimeError(err, ddb.red('Server:'), ` '${this.nodeName}-${wssData.name}'`, ddb.red(' did not start at'), ` https://${wssData.host}:${wssData.port}` + ddb.red('. Error: '), err.message);
          cb(err);
        });
      });
    }
    else {
      const err = new Error('-----');
      ddb.writelnTimeError(err, ddb.red('Server:'), ` '${this.nodeName}-${wssData.name}'`, ddb.red(' did not start at'), ` https://${wssData.host}:${wssData.port}` + ddb.red('. No SSL keys.'));
      cb(err);
    }
  }
  
  _readKeys(wssData) {
    if(wssData.keyFile && wssData.certFile) {
      try {
        const keyFile = Fs.readFileSync(wssData.keyFile);
        const certFile = Fs.readFileSync(wssData.certFile);
        return {
          key: keyFile,
          cert: certFile
        };
      }
      catch(err) {
        ddb.writelnTimeError(ddb.red(`Could not get cert and key file, error:`), err);
        return null;
      }
    }
    else {
      return null;
    }
  }
}


module.exports = WssWebServer;
