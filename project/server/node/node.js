
'use strict';

const DebugDashboard = require('z-abs-corelayer-cs/clientServer/debug-dashboard/log');
const ExternalServices = require('./external-services');
const Const = require('./const');
const Clients = require('./clients');
const Servers = require('./servers');
const NodeSettings  = require('./node-settings');
const DataResponse = require('../data-response');
const PluginFactoryProtocol = require('../plugin-factor-protocol');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');
const Net = require('net');


class Node {
  static TYPE_PRODUCTION = 0;
  static TYPE_DEVELOPMENT = 1;
  static TYPE_TEST = 2;
  static TYPE_NPM = 3;
  
  static NODE_OK = ddb.green('Node:     ');
  static NODE_STOP = ddb.yellow('Node:     ');
  static NODE_NOK = ddb.red('Node:     ');
  
  constructor(releaseData, dynamicConfig, settings, build) {
    this.settings = settings;
    this.build = build;
    this.NodeSettings = new NodeSettings(releaseData, dynamicConfig, settings);
    this.nodeName = settings.type ? `${settings.type}-${settings.name}` : settings.name;
    this.nodeId = GuidGenerator.create();
    this.nodeType = this._setNodeType();
    this.pluginDatas = settings.pluginDatas;
    this.pluginServices = settings.pluginServices;
    this.pluginComponents = settings.pluginComponents;
    this.externalServices = new ExternalServices();
    this.dataResponse = new DataResponse(this.nodeName, this.nodeId, this.nodeType, this, this.externalServices);
    this.servers = new Servers(this.nodeName, this.nodeId, this.NodeSettings.settings, this.settings.defaultSite, this.dataResponse);
    this.clients = new Clients(this.nodeName, this.nodeId, this.NodeSettings.settings, this.dataResponse);
    this.initialized = false;
    this.hasCaughtException = false;
    this.workers = new Map();
    if(settings.workers) {
      settings.workers.forEach((worker) => {
        const pluginFactoryProtocol = new PluginFactoryProtocol();
        const protocols = worker.protocols?.server ? worker.protocols.server : '[]';
        pluginFactoryProtocol.load(protocols, () => {});
        this.workers.set(worker.name, {
          worker, 
          pluginFactoryProtocol
        });
      });
    }
    this.nodeStartedSubscriptions = [];
    //console.log('pid:', process.pid, 'ppid:', process.ppid);
  }
  
  run(cb) {
    if(this.build) {
      return cb();
    }
    this.subscribeOnNodeStarted(() => {
      ddb.writelnTime(Node.NODE_OK, this.nodeName, ddb.yellow('::'), this.nodeId, ddb.green(' started.'));      
    });
    this.servers.create();
    this.clients.create();
    this.dataResponse.init(this.pluginDatas, this.pluginServices, this.pluginComponents, this.NodeSettings.settings, this.workers, () => {
      process.on('uncaughtException', this._uncaughtException.bind(this));
      process.on('exit', this._onExit.bind(this));
      process.on('SIGINT', this._onSigint.bind(this));
      this.dataResponse.startServices(() => {
        this.servers.start(() => {
          this.clients.start(() => {
            cb();
            this._publishNodeStarted();
          });
        });
      });
    });
  }
  
  subscribeOnConnectionEvents(guid, eventType, name, cb) {
    if(Const.EVENT_TYPE_CONNECTION === eventType) {
      this.servers.subscribeOnConnectionEvents(guid, eventType, name, cb);
      this.clients.subscribeOnConnectionEvents(guid, eventType, name, cb);
    }
  }
  
  unsubscribeOnConnectionEvents(guid, eventType, name) {
    if(Const.EVENT_TYPE_CONNECTION === eventType) {
      this.servers.unsubscribeOnConnectionEvents(guid, eventType, name);
      this.clients.unsubscribeOnConnectionEvents(guid, eventType, name);
    }
  }
  
  subscribeOnNodeStarted(cb) {
    this.nodeStartedSubscriptions.push(cb);
  }
  
  _setNodeType() {
    if(undefined === this.settings.nodeType) {
      return Const.NODE_TYPE_FRONTEND;
    }
    else if('backend' === this.settings.nodeType) {
      return Const.NODE_TYPE_BACKEND;
    }
    else if('proxy' === this.settings.nodeType) {
      return Const.NODE_TYPE_PROXY;
    }
    else if('surveillance' === this.settings.nodeType) {
      return Const.NODE_TYPE_SURVEILLANCE;
    }
  }
  
  _publishNodeStarted() {
    const cb = this.nodeStartedSubscriptions.shift();
    if(cb) {
      cb();
      process.nextTick(this._publishNodeStarted.bind(this));
    }
  }
  
  _uncaughtException(err, origin) {
    // TODO: Add timoutes.
    ddb.writelnTime(ddb.redBright(' uncaught exception: '), err.message);
    console.log(err);
    this.hasCaughtException = true;
    this.dataResponse.stopServices(true, () => {
      ddb.writelnTime(Node.NODE_STOP, this.nodeName, ddb.yellow('::'), this.nodeId, ddb.green(' stopped in an orderly manner.'));
      process.exit(0xBAD);
    });
  }
  
  _onExit(code) {
    if(this.hasCaughtException && 0xBAD !== code) {
      ddb.writelnTime(Node.NODE_NOK, this.nodeName, ddb.yellow('::'), this.nodeId, ddb.red(' stopped in an unordered manner.'));
    }
    // TODO: Add timoutes.
    /*this.dataResponse.stopServices(true, () => {
      ddb.writelnTime(ddb.green('****** Node '), this.nodeName, ddb.yellow('::'), this.nodeId, ddb.green(' stopped in an orderly manner. ******'));
      ddb.writelnTime(ddb.redBright(' uncaught exception: '));
      process.exit(code);
    });*/
  }
  
  _onSigint(code) {
    // TODO: Add timoutes.
    console.log(code);
    this.dataResponse.stopServices(false, () => {
      ddb.writelnTime(Node.NODE_STOP, this.nodeName, ddb.yellow('::'), this.nodeId, ddb.redBright(' received SIGINT'), ddb.green(' stopped in an orderly manner.'));
      process.exit(0xDEAD);
    });
  }
}


module.exports = Node;
