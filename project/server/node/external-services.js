
'use strict';

const Const = require('./const');


class ExternalServices {
  static emptyMap = new Map();
  static NODE = ddb.cyan('Node:     ');
  
  constructor() {
    this.nodes = new Map();
    this.services = new Map();
    this.dependencyNodeNames = new Set();
    this.eventListeners = new Map();
  }
  
  addDependencyNodeNames(dependencyNodeNames) {
    this.dependencyNodeNames = new Set([...dependencyNodeNames]);
  }
  
  _hasNode(nodeName) {
    for(let [symbol, node] of this.nodes) {
      if(nodeName === node.nodeName) {
        return true;
      }
    }
    return false;
  }
  
  getOfferData(nodeName, nodeSymbol) {
    let offer = true;
    let isDependent = this.dependencyNodeNames.has(nodeName);
    const dependencies = this.dependencyNodeNames.size;
    let connectedDependencies = 0;
    if(isDependent) {
      this.dependencyNodeNames.forEach((dependencyNodeName) => {
        if(this._hasNode(dependencyNodeName)) {
          ++connectedDependencies;
        }
        else {
          offer = false;
        }
      });
    }
    return {
      offer: offer || !isDependent,
      isDependent,
      dependencies,
      connectedDependencies
    };
  }
  
  addNode(connectionData) {
    const externalNode = {
      nodeSymbol: connectionData.nodeSymbol,
      nodeName: connectionData.nodeName,
      nodeType: connectionData.nodeType,
      connectionData: connectionData,
      isDependent: this.dependencyNodeNames.has(connectionData.nodeName),
      offered: false,
      serviceExport: null
    };
    this.nodes.set(connectionData.nodeSymbol, externalNode);
    ddb.writelnTime(ExternalServices.NODE, connectionData.nodeName, ddb.yellow('::'), connectionData.nodeId, ddb.green(' connected'));
    return externalNode;
  }
  
  deleteNode(connectionData) {
    if(this.nodes.delete(connectionData.nodeSymbol)) {
      ddb.writelnTime(ExternalServices.NODE, connectionData.nodeName, ddb.yellow('::'), connectionData.nodeId, ddb.red(' disconnected'));
    }
    this.offerOff(connectionData);
  }
  
  getNodeData(connectionData) {
    return this.nodes.get(connectionData.nodeSymbol);
  }
  
  setNodeOffer(connectionData, serviceExport) {
    const nodeData = this.nodes.get(connectionData.nodeSymbol);
    nodeData.serviceExport = serviceExport;
  }
  
  offerOn(connectionData, serviceExport) {
    serviceExport.frontendServices.forEach((serviceName) => {
      this._offerOn(serviceName, Const.OFFER_FRONTEND, connectionData);
    });
    serviceExport.backendServices.forEach((serviceName) => {
      this._offerOn(serviceName, Const.OFFER_BACKEND, connectionData);
    });
    serviceExport.surveillanceServices.forEach((serviceName) => {
      this._offerOn(serviceName, Const.OFFER_SURVEILLANCE, connectionData);
    });
  }
  
  offerOff(connectionData) {
    connectionData.frontendServices.forEach((serviceName) => {
      this._offerOff(serviceName, connectionData);
    });
    connectionData.backendServices.forEach((serviceName) => {
      this._offerOff(serviceName, connectionData);
    });
    connectionData.surveillanceServices.forEach((serviceName) => {
      this._offerOff(serviceName, connectionData);
    });
  }
  
  _offerOn(serviceName, offerType, connectionData) {
    const externalService = this.services.get(serviceName);
    if(!externalService) {
      connectionData.addService(serviceName, offerType);
      this.services.set(serviceName, new Map([[connectionData.id, {connectionData , instances: 1}]]));
      ddb.writelnTime(ExternalServices.NODE, connectionData.nodeName, ddb.yellow('::'), connectionData.nodeId, ddb.yellow(' offer '), ddb.cyan(serviceName), ddb.green(' on'), ddb.yellow(' id: '), connectionData.id, ', instances: ', ddb.yellow(1));
    }
    else {
      let externalConnectionData = null;
      if(externalService.has(connectionData.id)) {
        externalConnectionData = externalService.get(connectionData.id);
        ++externalConnectionData.instances;
      }
      else {
        connectionData.addService(serviceName, offerType);
        externalConnectionData = {connectionData , instances: 1};
        externalService.set(connectionData.id, externalConnectionData);
      }
      ddb.writelnTime(ExternalServices.NODE, connectionData.nodeName, ddb.yellow('::'), connectionData.nodeId, ddb.yellow(' offer '), ddb.cyan(serviceName), ddb.green(' on'), ddb.yellow(' id: '), connectionData.id, ', instances: ', ddb.yellow(externalConnectionData.instances));
    }
    const serviceListeners = this.eventListeners.get(serviceName);
    if(serviceListeners) {
      serviceListeners.forEach((cb) => {
        process.nextTick(() => {
          cb(connectionData, true);
       });
     });
    }
  }
  
  _offerOff(serviceName, connectionData) {
    const externalService = this.services.get(serviceName);
    if(externalService) {
      const externalConnectionData = externalService.get(connectionData.id);
      if(externalConnectionData) {
        if(0 === --externalConnectionData.instances) {
          connectionData.deleteService(serviceName);
          externalService.delete(connectionData.id);
          if(0 === externalService.size) {
            this.services.delete(serviceName, connectionData.nodeType);
          }
          const serviceListeners = this.eventListeners.get(serviceName);
          if(serviceListeners) {
            serviceListeners.forEach((cb) => {
              process.nextTick(() => {
                cb(connectionData, false);
              });
            });
          }
        }
        ddb.writelnTime(ExternalServices.NODE, connectionData.nodeName, ddb.yellow('::'), connectionData.nodeId, ddb.yellow(' offer '), ddb.cyan(serviceName), ddb.red(' off'), ddb.yellow(' id: '), connectionData.id, ', instances: ', ddb.yellow(externalConnectionData.instances));
      }
      else {
        ddb.writelnTime(ExternalServices.NODE, connectionData.nodeName, ddb.yellow('::'), connectionData.nodeId, ddb.red(' Service: '), ddb.cyan(serviceName), ddb.yellow(' id: '), connectionData.id, ddb.red(' not removed, it does not exist!'));
      }
    }
    else {
      ddb.writelnTime(ExternalServices.NODE, connectionData.nodeName, ddb.yellow('::'), connectionData.nodeId, ddb.red(' Service: '), ddb.cyan(serviceName), ddb.yellow(' not removed, it does not exist!'));
    }
  }
  
  getServices() {
    if(0 !== this.services.size) {
      return [...this.services.keys()];
    }
    else {
      return [];
    }
  }
  
  /*has(serviceName) {
    return this.services.has(serviceName);
  }*/
  
  getConnectionData(serviceName) {
    const externalService = this.services.get(serviceName);
    if(externalService) {
      // ROUND - ROBIN
      const externalConnectionData = externalService.entries().next().value[1];
      if(1 <= externalService.size) {
        externalService.delete(externalConnectionData.connectionData.id);
        externalService.set(externalConnectionData.connectionData.id, externalConnectionData);
      }
      return externalConnectionData.connectionData;
    }
    else {
      return null;
    }
  }
  
  on(serviceName, pluginService, cb) {
    let serviceListeners = this.eventListeners.get(serviceName);
    if(!serviceListeners) {
      serviceListeners = new Map();
      this.eventListeners.set(serviceName, serviceListeners);
    }
    serviceListeners.set(pluginService, cb);
    if(this.services.has(serviceName)) {
      cb();
    }
  }
}


module.exports = ExternalServices;