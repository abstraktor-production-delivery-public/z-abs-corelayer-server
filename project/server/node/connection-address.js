
'use strict';


class ConnectionAddress {
  constructor(localHost, localPort, remoteHost, remotePort, family) {
    this.localHost = localHost;
    this.localPort = localPort;
    this.remoteHost = remoteHost;
    this.remotePort = remotePort;
    this.family = family;
    
    this.connectionDataId = '';
    this.transport = '';
  }
  
  setData(connectionDataId, transport) {
    this.connectionDataId = connectionDataId;
    this.transport = transport;
  }
}


module.exports = ConnectionAddress;
