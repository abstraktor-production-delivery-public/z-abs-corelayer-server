
'use strict';

const Const = require('./const');
const WorkerInitRequest = require('../communication/messages/messages-s-to-s/worker-init-request');
const DataResponse = require('../data-response');
const ConnectionData = require('./connection-data');
const PluginFactoryProtocol = require('../plugin-factor-protocol');
const CoreProtocolConst = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/core-protocol-const');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');


class NodeWorker {
  static id = 0;
  
  constructor() {
    this.nodeName = `WORKER-${++NodeWorker.id}-NODE`
    this.nodeId = GuidGenerator.create();
    this.dataResponse = new DataResponse(this.nodeName, this.nodeId, Const.NODE_TYPE_WORKER, this, null);
    this.channelOutput = null;
    this.channelInput = null;
    this.connectionData = null;
    this.pluginFactoryProtocol = new PluginFactoryProtocol();
  }
  
  init(port, pluginPath, protocols) {
    this.connectionData = new ConnectionData(`NODE-${NodeWorker.id}-CLIENT-IN`, `NODE-${NodeWorker.id}-CLIENT-OUT`, process.send.bind(process));
    let pendings = 2;
    this.dataResponse.pluginDataFactory.load(() => {
      if(0 === --pendings) {
        this._init(port);
      }
    }, pluginPath);
    this.pluginFactoryProtocol.load(protocols, () => {
      this.pluginFactoryProtocol.register(null, this.connectionData.channelOutput);
      if(0 === --pendings) {
        this._init(port);
      }
    });
  }
  
  _init(port) {
    const channelInput = this.connectionData.channelInput; 
    process.on('message', (msg) => {
      channelInput.onNewMessage(Buffer.from(msg.data), true, (coreMessage) => {
      if(CoreProtocolConst.REQUEST === coreMessage.msgId) {
        this.dataResponse.handleRequests(coreMessage.msg, null, null, (response, dataBuffers, debug, token) => {
          this.send(response, dataBuffers, debug, coreMessage.envelope);
        }, (message, dataBuffers, debug, sessionId) => {
          this.send(message, dataBuffers, debug, coreMessage.envelope);
        }, this.connectionData);
      }
      else if(999 <= coreMessage.msgId) {
        const session = this.dataResponse.sessionCache.getSession(coreMessage.msg.sessionId);
        if(session) {
          session.dataPluginsInternal.forEach((dataPlugin) => {
            dataPlugin.handleMessageFromClient(coreMessage.msg);
          });
        }
      }
      else {
        // TODO: log
        console.log('*************************');
      }
      });
    });
    this.send(new WorkerInitRequest(port), null, false, null);
  }
  
  send(msg, dataBuffers, debug, envelope) {
    this.connectionData.channelOutput.send(msg, dataBuffers, debug, envelope);
  }
}


module.exports = NodeWorker;
