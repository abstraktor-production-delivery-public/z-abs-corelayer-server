
'use strict';

const Decoder = require('../communication/core-protocol/decoder');
const DeserializerMessageinit = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/deserializer-message-init');
const DeserializerMessageRequest = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/deserializer-message-request');
const DeserializerMessageResponse = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/deserializer-message-response');
const DeserializerMessageServiceInitRequest = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/deserializer-message-service-init-request');
const TextCache = require('z-abs-corelayer-cs/clientServer/communication/cache/text-cache');
const Deserializer = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/deserializer');
const AppDeserializer = require('z-abs-corelayer-cs/clientServer/communication/app-protocol/app-deserializer');
const CoreProtocolConst = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/core-protocol-const');


class ChannelInput {
  constructor(name) {
    this.name = name;
    this.textCache = new TextCache(name, process.pid);
    this.deserializer = new Deserializer(Decoder, this.textCache);
    this.appDeserializer = new AppDeserializer(new Decoder(this.textCache), new Decoder());
    this.appDeserializer.register(CoreProtocolConst.INIT, null, new DeserializerMessageinit(Deserializer.jsonFunc));
    this.appDeserializer.register(CoreProtocolConst.REQUEST, null, new DeserializerMessageRequest(Deserializer.jsonFunc));
    this.appDeserializer.register(CoreProtocolConst.RESPONSE, null, new DeserializerMessageResponse(Deserializer.jsonFunc));
    this.appDeserializer.register(CoreProtocolConst.SERVICE_INIT_REQUEST, null, new DeserializerMessageServiceInitRequest(Deserializer.jsonFunc));
  }
  
  register(id, name, deserializer) {
    this.appDeserializer.register(id, name, deserializer);
  }
  
  onMessage(msg, open) {
    const coreMessage = this.deserializer.do(msg);
    if(coreMessage) {
      if(open) {
        if(coreMessage.isBinary) {
          if(coreMessage.hasTextCache) {
            this.appDeserializer.doCachedText(coreMessage.cachedTextBuffer);
          }
          coreMessage.msg = this.appDeserializer.do(coreMessage.msgId, coreMessage.bin, coreMessage.isServiceAction);
        }
      }
      return coreMessage;
    }
    else {
      return null;
    }
  }
  
  onNewMessage(msg, open, cbMessage) {
    const coreMessage = this.deserializer.do(msg);
    if(coreMessage) {
      if(open) {
        if(coreMessage.isBinary) {
          if(coreMessage.hasTextCache) {
            this.appDeserializer.doCachedText(coreMessage.cachedTextBuffer);
          }
          coreMessage.msg = this.appDeserializer.do(coreMessage.msgId, coreMessage.bin, coreMessage.isServiceAction);
        }
      }
      cbMessage(coreMessage);
      this.onNewMessage(null, open, cbMessage);
    }
  }
}


module.exports = ChannelInput;
