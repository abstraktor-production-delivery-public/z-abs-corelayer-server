
'use strict';


class Const {
  static CONNECTION_TYPE_SERVER_REST = 0;
  static CONNECTION_TYPE_SERVER_IP = 1;
  static CONNECTION_TYPE_SERVER_WORKER = 2;
  static CONNECTION_TYPE_SERVER_WEB = 3;
  static CONNECTION_TYPE_CLIENT_IP = 4;
  static CONNECTION_TYPE_CLIENT_WORKER = 5;
  
  static EVENT_TYPE_CONNECTION = 0;
  static IP_EVENT_CONNECTED = 0;
  static IP_EVENT_DISCONNECTED = 1;
  
  static TRANSPORT_TCP = 0;
  static TRANSPORT_TLS = 1;
  static TRANSPORT_WS = 2;
  static TRANSPORT_WSS = 3;
  static TRANSPORT_HTTP = 4;
  static TRANSPORT_HTTPS = 5;
  static TRANSPORT_HTTP2 = 6;
  
  static TRANSPORT_AS_TEXT = ['tcp', 'tls', 'ws', 'wss', 'http', 'http', 'http'];
  
  static NODE_TYPE_FRONTEND = 0;
  static NODE_TYPE_BACKEND = 1;
  static NODE_TYPE_PROXY = 2;
  static NODE_TYPE_SURVEILLANCE = 3;
  static NODE_TYPE_WORKER = 4;
  
  #BUILD_DEBUG_START
  static NODE_TYPES = ['NODE_TYPE_FRONTEND', 'NODE_TYPE_BACKEND', 'NODE_TYPE_PROXY', 'NODE_TYPE_SURVEILLANCE', 'NODE_TYPE_WORKER'];
  #BUILD_DEBUG_STOP
  
  static OFFER_LOCAL = 0;
  static OFFER_FRONTEND = 1;
  static OFFER_BACKEND = 2;
  static OFFER_SURVEILLANCE = 3;
}


module.exports = Const;
