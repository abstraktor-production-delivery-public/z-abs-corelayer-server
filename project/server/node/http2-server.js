
'use strict';

const HttpRequestHandler = require('./http-request-handler');
const Fs = require('fs');
const Http2 = require('http2');


class Http2Server {
  constructor(serverData, nodeName, nodeId, httpCache, dataResponse, defaultSite) {
    this.serverData = serverData;
    this.nodeName = nodeName;
    this.nodeId = nodeId;
    this.httpRequestHandler = new HttpRequestHandler(httpCache, dataResponse, defaultSite);
  }
  
  start(http2Data, cb) {
    this.httpRequestHandler.init(http2Data, true);
    const options = this._readKeys(http2Data);
    if(options) {
      const server = Http2.createSecureServer({
        cert: options.certFile,
        key: options.keyFile,
        allowHTTP1: false 
      }, (req, res) => {
        this.httpRequestHandler.handle(req, res);
      });
      server.on('listening', () => {
        ddb.writelnTime(ddb.cyan('Server:   '), `${this.nodeName}-${http2Data.name} `, ddb.green('started at '), `http://${http2Data.host}:${http2Data.port}`);
        cb();
      });
	    server.on('error', (err) => {
        ddb.writelnTimeError(err, ddb.red('Server:'), ` '${this.nodeName}-${http2Data.name}'`, ddb.red(' did not start at'), ` http://${http2Data.host}:${http2Data.port}` + ddb.red('. Error: '), err.message);
        cb(err);
      });
      server.listen(http2Data.port, http2Data.host);
    }
    else {
      const err = new Error('-----');
      ddb.writelnTimeError(err, ddb.red('Server:'), ` '${this.nodeName}-${http2Data.name}'`, ddb.red(' did not start at '), ` http://${http2Data.host}:${http2Data.port}` + ddb.red('. No SSL keys.'));
      cb(err);
    }
  }
  
  _readKeys(http2Data) {
    if(http2Data.keyFile && http2Data.certFile) {
      try {
        const keyFile = Fs.readFileSync(http2Data.keyFile);
        const certFile = Fs.readFileSync(http2Data.certFile);
        return {
          key: keyFile,
          cert: certFile
        };
      }
      catch(err) {
        ddb.writelnTimeError(ddb.red(`Could not get cert and key file, error:`), err);
        return null;
      }
    }
    else {
      return null;
    }
  }
}


module.exports = Http2Server;
