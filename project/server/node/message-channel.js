
'use strict';

const CoreProtocolConst = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/core-protocol-const');
const ActionRequest = require('z-abs-corelayer-cs/clientServer/communication/action-request');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');


class MessageChannel {
  constructor(socket, dataResponse, BufferHandler, name) {
    this.socket = socket;
    this.dataResponse = dataResponse;
    this.BufferHandler = BufferHandler;
    this.name = name;
    this.initialized = false;
    this.requests = new Map();
    this.connectionSessionDataCache = new Map();
  }
  
  request(serviceAction, sessionId, cb) {
    const guid = GuidGenerator.create();
    serviceAction.setIds(guid, sessionId);
    const request = {
      type: 'DP_REQ',
      sessionId: sessionId,
      data: serviceAction.actionRequest
    };
    this.requests.set(guid, {
      serviceAction: serviceAction,
      cb: cb
    });
    this.socket.write(this.BufferHandler.convertToBuffer(request));
  }
  
  onMessage(msg, cbInit) {
    if(CoreProtocolConst.INIT === msg.msgId) {
      this.initialized = true;
      cbInit(msg);
    }
    else if(CoreProtocolConst.RESPONSE === msg.msgId) {
      this._response(msg);
    }
    else if('DP_REQ' === msg.type || 0 !== msg.msgId) {
      if(this.initialized) {
        this.dataResponse.onMessage(msg, this.connectionSessionDataCache, (msgOut) => {
          if('object' === typeof msgOut) {
            this.socket.write(this.BufferHandler.convertToBuffer(msgOut), (err) => {});
          }
          else {
            this.socket.write(this.BufferHandler.convertToBufferWithoutStringify(msgOut));
          }
        });
      }
      else {
        if('DP_REQ' === msg.type) {
          // TODO: make an error response.
          ddb.warning(ddb.red('Server: '), this.name, ddb.red(` DROPPED message, received a request when the connection where not initialized. `), this._jsonStringify(msg));
        }
        else {
          ddb.warning(ddb.red('Server: '), this.name, ddb.red(` DROPPED message, received a message when the connection where not initialized. `), this._jsonStringify(msg));
        }
      }
    }
    else {
      ddb.warning(ddb.red('Server: '), this.name, ddb.red(' DROPPED message, received a message of unknown type: '), this._jsonStringify(msg));
    }
  }
  
  _response(response) {
    const serviceActionData = this.requests.get(response.id);
    this.requests.delete(response.id);
    serviceActionData.serviceAction.setResponses(response.responses);
    const responses = serviceActionData.serviceAction.responses;
    if(1 === responses.length) {
      if('success' === responses[0].result.code) {
        serviceActionData.cb(null, responses[0].data);
      }
      else {
        serviceActionData.cb(new Error(responses[0].result.msg));
      }
    }
    else {
      serviceActionData.cb(new Error('Wrong response format'));
    }
  }
  
  _jsonStringify(msg) {
    try {
      return JSON.stringify(msg);
    }
    catch(err) {
      return ddb.red('Could not stringify msg');
    }
  }
}


module.exports = MessageChannel;
