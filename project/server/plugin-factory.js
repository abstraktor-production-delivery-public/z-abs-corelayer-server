
'use strict';

const ActorPathDist = require('./path/actor-path-dist');
const Logger = require('./log/logger');
const Fs = require('fs');
const Path = require('path');


class PluginFactory {
  constructor(type, params, libType='server') {
    this.libType = libType;
    this.prefix = `plugin_${type}_`;
    this.params = params;
    this.prefix_length = this.prefix.length;
    this.appSearchPath = `plugin-${type}`;
    this.paths = [];
    this.plugins = new Map();
  }
  
  create(name) {
    const foundFactoryPath = this.plugins.get(name);
    if(foundFactoryPath) {
      const Plugin = require(foundFactoryPath.factoryPath);
      try {
        const plugin = new Plugin();
        if(plugin.initData) {
          plugin.initData(...this.params);
        }
        return plugin;
      }
      catch(e) {
        ddb.error('Could not create plugin:', name);
        console.log(e);
      }
    }
    ddb.error('FACTORY ERROR: ' + name);
  }
  
  createAll() {
    if(0 !== this.plugins.size) {
      const all = [];
      this.plugins.forEach((value, key) => {
        const plugin = this.create(key);
        all.push({libName: value.libName, plugin});
      });
      return all;
    }
    else {
      return [];
    }
  }
  
  _setPaths(path) {
    this.paths.push(Path.join(ActorPathDist.getLayersPath(), path + `${Path.sep}${this.libType}`, this.appSearchPath));
  }
  
  setPaths(paths) {
    if(Array.isArray(paths)) {
      paths.forEach((path) => {
        this._setPaths(path);
      });
    }
    else {
      this._setPaths(paths);
    }
  }
  
  load(done, path) {
    const loadPath = path ? this.paths.concat(path) : this.paths;
    let pendings = loadPath.length;
    if(0 === pendings) {
      return done(this.plugins.size);
    }
    loadPath.forEach((p) => {
      this.loadPlugins(p, () => {
        if(0 === --pendings) {
          done(this.plugins.size);
        }
      }); 
    });
  }
  
  loadPlugins(path, done) {
    Fs.lstat(path, (err, stat) => {
      if(err) {
        return done();
      }
      if(stat.isDirectory()) {
        // We have a directory: do a tree walk
        Fs.readdir(path, (err, files) => {
          let file, length = files.length;
          let pendings = length;
          if(0 === pendings) {
            done();
          }
          else {
            for(let i = 0; i < length; ++i) {
              file = Path.join(path, files[i]);
              this.loadPlugins(file, () => {
                if(0 === --pendings) {
                  done();
                }
              });
            }
          }
        });
      }
      else if(stat.isFile()) {
        const splitFilePath = path.split(Path.sep);
        const libName = splitFilePath[splitFilePath.length - 4];
        const filePath = splitFilePath[splitFilePath.length - 2];
        const fileName = splitFilePath[splitFilePath.length - 1];
        const names = fileName.split('.');
        if(names[0].startsWith(this.prefix)) {
          const name = names[0].substring(this.prefix_length);
          const factoryPath = path.substring(0, path.lastIndexOf(Path.sep)) + Path.sep + names[0];
          this.plugins.set(name, {libName, factoryPath});
        }
        done();
      }
    });
  }
}

module.exports = PluginFactory;
