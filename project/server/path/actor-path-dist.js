
'use strict';

const ActorPath = require('./actor-path');
const Path = require('path');


class ActorPathDist {
  constructor() {
    this.actorDistPath = ActorPath.setPath(ActorPath.getActorPath(), `${Path.sep}dist`);
    this.actorDistLayersPath = ActorPath.setPath(this.actorDistPath, `${Path.sep}Layers`);
    this.actorDistScriptsPath = ActorPath.setPath(this.actorDistPath, `${Path.sep}scripts`);
    this.actorDistCssPath = ActorPath.setPath(this.actorDistPath, `${Path.sep}css`);
    this.actorDistApiPath = ActorPath.setPath(this.actorDistPath, `${Path.sep}api`);
    this.actorDistActorApiPath = ActorPath.setPath(this.actorDistApiPath, `${Path.sep}actor-api`);
    this.actorDistNodejsApiPath = ActorPath.setPath(this.actorDistApiPath, `${Path.sep}nodejs-api`);
    this.actorDistStackApiPath = ActorPath.setPath(this.actorDistApiPath, `${Path.sep}stack-api`);
    this.actorDistActorsPath = ActorPath.setPath(this.getActorDistPath(), `${Path.sep}actors`);
    this.actorDistStacksPath = ActorPath.setPath(this.getActorDistPath(), `${Path.sep}stacks`);
    this.actorDistStacksGlobalPath = ActorPath.setPath(this.getActorDistPath(), `${Path.sep}stacks${Path.sep}Stacks-global`);
    this.actorDistStacksLocalPath = ActorPath.setPath(this.getActorDistPath(), `${Path.sep}stacks${Path.sep}Stacks-local`);
    this.actorDistServerPath = ActorPath.setPath(this.getActorDistPath(), `${Path.sep}Layers${Path.sep}AppLayer${Path.sep}server`);
    this.actorDistServerPluginDataPath = ActorPath.setPath(this.getActorDistServerPath(), `${Path.sep}plugin-data`);
    this.actorDistServerPluginServicePath = ActorPath.setPath(this.getActorDistServerPath(), `${Path.sep}plugin-service`);
    this.actorDistServerPluginComponentPath = ActorPath.setPath(this.getActorDistServerPath(), `${Path.sep}plugin-component`);
    this.serverActorsGlobalJs = 'serverActorsGlobalJs';
    this.serverActorsLocalJs = 'serverActorsLocalJs';
    this.serverStacksGlobalJs = 'serverStacksGlobalJs';
    this.serverStacksLocalJs = 'serverStacksLocalJs';
  }
  
  getActorDistPath() {
    return this.actorDistPath;
  }
  
  getLayersPath() {
    return this.actorDistLayersPath;
  }
  
  getScriptsPath() {
    return this.actorDistScriptsPath;
  }
  
  getCssPath() {
    return this.actorDistCssPath;
  }
  
  getActorDistActorsPath() {
    return this.actorDistActorsPath;
  }
  
  getActorFile(path) {
    return Path.normalize(`${this.getActorDistActorsPath()}${Path.sep}${path.replace(new RegExp('[/\\\\]', 'g'), Path.sep)}`);
  }
  
  getActorCacheName(path) {
    if(path.startsWith('./Actors-global')) {
      return this.serverActorsGlobalJs;
    }
    else if(path.startsWith('./Actors-local')) {
      return this.serverActorsLocalJs;
    }
  }
  
  getActorDistStacksPath() {
    return this.actorDistStacksPath;
  }
  
  getActorDistStacksGlobalPath() {
    return this.actorDistStacksGlobalPath;
  }
  
  getActorDistStacksLocalPath() {
    return this.actorDistStacksLocalPath;
  }
  
  getStackFile(path) {
    return Path.normalize(`${this.getActorDistStacksPath()}${Path.sep}${path.replace(new RegExp('[/\\\\]', 'g'), Path.sep)}`);
  }
  
  getStackCacheName(path) {
    if(path.startsWith('./Stacks-global')) {
      return this.serverStacksGlobalJs;
    }
    else if(path.startsWith('./Stacks-local')) {
      return this.serverStacksLocalJs;
    }
  }
  
  getActorDistServerPath() {
    return this.actorDistServerPath;
  }
  
  getActorDistServerPluginDataPath() {
    return this.actorDistServerPluginDataPath;
  }
  
  getActorDistServerPluginServicePath() {
    return this.actorDistServerPluginServicePath;
  }
  
  getActorDistServerPluginComponentPath() {
    return this.actorDistServerPluginComponentPath;  
  }
  
  getActorDistServerWorkerLogPath() {
    return this.actorDistServerWorkerLogPath; 
  }
  
  getCodeFile(file, projectType) {
    if(!projectType) {
      if(file.startsWith('./project/api')) {
        const fileName = file.substring('./project/'.length);
        return Path.normalize(`${this.getActorDistPath()}${Path.sep}${fileName.replace(new RegExp('[/\\\\]', 'g'), Path.sep)}`);
      }
      else {
        const fileName = file.substring('./project/'.length);
        return Path.normalize(`${this.getLayersPath()}${Path.sep}AppLayer${Path.sep}${fileName.replace(new RegExp('[/\\\\]', 'g'), Path.sep)}`);
      }
    }
    else {
      return Path.normalize(`${this.getLayersPath()}${Path.sep}${file.replace(new RegExp('[/\\\\]', 'g'), Path.sep)}`);
    }
  }
  
  getImage(path) {
    return `/abs-images/${path}`;
  }
}

module.exports = new ActorPathDist();
