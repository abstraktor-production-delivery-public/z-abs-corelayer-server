
'use strict';

const ActorPath = require('./actor-path');
const Path = require('path');


class ActorPathData {
  constructor() {
    this.actor = 'Actor';
    this.globalDataRepoName = 'actorjs-data-global';
    
    if(ActorPath.releaseStepDelivery) {
      this.dataFolderLocal = ActorPath.setPath(ActorPath.getActorPath(), `..${Path.sep}..${Path.sep}..${Path.sep}Data`);
      this.dataFolderGlobal = ActorPath.setPath(ActorPath.getActorPath(), `${Path.sep}node_modules${Path.sep}${ActorPath.organization}`);
      this.actorDataGlobalFolder  = ActorPath.setPath(this.dataFolderGlobal, `${Path.sep}${this.globalDataRepoName}`);
    }
    else if(ActorPath.releaseStepCandidate) {
      this.dataFolderLocal = ActorPath.setPath(ActorPath.getActorPath(), `..${Path.sep}Data`);
      this.dataFolderGlobal = ActorPath.setPath(ActorPath.getActorPath(), `${Path.sep}node_modules`);
      this.actorDataGlobalFolder  = ActorPath.setPath(this.dataFolderGlobal, `${Path.sep}${this.globalDataRepoName}`);
    }
    else {
      this.dataFolderLocal = ActorPath.setPath(ActorPath.getActorPath(), `..${Path.sep}Data`);
      this.dataFolderGlobal = ActorPath.setPath(ActorPath.getActorPath(), `..${Path.sep}Data`);
      this.actorDataGlobalFolder  = ActorPath.setPath(this.dataFolderGlobal, `${Path.sep}${this.globalDataRepoName}`);
    }
    
    // *** LOCAL
    
    this.actorDataLocalFolder = ActorPath.setPath(this.dataFolderLocal, `${Path.sep}actorjs-data-local`);
    this.testAbstractionsLocalFolder = ActorPath.setPath(this.actorDataLocalFolder, `${Path.sep}TestAbstractions`);
    this.actorsLocalFolder = ActorPath.setPath(this.testAbstractionsLocalFolder, `${Path.sep}Actors-local`);
    this.stacksLocalFolder = ActorPath.setPath(this.testAbstractionsLocalFolder, `${Path.sep}Stacks-local`);
    this.addressesLocalFolder = ActorPath.setPath(this.actorDataLocalFolder, `${Path.sep}Addresses-local`);
    
    this.testDataLocalFolder = ActorPath.setPath(this.actorDataLocalFolder, `${Path.sep}TestData`);
    
    this.documentationNoteLocalFolder = ActorPath.setPath(this.actorDataLocalFolder, `${Path.sep}Note`);
    
    this.contentLocalFolder = ActorPath.setPath(this.actorDataLocalFolder, `${Path.sep}Content`);
    this.reposLocalFolder = ActorPath.setPath(this.actorDataLocalFolder, `${Path.sep}Repos`);
    this.reposLocalFile = ActorPath.setPath(this.reposLocalFolder, `${Path.sep}repos.json`);
    
    this.reposLocalReadMeFile = ActorPath.setPath(this.actorDataLocalFolder, `${Path.sep}README.md`);
    
    // *** GLOBAL
    
    this.testAbstractionsGlobalFolder = ActorPath.setPath(this.actorDataGlobalFolder, `${Path.sep}TestAbstractions`);
    this.actorsGlobalFolder = ActorPath.setPath(this.testAbstractionsGlobalFolder, `${Path.sep}Actors-global`);
    this.stacksGlobalFolder = ActorPath.setPath(this.testAbstractionsGlobalFolder, `${Path.sep}Stacks-global`);
    
    this.addressesGlobalFolder = ActorPath.setPath(this.actorDataGlobalFolder, `${Path.sep}Addresses-global`);
    this.certsGlobalFolder = ActorPath.setPath(this.actorDataGlobalFolder, `${Path.sep}Certs`);
    this.testDataGlobalFolder = ActorPath.setPath(this.actorDataGlobalFolder, `${Path.sep}TestData`);
    this.contentGlobalFolder = ActorPath.setPath(this.actorDataGlobalFolder, `${Path.sep}Content`);
    
    this.loginGlobalFolder = ActorPath.setPath(this.actorDataGlobalFolder, `${Path.sep}Login`);
    this.loginGlobalFile = ActorPath.setPath(this.loginGlobalFolder, `${Path.sep}licenses.json`);
    this.dependenciesGlobalFile = ActorPath.setPath(this.loginGlobalFolder, `${Path.sep}dependencies.json`);
    
    this.testAbstractions = 'TestAbstractions';
  }
  
  getGlobalDataRepoName() {
    return this.globalDataRepoName;
  }
  
  getActorDataLocalFolder() {
    return this.actorDataLocalFolder;
  }
  
  getActorDataGlobalFolder() {
    return this.actorDataGlobalFolder;
  }
  
  getReposLocalFolder() {
    return this.dataFolderLocal;
  }
  
  getReposGlobalFolder() {
    return this.dataFolderGlobal;
  }
  
  getTestAbstractionGlobalFolder() {
    return this.testAbstractionsGlobalFolder;
  }
  
  getTestAbstractionLocalFolder() {
    return this.testAbstractionsLocalFolder;
  }
  
  getReposRepoFolder(repoName) {
    if(this.globalDataRepoName === repoName) {
      return this.actorDataGlobalFolder;
    }
    else {
      return `${this.dataFolderLocal}${Path.sep}${repoName}`;
    }
  }
  
  getTestAbstractionFolder(repoName) {
    if(this.globalDataRepoName === repoName) {
      return this.getTestAbstractionGlobalFolder();
    }
    else {
      return `${this.dataFolderLocal}${Path.sep}${repoName}${Path.sep}${this.testAbstractions}`;
    }
  }

  getSystemUnderTestFolder(repoName, sutName) {
    if(this.globalDataRepoName === repoName) {
      return `${this.testAbstractionsGlobalFolder}${Path.sep}Sut${sutName}`;
    }
    else {
      return `${this.getTestAbstractionFolder(repoName)}${Path.sep}Sut${sutName}`;
    }
  }
  
  getSystemUnderTestFile(repoName, sutName) {
    return `${this.getSystemUnderTestFolder(repoName, sutName)}${Path.sep}Sut${sutName}.json`;
  }
  
  getSystemUnderTestActorGlobalFolder() {
    return `${this.testAbstractionsGlobalFolder}${Path.sep}Sut${this.actor}`;
  }
  
  getSystemUnderTestActorFile() {
    return `${this.getSystemUnderTestFolder(this.globalDataRepoName, this.actor)}${Path.sep}Sut${this.actor}.json`;
  }
  
  getFunctionUnderTestFolder(repoName, sutName, futName) {
    return `${this.getSystemUnderTestFolder(repoName, sutName)}${Path.sep}Fut${sutName}${futName}`;
  }
  
  getFunctionUnderTestFile(repoName, sutName, futName) {
    return `${this.getFunctionUnderTestFolder(repoName, sutName, futName)}${Path.sep}Fut${sutName}${futName}.json`;
  }
  
  getTestCasesFolder(repoName, sutName, futName) {
    return `${this.getFunctionUnderTestFolder(repoName, sutName, futName)}${Path.sep}TestCases`;
  }
  
  getTestSuitesFolder(repoName, sutName, futName) {
    return `${this.getFunctionUnderTestFolder(repoName, sutName, futName)}${Path.sep}TestSuites`;
  }
  
  getTestCaseFolder(repoName, sutName, futName, tcName) {
    return `${this.getTestCasesFolder(repoName, sutName, futName)}${Path.sep}Tc${tcName}`;
  }
  
  getTestSuiteFolder(repoName, sutName, futName, tsName) {
    return `${this.getTestSuitesFolder(repoName, sutName, futName)}${Path.sep}Ts${tsName}`;
  }

  getTestCaseFile(repoName, sutName, futName, tcName) {
    return `${this.getTestCaseFolder(repoName, sutName, futName, tcName)}${Path.sep}Tc${tcName}.json`;
  }
  
  getTestCaseFileName(tcName) {
    return `Tc${tcName}.json`;
  }
  
  getTestSuiteFile(repoName, sutName, futName, tsName) {
    return `${this.getTestSuiteFolder(repoName, sutName, futName, tsName)}${Path.sep}Ts${tsName}.json`;
  }
  
  getActorsLocalFolder() {
    return this.actorsLocalFolder;
  }
  
  getActorsGlobalFolder() {
    return this.actorsGlobalFolder;
  }
    
  getActorsLocalProjectPureFile() {
    return `${this.getActorsLocalFolder()}${Path.sep}actors-local-project.tree`;
  }
    
  getActorsGlobalProjectPureFile() {
    return `${this.getActorsGlobalFolder()}${Path.sep}actors-global-project.tree`;
  }
  
  getActorsProjectPureFile(path) {
    if('./Actors-global' === path) {
      return this.getActorsGlobalProjectPureFile();
    }
    else if('./Actors-local' === path ) {
      return this.getActorsLocalProjectPureFile();
    }
    return 'NotFound';
  }
  
  getActorFile(path) {
    if(path.startsWith('./Actors-global')) {
      return Path.normalize(`${this.getTestAbstractionGlobalFolder()}${Path.sep}${path.replace(new RegExp('[/\\\\]', 'g'), Path.sep)}`);
    }
    else if(path.startsWith('./Actors-local')) {
      return Path.normalize(`${this.getTestAbstractionLocalFolder()}${Path.sep}${path.replace(new RegExp('[/\\\\]', 'g'), Path.sep)}`);
    }
    return 'NotFound';
  }
  
  getStacksLocalFolder() {
    return this.stacksLocalFolder;
  }
  
  getStacksGlobalFolder() {
    return this.stacksGlobalFolder;
  }
    
  getStacksLocalProjectPureFile() {
    return `${this.getStacksLocalFolder()}${Path.sep}stacks-local-project.tree`;
  }
      
  getStacksGlobalProjectPureFile() {
    return `${this.getStacksGlobalFolder()}${Path.sep}stacks-global-project.tree`;
  }
  
  getStacksProjectPureFile(path) {
    if('./Stacks-global' === path) {
      return this.getStacksGlobalProjectPureFile();
    }
    else if('./Actors-local' === path ) {
      return this.getStacksLocalProjectPureFile();
    }
    return 'NotFound';
  }
  
  getStackFile(path) {
    if(path.startsWith('./Stacks-global')) {
      return Path.normalize(`${this.getTestAbstractionGlobalFolder()}${Path.sep}${path.replace(new RegExp('[/\\\\]', 'g'), Path.sep)}`);
    }
    else if(path.startsWith('./Stacks-local')) {
      return Path.normalize(`${this.getTestAbstractionLocalFolder()}${Path.sep}${path.replace(new RegExp('[/\\\\]', 'g'), Path.sep)}`);
    }
    return 'NotFound';
  }
  
  getAddressesLocalFolder() {
    return this.addressesLocalFolder;
  }
  
  getAddressesGlobalFolder() {
    return this.addressesGlobalFolder;
  }
  
  getCertsGlobalFolder() {
    return this.certsGlobalFolder;
  }
  
  getAddressesLocalNetworksFile() {
    return `${this.getAddressesLocalFolder()}${Path.sep}networks.json`;
  }
  
  getAddressesGlobalNetworksFile() {
    return `${this.getAddressesGlobalFolder()}${Path.sep}networks.json`;
  }

  getAddressesLocalInterfacesSutFile() {
    return `${this.getAddressesLocalFolder()}${Path.sep}interfacesSut.json`;
  }
  
  getAddressesGlobalInterfacesSutFile() {
    return `${this.getAddressesGlobalFolder()}${Path.sep}interfacesSut.json`;
  }

  getAddressesLocalInterfacesClientFile() {
    return `${this.getAddressesLocalFolder()}${Path.sep}interfacesClient.json`;
  }
  
  getAddressesGlobalInterfacesClientFile() {
    return `${this.getAddressesGlobalFolder()}${Path.sep}interfacesClient.json`;
  }

  getAddressesLocalInterfacesServerFile() {
    return `${this.getAddressesLocalFolder()}${Path.sep}interfacesServer.json`;
  }
  
  getAddressesGlobalInterfacesServerFile() {
    return `${this.getAddressesGlobalFolder()}${Path.sep}interfacesServer.json`;
  }
  
  getAddressesLocalAddressesSutFile() {
    return `${this.getAddressesLocalFolder()}${Path.sep}addressesSut.json`;
  }

  getAddressesGlobalAddressesSutFile() {
    return `${this.getAddressesGlobalFolder()}${Path.sep}addressesSut.json`;
  }

  getAddressesLocalAddressesClientFile() {
    return `${this.getAddressesLocalFolder()}${Path.sep}addressesClient.json`;
  }

  getAddressesGlobalAddressesClientFile() {
    return `${this.getAddressesGlobalFolder()}${Path.sep}addressesClient.json`;
  }

  getAddressesLocalAddressesServerFile() {
    return `${this.getAddressesLocalFolder()}${Path.sep}addressesServer.json`;
  }
  
  getAddressesGlobalAddressesServerFile() {
    return `${this.getAddressesGlobalFolder()}${Path.sep}addressesServer.json`;
  }
  
  getAddressesLocalSrcFile() {
    return `${this.getAddressesLocalFolder()}${Path.sep}addressesSrc.json`;
  }
  
  getAddressesGlobalSrcFile() {
    return `${this.getAddressesGlobalFolder()}${Path.sep}addressesSrc.json`;
  }
  
  getAddressesLocalDstFile() {
    return `${this.getAddressesLocalFolder()}${Path.sep}addressesDst.json`;
  }
  
  getAddressesGlobalDstFile() {
    return `${this.getAddressesGlobalFolder()}${Path.sep}addressesDst.json`;
  }
  
  getAddressesLocalSrvFile() {
    return `${this.getAddressesLocalFolder()}${Path.sep}addressesSrv.json`;
  }
  
  getAddressesGlobalSrvFile() {
    return `${this.getAddressesGlobalFolder()}${Path.sep}addressesSrv.json`;
  }
  
  getAddressesLocalPortsFile() {
    return `${this.getAddressesLocalFolder()}${Path.sep}addressesPorts.json`;
  }
  
  getAddressesGlobalPortsFile() {
    return `${this.getAddressesGlobalFolder()}${Path.sep}addressesPorts.json`;
  }
  
  getAddressesLocalDnsFile() {
    return `${this.getAddressesLocalFolder()}${Path.sep}addressesDns.json`;
  }
  
  getAddressesGlobalDnsFile() {
    return `${this.getAddressesGlobalFolder()}${Path.sep}addressesDns.json`;
  }
  
  getTestDataGlobalFolder() {
    return this.testDataGlobalFolder;
  }
  
  getTestDataLocalFolder() {
    return this.testDataLocalFolder;
  }
  
  getTestDataEnvironmentGlobalFile() {
    return `${this.getTestDataGlobalFolder()}${Path.sep}test-data-environment.json`;
  }
  
  getTestDataEnvironmentLocalFile() {
    return `${this.getTestDataLocalFolder()}${Path.sep}test-data-environment.json`;
  }
  
  getTestDataGeneralGlobalFile() {
    return `${this.getTestDataGlobalFolder()}${Path.sep}test-data-general.json`;
  }
  
  getTestDataGeneralLocalFile() {
    return `${this.getTestDataLocalFolder()}${Path.sep}test-data-general.json`;
  }
  
  getTestDataOutputGlobalFile() {
    return `${this.getTestDataGlobalFolder()}${Path.sep}test-data-output.json`;
  }
  
  getTestDataOutputLocalFile() {
    return `${this.getTestDataLocalFolder()}${Path.sep}test-data-output.json`;
  }
  
  getTestDataSystemGlobalFile() {
    return `${this.getTestDataGlobalFolder()}${Path.sep}test-data-system.json`;
  }
  
  getTestDataSystemLocalFile() {
    return `${this.getTestDataLocalFolder()}${Path.sep}test-data-system.json`;
  }
  
  getDocumentationNoteLocalFilder() {
    return this.documentationNoteLocalFolder;
  }
 
  getDocumentationNoteLocalFileAndPath(guid, path) {
    let fullPath = `${this.getDocumentationNoteLocalFilder()}${Path.sep}${path}`;
    let formattedPath = Path.normalize(fullPath.replace(new RegExp('[/\\\\]', 'g'), Path.sep));
    let formattedFile = `${formattedPath}${Path.sep}Local-note-${guid}.txt`;
    return {
      path: formattedPath,
      file: formattedFile
    };
  }
    
  getDocumentationNoteLocalFile(guid, path) {
    return this.getDocumentationNoteLocalFileAndPath(guid, path).file;
  }
  
  getContentLocalFolder() {
    return this.contentLocalFolder;
  }
  
  getContentTextLocalFile() {
    return `${this.contentLocalFolder}${Path.sep}text.json`;
  }
  getContentDocumentsLocalFile() {
    return `${this.contentLocalFolder}${Path.sep}documents.json`;
  }
  
  getContentImageLocalFile() {
    return `${this.contentLocalFolder}${Path.sep}image.json`;
  }
  
  getContentVideoLocalFile() {
    return `${this.contentLocalFolder}${Path.sep}video.json`;
  }
  
  getContentAudioLocalFile() {
    return `${this.contentLocalFolder}${Path.sep}audio.json`;
  }
  
  getContentOtherLocalFile() {
    return `${this.contentLocalFolder}${Path.sep}other.json`;
  }

  getContentGlobalFolder() {
    return this.contentGlobalFolder;
  }
 
  getContentTextGlobalFile() {
    return `${this.contentGlobalFolder}${Path.sep}text.json`;
  }
  
  getContentDocumentsGlobalFile() {
    return `${this.contentGlobalFolder}${Path.sep}documents.json`;
  }
  
  getContentImageGlobalFile() {
    return `${this.contentGlobalFolder}${Path.sep}image.json`;
  }
  
  getContentVideoGlobalFile() {
    return `${this.contentGlobalFolder}${Path.sep}video.json`;
  }
  
  getContentAudioGlobalFile() {
    return `${this.contentGlobalFolder}${Path.sep}audio.json`;
  }
  
  getContentOtherGlobalFile() {
    return `${this.contentGlobalFolder}${Path.sep}other.json`;
  }
  
  getLoginGlobalFolder() {
    return this.loginGlobalFolder;
  }
  
  getLicensesFile() {
    return this.loginGlobalFile;
  }
  
  getDependenciesFile() {
    return this.dependenciesGlobalFile;
  }
  
  getLocalReposFolder() {
    return this.reposLocalFolder;
  }
  
  getLocalReposFile() {
    return this.reposLocalFile;
  }
  
  getReadMeFile() {
    return this.reposLocalReadMeFile;
  }
}


module.exports = new ActorPathData();
