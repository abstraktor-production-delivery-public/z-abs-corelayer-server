
'use strict';

const Path = require('path');


class ActorPath {
  constructor() {
    const releaseData = Reflect.get(global, 'release-data@abstractor');
    this.releaseStep = releaseData.releaseStep;
    this.organization = releaseData.organization;
    this.appName = releaseData.appName;
    this.releaseStepDevelopment = 'source' === this.releaseStep || 'development' === this.releaseStep;
    this.releaseStepCandidate = 'candidate' === this.releaseStep;
    this.releaseStepDelivery = 'delivery' === this.releaseStep;
      
    this.actorPath = this.setPath(Path.resolve('.'), '');
    this.actorParentPath = this.setPath(Path.resolve('..'), '');
    this.actorTmpPath = this.setPath(this.actorPath, `${Path.sep}temp`);
    this.packageJsonFile = this.setPath(this.actorPath, `${Path.sep}package.json`);
    this.packageJsonLockFile = this.setPath(this.actorPath, `${Path.sep}package-lock.json`);
    this.settingsFile = this.setPath(this.actorPath, `${Path.sep}settings.json`);
  }
  
  setPath(prePath, newPath) {
    return Path.normalize(`${prePath}${Path.sep}${newPath}`);
  }
  
  getActorPath() {
    return this.actorPath;
  }
  
  getActorParentPath() {
    return this.actorParentPath;
  }
      
  getActorTmpPath() {
    return this.actorTmpPath;
  }
  
  getPackageJsonFile() {
    return this.packageJsonFile;
  }
  
  getPackageJsonLockFile() {
    return this.packageJsonLockFile;
  }
  
  getSettingsFile() {
    return this.settingsFile;
  }
}


ActorPath.space = '                                                  ';

module.exports = new ActorPath();
