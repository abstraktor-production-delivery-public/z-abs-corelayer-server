
'use strict';

const ActorPath = require('./actor-path');
const Path = require('path');


class ActorPathBuild {
  constructor() {
    this.actorBuildPath = ActorPath.setPath(ActorPath.getActorPath(), `${Path.sep}build`);
  }
  
  getActorBuildPath() {
    return this.actorBuildPath;
  }
  
  getCodeFile(file, projectType) {
    if(!projectType) {
      const fileName = file.substring('./project/'.length);
      return Path.normalize(`${this.getActorBuildPath()}${Path.sep}AppLayer${Path.sep}${fileName.replace(new RegExp('[/\\\\]', 'g'), Path.sep)}`);
    }
    else {
      return Path.normalize(`${this.getActorBuildPath()}${Path.sep}${file.replace(new RegExp('[/\\\\]', 'g'), Path.sep)}`);
    }
  }
}


module.exports = new ActorPathBuild();
