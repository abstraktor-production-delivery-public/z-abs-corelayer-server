
'use strict';

const ActorPath = require('./actor-path');
const Path = require('path');


class ActorPathContent {
  constructor() {
    if(ActorPath.releaseStepDelivery) {
      this.contentFolder = ActorPath.setPath(ActorPath.getActorPath(), `..${Path.sep}..${Path.sep}..${Path.sep}Content`);
      this.contentGlobalFolder = ActorPath.setPath(ActorPath.getActorPath(), `${Path.sep}node_modules${Path.sep}${ActorPath.organization}${Path.sep}actorjs-content-global`);
    }
    else if(ActorPath.releaseStepCandidate) {
      this.contentFolder = ActorPath.setPath(ActorPath.getActorPath(), `..${Path.sep}Content`);
      this.contentGlobalFolder = ActorPath.setPath(ActorPath.getActorPath(), `${Path.sep}node_modules${Path.sep}actorjs-content-global`);
    }
    else {
      this.contentFolder = ActorPath.setPath(ActorPath.getActorPath(), `..${Path.sep}Content`);
      this.contentGlobalFolder = ActorPath.setPath(this.contentFolder, `${Path.sep}actorjs-content-global`);
    }
    
    // *** LOCAL
    
    this.contentLocalFolder = ActorPath.setPath(this.contentFolder, `${Path.sep}actorjs-content-local`);
    this.contentLocalReadMeFile = ActorPath.setPath(this.contentLocalFolder, `${Path.sep}README.md`);
  }
  
  getContentFolder() {
    return this.contentFolder;
  }
  
  getContentLocalFolder() {
    return this.contentLocalFolder;
  }

  getContentGlobalFolder() {
    return this.contentGlobalFolder;
  }
  
  getContentFile(repoPath, path) {
    return Path.normalize(`${repoPath}${Path.sep}${path}`);
  }
  
  getReadMeFile() {
    return this.contentLocalReadMeFile;
  }
}

module.exports = new ActorPathContent();
