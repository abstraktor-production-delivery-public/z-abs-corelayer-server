
'use strict';

const ActorPathCreator = require('../actor-path-creator');
const ActorPathGenerated = require('../actor-path-generated');
const Path = require('path');


class ActorGeneratedPaths {
  constructor() {
    this.folderPaths = [
      ActorPathGenerated.getActorGeneratedFolder(),
      ActorPathGenerated.getStagingFolder(),
      ActorPathGenerated.getTestCaseFolder(),
      ActorPathGenerated.getStagingButtonsFolder(),
      ActorPathGenerated.getTestCaseButtonsFolder(),
      ActorPathGenerated.getTestSuiteFolder(),
      ActorPathGenerated.getTestSuiteButtonsFolder(),
      ActorPathGenerated.getTestDebuggerFolder(),
      ActorPathGenerated.getWorkspaceFolder(),
      ActorPathGenerated.getProjectsFolder(),
      ActorPathGenerated.getDatabasesFolder(),
      ActorPathGenerated.getDatabasesGitFolder(),
      ActorPathGenerated.getDatabasesNpmFolder(),
      ActorPathGenerated.getBuildFolder()
    ];
    this.filePaths = [
      {
        path: ActorPathGenerated.getStagingFile(),
        default: {
          labId: 'actor',
          userId: 'actor',
          repo: 'actorjs-data-global',
          systemUnderTest: 'Actor',
          systemUnderTestInstance: '',
          forcedLocalhost: false,
          nodes: []
        }
      },
      {
        path: ActorPathGenerated.getNetworksFile(),
        default: []
      },
      {
        path: ActorPathGenerated.getAddressesFile(),
        default: {}
      },
      {
        path: ActorPathGenerated.getLocalDnsFile(),
        default: []
      },
      {
        path: ActorPathGenerated.getStagingReport(),
        default: []
      },
      {
        path: ActorPathGenerated.getChosenAddresses(),
        default: {}
      },
      {
        path: ActorPathGenerated.getDependenciesFile(),
        default: {
          time: '',
          dependencies: []
        }
      },
      {
        path: ActorPathGenerated.getTestCasesRecentFile(),
        default: []
      },
      {
        path: ActorPathGenerated.getTestSuitesRecentFile(),
        default: []
      },
      {
        path: ActorPathGenerated.getTestDebuggerFolder(),
        default: {
          breakpoints: []
        }
      },
      {
        path: ActorPathGenerated.getWorkspaceRecentFiles(),
        default: {
          recentWorkspaces: [{
            appName: 'actorjs',
            workspaceName: 'actorjs'
          }]
        }
      }
    ];
  }
  
  verifyOrCreate(done) {
    new ActorPathCreator(this.folderPaths, this.filePaths).verifyOrCreate(done);
  }
}

module.exports = ActorGeneratedPaths;
