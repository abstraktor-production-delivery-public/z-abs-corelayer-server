
'use strict';

const ActorPathCreator = require('../actor-path-creator');
const ActorPathData = require('../actor-path-data');
const ActorPath = require('../actor-path');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');


class ActorDataPaths {
  constructor() {
    this.folderPaths = [
      ActorPathData.getReposLocalFolder(),
      ActorPathData.getActorDataLocalFolder(),
      ActorPathData.getDocumentationNoteLocalFilder(),
      ActorPathData.getTestAbstractionLocalFolder(),
      ActorPathData.getActorsLocalFolder(),
      ActorPathData.getStacksLocalFolder(),
      ActorPathData.getAddressesLocalFolder(),
      ActorPathData.getTestDataLocalFolder(),
      ActorPathData.getContentLocalFolder(),
      ActorPathData.getLocalReposFolder()
    ];
    if(ActorPath.releaseStepDevelopment) {
      this.folderPaths.push(ActorPathData.getActorDataGlobalFolder());
      this.folderPaths.push(ActorPathData.getTestAbstractionGlobalFolder());
      this.folderPaths.push(ActorPathData.getActorsGlobalFolder());
      this.folderPaths.push(ActorPathData.getStacksGlobalFolder());
      this.folderPaths.push(ActorPathData.getAddressesGlobalFolder());
      this.folderPaths.push(ActorPathData.getTestDataGlobalFolder());
      this.folderPaths.push(ActorPathData.getSystemUnderTestActorGlobalFolder());
      this.folderPaths.push(ActorPathData.getContentGlobalFolder());
      this.folderPaths.push(ActorPathData.getLoginGlobalFolder());
    }
    
    this.filePaths = [
      {
        path: ActorPathData.getAddressesLocalNetworksFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesLocalInterfacesSutFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesLocalInterfacesClientFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesLocalInterfacesServerFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesLocalAddressesSutFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesLocalAddressesClientFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesLocalAddressesServerFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesLocalSrcFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesLocalDnsFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesLocalDstFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesLocalSrvFile(),
        default: []
      },
      {
        path: ActorPathData.getAddressesLocalPortsFile(),
        default: []
      },
      {
        path: ActorPathData.getActorsLocalProjectPureFile(),
        text: `${GuidGenerator.create()};;Actors-local;folder;.;[actorjs,js]`
      },
      {
        path: ActorPathData.getStacksLocalProjectPureFile(),
        text: `${GuidGenerator.create()};;Stacks-local;folder;.;[actorjs,js]`
      },
      {
        path: ActorPathData.getTestDataEnvironmentLocalFile(),
        default: []
      },
      {
        path: ActorPathData.getTestDataGeneralLocalFile(),
        default: []
      },
      {
        path: ActorPathData.getLicensesFile(),
        default: []
      },
      {
        path: ActorPathData.getDependenciesFile(),
        default: []
      },
      {
        path: ActorPathData.getTestDataOutputLocalFile(),
        default: [/*
          {
            name: 'log-type-engine',
            value: 'all',
            description: 'none, filter or all'
          },
          {
            name: 'log-type-debug',
            value: 'all',
            description: 'none, filter or all'
          },
          {
            name: 'log-type-error',
            value: 'all',
            description: 'none, filter or all'
          },
          {
            name: 'log-type-warning',
            value: 'all',
            description: 'none, filter or all'
          },
          {
            name: 'log-type-ip',
            value: 'all',
            description: 'none, filter or all'
          },
          {
            name: 'log-type-success',
            value: 'all',
            description: 'none, filter or all'
          },
          {
            name: 'log-type-failure',
            value: 'all',
            description: 'none, filter or all'
          },
          {
            name: 'log-type-test-data',
            value: 'all',
            description: 'none, filter or all'
          }
        */]
      },
      {
        path: ActorPathData.getTestDataSystemLocalFile(),
        default: []
      },
      {
        path: ActorPathData.getSystemUnderTestActorFile(),
        default: {
          name: 'Actor',
          description: 'Default System Under Test. The tool it self acts as the system under test.',
          status: 'static'
        }
      },
      {
        path: ActorPathData.getContentTextLocalFile(),
        default: []
      },
      {
        path: ActorPathData.getContentDocumentsLocalFile(),
        default: []
      },
      {
        path: ActorPathData.getContentImageLocalFile(),
        default: []
      },
      {
        path: ActorPathData.getContentVideoLocalFile(),
        default: []
      },
      {
        path: ActorPathData.getContentAudioLocalFile(),
        default: []
      },
      {
        path: ActorPathData.getContentOtherLocalFile(),
        default: []
      },
      {
        path: ActorPathData.getLocalReposFile(),
        default: {
          actorjsContentRepos: [],
          actorjsDataRepos: []
        }
      },
      {
        path: ActorPathData.getReadMeFile(),
        text: `# Abstraktor AB #

## actorjs-data-local - repo ##`
      }
    ];
    if(ActorPath.releaseStepDevelopment) {
      this.filePaths.push({
        path: ActorPathData.getAddressesGlobalNetworksFile(),
        default: []
      });
      this.filePaths.push({
        path: ActorPathData.getAddressesGlobalInterfacesSutFile(),
        default: []
      });
      this.filePaths.push({
        path: ActorPathData.getAddressesGlobalInterfacesClientFile(),
        default: []
      });
      this.filePaths.push({
        path: ActorPathData.getAddressesGlobalInterfacesServerFile(),
        default: []
      });
      this.filePaths.push({
        path: ActorPathData.getAddressesGlobalAddressesSutFile(),
        default: []
      });
      this.filePaths.push({
        path: ActorPathData.getAddressesGlobalAddressesClientFile(),
        default: []
      });
      this.filePaths.push({
        path: ActorPathData.getAddressesGlobalAddressesServerFile(),
        default: []
      });
      this.filePaths.push({
        path: ActorPathData.getAddressesGlobalSrcFile(),
        default: []
      });
      this.filePaths.push({
        path: ActorPathData.getAddressesGlobalDnsFile(),
        default: []
      });
      this.filePaths.push({
        path: ActorPathData.getAddressesGlobalDstFile(),
        default: []
      });
      this.filePaths.push({
        path: ActorPathData.getAddressesGlobalSrvFile(),
        default: []
      });
      this.filePaths.push({
        path: ActorPathData.getAddressesGlobalPortsFile(),
        default: []
      });
      this.filePaths.push({
        path: ActorPathData.getTestDataEnvironmentGlobalFile(),
        default: []
      });
      this.filePaths.push({
        path: ActorPathData.getTestDataGeneralGlobalFile(),
        default: []
      });
      this.filePaths.push({
        path: ActorPathData.getTestDataOutputGlobalFile(),
        default: []
      });
      this.filePaths.push({
        path: ActorPathData.getTestDataSystemGlobalFile(),
        default: []
      });
      this.filePaths.push({
        path: ActorPathData.getContentTextGlobalFile(),
        default: []
      });
      this.filePaths.push({
        path: ActorPathData.getContentDocumentsGlobalFile(),
        default: []
      });
      this.filePaths.push({
        path: ActorPathData.getContentImageGlobalFile(),
        default: []
      });
      this.filePaths.push({
        path: ActorPathData.getContentAudioGlobalFile(),
        default: []
      });
      this.filePaths.push({
        path: ActorPathData.getContentVideoGlobalFile(),
        default: []
      });
      this.filePaths.push({
        path: ActorPathData.getContentOtherGlobalFile(),
        default: []
      });
    }
  }
  
  verifyOrCreate(done) {
    new ActorPathCreator(this.folderPaths, this.filePaths).verifyOrCreate(done);
  }
}

module.exports = ActorDataPaths;
