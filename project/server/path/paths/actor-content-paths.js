
'use strict';

const ActorPathCreator = require('../actor-path-creator');
const ActorPathContent = require('../actor-path-content');
const ActorPath = require('../actor-path');


class ActorContentPaths {
  constructor() {
    this.folderPaths = [
      ActorPathContent.getContentFolder(),
      ActorPathContent.getContentLocalFolder()
    ];
    if(ActorPath.releaseStepDevelopment) {
      this.folderPaths.push(ActorPathContent.getContentGlobalFolder());
    }
    this.filePaths = [
      {
        path: ActorPathContent.getReadMeFile(),
        text: `# Abstraktor AB #

## actorjs-content-local - repo ##`
      }
    ];
  }
  
  verifyOrCreate(done) {
    new ActorPathCreator(this.folderPaths, this.filePaths).verifyOrCreate(done);
  }
}

module.exports = ActorContentPaths;
