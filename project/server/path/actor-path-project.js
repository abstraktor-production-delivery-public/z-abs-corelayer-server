
'use strict';

const ActorPath = require('./actor-path');
const Path = require('path');


class ActorPathProject {
  constructor() {
    this.codeProjectFolder = ActorPath.setPath(ActorPath.getActorPath(), `${Path.sep}project`);
    this.pluginPathPart = `${Path.sep}..${Path.sep}..${Path.sep}..${Path.sep}..${Path.sep}..${Path.sep}..`;
    if(ActorPath.releaseStepDelivery) {
      this.projectRelativePath = `.${Path.sep}`;
      this.projectResolvedPath = `${Path.resolve(this.projectRelativePath)}${Path.sep}`;
      this.externalRelativePath = `.${Path.sep}node_modules${Path.sep}${ActorPath.organization}${Path.sep}`;
      this.externalResolvedPath = `${Path.resolve(this.externalRelativePath)}${Path.sep}`;
      this.pluginRelativePath = `..${Path.sep}..${Path.sep}..${Path.sep}..${Path.sep}`;
      this.pluginResolvedPath = `${Path.resolve(this.pluginRelativePath)}${Path.sep}`;
    }
    else if(ActorPath.releaseStepCandidate) {
      this.projectRelativePath = `.${Path.sep}`;
      this.projectResolvedPath = `${Path.resolve(this.projectRelativePath)}${Path.sep}`;
      this.externalRelativePath = `.${Path.sep}node_modules${Path.sep}`;
      this.externalResolvedPath = `${Path.resolve(this.externalRelativePath)}${Path.sep}`;
      this.pluginRelativePath = `..${Path.sep}..${Path.sep}..${Path.sep}..${Path.sep}`;
      this.pluginResolvedPath = `${Path.resolve(this.pluginRelativePath)}${Path.sep}`;
    }
    else {
      this.projectRelativePath = `.${Path.sep}`;
      this.projectResolvedPath = `${Path.resolve(this.projectRelativePath)}${Path.sep}`;
      this.externalRelativePath = `..${Path.sep}`;
      this.externalResolvedPath = `${Path.resolve(this.externalRelativePath)}${Path.sep}`;
      this.pluginRelativePath = `..${Path.sep}`;
      this.pluginResolvedPath = `${Path.resolve(this.pluginRelativePath)}${Path.sep}`;
    }
    this.clientJsx = 'clientJsx';
    this.clientJs = 'clientJs';
    this.clientServerJs = 'clientServerJs';
    this.serverWorkerLogJs = 'serverWorkerLogJs';
    this.serverWorkerActorJs = 'serverWorkerActorJs';
    this.serverJs = 'serverJs';
    this.apiJs = 'apiJs';
  }
  
  getCacheName(path, projectType) {
    if(projectType) {
      if('server' === projectType) {
        return this.serverJs;
      }
      else if('clientServer' === projectType) {
        return this.clientServerJs;
      }
      else if('client' === projectType) {
        if(path.endsWith('jsx')) {
          return this.clientJsx;
        }
        if(path.endsWith('js')) {
          return this.clientJs;
        }
      }
    }
    else {
      if(path.startsWith('./project/server')) {
        return this.serverJs;
      }
      else if(path.startsWith('./project/clientServer')) {
        return this.clientServerJs;
      }
      else if(path.startsWith('./project/client')) {
        if(path.endsWith('jsx')) {
          return this.clientJsx;
        }
        if(path.endsWith('js')) {
          return this.clientJs;
        }
      }
      else if(path.startsWith('./project/api')) {
        return this.apiJs;
      }
      else if(path.startsWith('./project/serverWorkers/serverWorkerLog')) {
        return this.serverWorkerLogJs;
      }
      else if(path.startsWith('./project/serverWorkers/serverWorkerActor')) {
        return this.serverWorkerActorJs;
      }
    }
  }
  
  getWorkspaceFile(appName, workspaceName) {
    return `${ActorPath.getActorParentPath()}${appName}${Path.sep}workspace${Path.sep}${workspaceName}.wrk`;
  }
  
  getCodeProjectFolder(name, type, workspaceName) {
    if(!name) {
      if(workspaceName) {
        return `${ActorPath.getActorParentPath()}${Path.sep}${workspaceName}${Path.sep}project`;
      }
      else {
        return this.codeProjectFolder;
      }
    }
    else if(!type) {
      return `${this.externalResolvedPath}${Path.sep}${name}${Path.sep}project`;
    }
    else {
      return `${this.externalResolvedPath}${Path.sep}${name}${Path.sep}project${Path.sep}${type}`;
    }
  }
  
  getCodeProjectPureFile(name, type, isExternalPlugin) {
    if(!type) {
      return `..${Path.sep}${name}${Path.sep}project${Path.sep}${name}-project.tree`;
    }
    else {
      if(!isExternalPlugin) {
        return Path.resolve(`${this.externalResolvedPath}${Path.sep}${name}${Path.sep}project${Path.sep}${name}.tree`);
      }
      else {
        return Path.resolve(`${this.pluginResolvedPath}${Path.sep}${name}${Path.sep}project${Path.sep}${name}.tree`);
      }
    }
  }
  
  getFile(fileName, projectType, isExternalPlugin, workspaceName) {
    if(!projectType) {
      if(workspaceName) {
        return Path.normalize(`${ActorPath.getActorParentPath()}${workspaceName}${fileName.substring(1).replace(new RegExp('[/\\\\]', 'g'), Path.sep)}`);
      }
      else {
        return Path.normalize(`${this.projectResolvedPath}${fileName.replace(new RegExp('[/\\\\]', 'g'), Path.sep)}`);
      }
    }
    else {
      const secondSlash = fileName.indexOf('/', 2);
      if(-1 === secondSlash) {
        throw new Error(`fileName: '${fileName}' is not a external project filename.`);
      }
      if(!isExternalPlugin) {
       return Path.normalize(`${this.externalResolvedPath}${fileName.substring(0, secondSlash)}${Path.sep}project${Path.sep}${fileName.substring(secondSlash + 1)}`.replace(new RegExp('[/\\\\]', 'g'), Path.sep));
      }
      else {
        return Path.normalize(`${this.pluginResolvedPath}${fileName.substring(0, secondSlash)}${Path.sep}project${Path.sep}${fileName.substring(secondSlash + 1)}`.replace(new RegExp('[/\\\\]', 'g'), Path.sep));
      }
    }
  }
  
  getRelativePath(path, projectType, isExternalPlugin) {
    if(!projectType) {
     return Path.normalize(`${this.projectRelativePath}${path.replace(new RegExp('[/\\\\]', 'g'), Path.sep)}`);
    }
    else {
      const secondSlash = path.indexOf('/', 2);
      if(-1 === secondSlash) {
        throw new Error(`path: '${path}' is not a external project path.`);
      }
      if(!isExternalPlugin) {
       return Path.normalize(`${this.externalRelativePath}${path.substring(0, secondSlash)}${Path.sep}project${Path.sep}${path.substring(secondSlash + 1)}`.replace(new RegExp('[/\\\\]', 'g'), Path.sep));
      }
      else {
        return Path.normalize(`${this.pluginRelativePath}${path.substring(0, secondSlash)}${Path.sep}project${Path.sep}${path.substring(secondSlash + 1)}`.replace(new RegExp('[/\\\\]', 'g'), Path.sep));
     }
    }
  }
  
  getRawPath(projectType, isExternalPlugin) {
    if(!projectType) {
      return Path.normalize(this.projectResolvedPath);
    }
    else {
      if(!isExternalPlugin) {
        return Path.normalize(this.externalResolvedPath);
      }
      else {
        return Path.normalize(this.pluginResolvedPath);
     }
    }
  }
}


module.exports = new ActorPathProject();
