
'use strict';

const Fs = require('fs');


class ActorPathCreator {
  constructor(folderPaths, filePaths) {
    this.folderPaths = folderPaths;
    this.filePaths = filePaths;
    this.indexFolder = this.folderPaths.length;
    this.indexFile = this.filePaths.length;
  }
  
  verifyOrCreate(done) {
    if(0 !== this.indexFolder) {
      this.verifyOrCreateFolders((errorsFolder) => {
        if(0 !== errorsFolder.length) {
          done(errorsFolder);
        }
        else {
          if(0 !== this.indexFile) {
            this.verifyOrCreateFiles((errorsFile) => {
              done(0 !== errorsFile.length ? errorsFile : undefined);
            });
          }
          else {
            process.nextTick(() => {
              done();
            });
          }
        }
      });
    }
    else {
      process.nextTick(() => {
        done();
      });
    }
  }
  
  verifyOrCreateFolders(done) {
    let pendings = 0;
    const errors = [];
    this.folderPaths.forEach((folder) => {
      ++pendings;
      Fs.access(folder, Fs.R_OK | Fs.W_OK, (err) => {
        if(err) {
          Fs.mkdir(folder, { recursive: true }, (e) => {
            if(e) {
              errors.push(e);
            }
            if(0 === --pendings) {
              done(errors);
            }
          });
        }
        else {
          if(0 === --pendings) {
            done(errors);
          }
        }
      });    
    });
  }

  verifyOrCreateFiles(done) {
    let pendings = 0;
    const errors = [];
    this.filePaths.forEach((file) => {
      ++pendings;
      Fs.access(file.path, Fs.R_OK | Fs.W_OK, (err) => {
        if(err) {
          Fs.writeFile(file.path, file.default ? JSON.stringify(file.default, null, 2) : file.text, (e) => {
            if(e) {
              errors.push(e);
            }
            if(0 === --pendings) {
              done(errors);
            }
          });
        }
        else {
          if(0 === --pendings) {
            done(errors);
          }
        }
      });
    });
  }
}

module.exports = ActorPathCreator;
