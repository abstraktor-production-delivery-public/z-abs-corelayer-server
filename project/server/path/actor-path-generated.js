
'use strict';

const ActorPath = require('./actor-path');
const Path = require('path');


class ActorPathGenerated {
  constructor() {
    this.actor = 'Actor';
    
    if(ActorPath.releaseStepDelivery) {
      this.actorGeneratedFolder  = ActorPath.setPath(ActorPath.getActorPath(), `..${Path.sep}..${Path.sep}..${Path.sep}Generated`);
    }
    else {
      this.actorGeneratedFolder  = ActorPath.setPath(ActorPath.getActorPath(), `..${Path.sep}Generated`);
    }
    
    this.actorGeneratedStagingFolder = ActorPath.setPath(this.actorGeneratedFolder, `${Path.sep}Staging`);
    this.actorGeneratedTestCaseFolder = ActorPath.setPath(this.actorGeneratedFolder, `${Path.sep}TestCase`);
    this.actorGeneratedStagingButtonsFolder = ActorPath.setPath(this.actorGeneratedStagingFolder, `${Path.sep}StagingButtons`);
    this.actorGeneratedTestCaseButtonsFolder = ActorPath.setPath(this.actorGeneratedTestCaseFolder, `${Path.sep}TestCaseButtons`);
    this.actorGeneratedTestSuiteFolder = ActorPath.setPath(this.actorGeneratedFolder, `${Path.sep}TestSuite`);
    this.actorGeneratedTestSuiteButtonsFolder = ActorPath.setPath(this.actorGeneratedTestSuiteFolder, `${Path.sep}TestSuiteButtons`);
    this.actorGeneratedTestCaseDebuggerFolder = ActorPath.setPath(this.actorGeneratedFolder, `${Path.sep}Debugger`);
    this.actorGeneratedWorkspaceFolder = ActorPath.setPath(this.actorGeneratedFolder, `${Path.sep}Workspace`);
    this.actorGeneratedProjectsFolder = ActorPath.setPath(this.actorGeneratedWorkspaceFolder, `${Path.sep}Projects`);
    this.actorGeneratedDatabasesFolder = ActorPath.setPath(this.actorGeneratedFolder, `${Path.sep}Databases`);
    this.actorGeneratedDatabasesGitFolder = ActorPath.setPath(this.actorGeneratedDatabasesFolder, `${Path.sep}Git`);
    this.actorGeneratedDatabasesNpmFolder = ActorPath.setPath(this.actorGeneratedDatabasesFolder, `${Path.sep}Npm`);
    this.actorGeneratedBuildFolder = ActorPath.setPath(this.actorGeneratedFolder, `${Path.sep}Build`);
    
    this.actorGeneratedStagingFile = ActorPath.setPath(this.actorGeneratedStagingFolder, `${Path.sep}staging.json`);
    this.actorGeneratedNetworksFile = ActorPath.setPath(this.actorGeneratedStagingFolder, `${Path.sep}networks.json`);
    this.actorGeneratedAddressesFile = ActorPath.setPath(this.actorGeneratedStagingFolder, `${Path.sep}addresses.json`);
    this.actorGeneratedLocalDnsFile = ActorPath.setPath(this.actorGeneratedStagingFolder, `${Path.sep}local-dns.json`);
    this.actorGeneratedDependenciesFile = ActorPath.setPath(this.actorGeneratedStagingFolder, `${Path.sep}dependencies.json`);
    
    this.actorGeneratedStagingReport = ActorPath.setPath(this.actorGeneratedStagingFolder, `${Path.sep}staging-report.json`);
    this.actorGeneratedChosenAddresses = ActorPath.setPath(this.actorGeneratedStagingFolder, `${Path.sep}chosen-addresses.json`);
    this.actorGeneratedTestCasesRecentFile = ActorPath.setPath(this.actorGeneratedTestCaseFolder, `${Path.sep}testCaseRecent.json`);
    this.actorGeneratedStagingButtonsFile = ActorPath.setPath(this.actorGeneratedStagingButtonsFolder, `${Path.sep}stagingButtons`);
    this.actorGeneratedTestCaseButtonsFile = ActorPath.setPath(this.actorGeneratedTestCaseButtonsFolder, `${Path.sep}testCaseButtons`);
    this.actorGeneratedTestSuiteButtonsFile = ActorPath.setPath(this.actorGeneratedTestSuiteButtonsFolder, `${Path.sep}testSuiteButtons`);
    this.actorGeneratedTestSuitesRecentFile = ActorPath.setPath(this.actorGeneratedTestSuiteFolder, `${Path.sep}testSuiteRecent.json`);
    this.actorGeneratedWorkspaceFile = ActorPath.setPath(this.actorGeneratedWorkspaceFolder, `${Path.sep}workspaceRecent.json`);
  }
  
  getActorGeneratedFolder() {
    return this.actorGeneratedFolder;
  }
  
  getStagingFolder() {
    return this.actorGeneratedStagingFolder;
  }
  
  getStagingFile() {
    return this.actorGeneratedStagingFile;
  }
  
  getNetworksFile() {
    return this.actorGeneratedNetworksFile;
  }
  
  getAddressesFile() {
    return this.actorGeneratedAddressesFile;
  }
  
  getLocalDnsFile() {
    return this.actorGeneratedLocalDnsFile;
  }
  
  getStagingReport() {
    return this.actorGeneratedStagingReport;
  }
  
  getChosenAddresses() {
    return this.actorGeneratedChosenAddresses;
  }
  
  getDependenciesFile() {
    return this.actorGeneratedDependenciesFile;
  }
  
  getTestCaseFolder() {
    return this.actorGeneratedTestCaseFolder;
  }
  
  getStagingButtonsFolder() {
    return this.actorGeneratedStagingButtonsFolder;
  }
  
  getTestCaseButtonsFolder() {
    return this.actorGeneratedTestCaseButtonsFolder;
  }
  
  getTestSuiteButtonsFolder() {
    return this.actorGeneratedTestSuiteButtonsFolder;
  }
  
  getTestCasesRecentFile() {
    return this.actorGeneratedTestCasesRecentFile;
  }
  
  getStagingButtonsFile(key) {
    return `${this.actorGeneratedStagingButtonsFile}_${key}.json`;
  }
  
  getTestCaseButtonsFile(key) {
    return `${this.actorGeneratedTestCaseButtonsFile}_${key}.json`;
  }
  
  getTestCasesButtonsFile() {
    return this.actorGeneratedTestCaseButtonsFile;
  }
  
  getTestSuiteButtonsFile(key) {
    return `${this.actorGeneratedTestSuiteButtonsFile}_${key}.json`;
  }
  
  getTestSuitesButtonsFile() {
    return this.actorGeneratedTestSuiteButtonsFile;
  }
  
  getTestSuiteFolder() {
    return this.actorGeneratedTestSuiteFolder;
  }
  
  getTestSuitesRecentFile() {
    return this.actorGeneratedTestSuitesRecentFile;
  }
  
  getTestDebuggerFolder() {
    return this.actorGeneratedTestCaseDebuggerFolder;
  }
  
  getTestCaseDebuggerFolder(sut, fut) {
    return `${this.actorGeneratedTestCaseDebuggerFolder}${Path.sep}Sut${sut}${Path.sep}Fut${fut}`;
  }
  
  getTestCaseDebuggerFile(sut, fut, tc) {
    return `${this.actorGeneratedTestCaseDebuggerFolder}${Path.sep}Sut${sut}${Path.sep}Fut${fut}${Path.sep}Tc${tc}.json`;
  }
  
  getWorkspaceFolder() {
    return this.actorGeneratedWorkspaceFolder;
  }
  
  getProjectsFolder() {
    return this.actorGeneratedProjectsFolder;
  }
  
  getDatabasesFolder() {
    return this.actorGeneratedDatabasesFolder;
  }
  
  getDatabasesGitFolder() {
    return this.actorGeneratedDatabasesGitFolder;
  }
  
  getDatabasesNpmFolder() {
    return this.actorGeneratedDatabasesNpmFolder;
  }
  
  getBuildFolder() {
    return this.actorGeneratedBuildFolder;
  }
  
  getWorkspaceRecentFiles() {
    return this.actorGeneratedWorkspaceFile;
  }
  
  getProjectFile(name, type) {
    if(!type) {
      return `${this.actorGeneratedProjectsFolder}${Path.sep}${name}-project.json`;
    }
    else {
      return `${this.actorGeneratedProjectsFolder}${Path.sep}${name}.json`;
    }
  }
  
  getActorsProjectFile(path) {
    if(path.startsWith('./Actors-global')) {
      return `${this.actorGeneratedProjectsFolder}${Path.sep}actors-global-project.json`;
    }
    else if(path.startsWith('./Actors-local')) {
      return `${this.actorGeneratedProjectsFolder}${Path.sep}actors-local-project.json`;
    }
  }
  
  getStacksProjectFile(path) {
    if(path.startsWith('./Stacks-global')) {
      return `${this.actorGeneratedProjectsFolder}${Path.sep}stacks-global-project.json`;
    }
    else if(path.startsWith('./Stacks-local')) {
      return `${this.actorGeneratedProjectsFolder}${Path.sep}stacks-local-project.json`;
    }
  }
}

module.exports = new ActorPathGenerated();
