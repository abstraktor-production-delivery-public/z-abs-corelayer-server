
'use strict';

const WorkerChannel = require('./worker-channel');


class WorkerMain {
  constructor() {
    this.id = -1;
    this.parentObject = null;
    this.typeName = '';
    this.instanceId = -1;
    this.globalInstanceId = -1;
    this.workerPool = null;
    this.workerCore = null;
    this.channel = null;
    this.cbStart = null;
    this.cbStop = null;
    this.state = WorkerMain.STATE_NONE;
  }
  
  _init(typeName, instanceId, globalInstanceId, workerPool, workerCore) {
    this.typeName = typeName;
    this.instanceId = instanceId;
    this.globalInstanceId = globalInstanceId;
    this.workerPool = workerPool;
    this.workerCore = workerCore;
  }
  
  start(id, parentObject, cbStart, cbStop) {
    this.id = id;
    this.workerCore.id = id;
    this.parentObject = parentObject;
    this.cbStart = cbStart;
    this.cbStop = cbStop;
    if(WorkerMain.STATE_NONE !== this.state && WorkerMain.STATED_STOPPED !== this.state) {
      process.nextTick(() => {
        if(this.cbStart) {
          this.cbStart(new Error(`Can not start Worker in state: '${WorkerMain.STATES[this.state]}'.`));
          this.cbStart = null;
        }
      });
      return;
    }
    if(null === this.channel) {
      this.workerCore.start((exitCode) => {
        this.state = WorkerMain.STATED_KILLED;
        if(this.cbStop) {
          if(this.channel) {
            this.channel.dead(() => {
              if(this.cbStart) {
                this.cbStart(new Error('Can not start Worker, it died. exitCode: ' + exitCode + '.'));
                this.cbStart = null;
              }
              if(this.cbStop) {
                this.cbStop(exitCode);
                this.cbStop = null;
                this.channel = null;
              }
            });
          }
          else {
            this.cbStop(exitCode);
            this.cbStop = null;
          }
        }
      });
      this.channel = new WorkerChannel(id, WorkerChannel.TYPE_MAIN, this.workerCore.worker);
      this.workerCore.worker.on('message', (message) => {
        if(WorkerChannel.MSG_RESPONSE === message.type) {
          if(this.channel) {
            this.channel.handleResponse(message);
          }
          else {
            ddb.warning('DROPPED', message);
          }
        }
        else {
          const func = Reflect.get(this.parentObject, `on${message.msgId}`).bind(this.parentObject);
          if(null !== func) {
            if(WorkerChannel.MSG_MESSAGE === message.type) {
              func(this.id, ...message.params);
            }
            else if(WorkerChannel.MSG_METHOD === message.type) {
              func((err, ...params) => {
                if(this.channel) {
                  this.channel.response(message, err, ...params);
                }
                else {
                  ddb.warning('DROPPED', message);
                }
              }, ...message.params);
            }
          }
          else {
           ddb.error(`Error function 'on${message.msgId}' not found.`);
          }
        }
      });
    }
    this.channel.start(this.id);
    this.channel.command('start', (err) => {
      this.state = WorkerMain.STATE_STARTED;
      if(this.cbStart) {
        this.cbStart(err);
        this.cbStart = null;
      }
    }, undefined, id);
  }
  
  stop(done) {
    if(WorkerMain.STATE_STARTED !== this.state) {
      process.nextTick(done);
      return;
    }
    this.channel.stop((cbStopped) => {
      this.channel.command('stop', (err) => {
        cbStopped();
        this.state = WorkerMain.STATED_STOPPED;
        if(this.cbStop) {
          this.cbStop(-1);
          this.cbStop = null;
        }
        if(!err && !this.workerCore.error) {
          //this.id = -1;
          this.workerCore.id = -1;
          this.workerPool.store(this);
        }
        else {
          this.channel = null;
        }
        done(err);
      }, undefined);
    });
  }
  
  kill(done) {
    if(WorkerMain.STATE_STARTED !== this.state && WorkerMain.STATED_STOPPED !== this.state) {
      process.nextTick(done);
      return;
    }
    if(this.channel) {
      this.channel.kill((cbKilled) => {
        this.channel.command('kill', (err) => {
          cbKilled();
          this.state = WorkerMain.STATED_KILLED;
          this.channel = null;
          done(err);
        }, undefined);
      });
    }
    else {
      process.nextTick(done);
    }
  }
}

WorkerMain.STATE_NONE = 0;
WorkerMain.STATE_STARTED = 1;
WorkerMain.STATED_STOPPED = 2;
WorkerMain.STATED_KILLED = 3;
WorkerMain.STATES = ['NONE', 'STARTED', 'STOPPED', 'KILLED'];


module.exports = WorkerMain;
