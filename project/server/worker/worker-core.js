
'use strict';

const HighResolutionTimestamp = require('../high-resolution-timestamp');
const { Worker, isMainThread, parentPort, workerData, threadId } = require('worker_threads');

if(isMainThread) {
  
  class WorkerCore {
    constructor(typeName, instanceId, factoryThreadPath, workerPool) {
      this.typeName = typeName;
      this.instanceId = instanceId;
      this.factoryThreadPath = factoryThreadPath;
      this.workerPool = workerPool;
      this.id = -1;
      this.worker = null;
      this.error = null;
    }
    
    start(cbExit) {
      const releaseData = Reflect.get(global, 'release-data@abstractor');
      this.worker = new Worker(__filename, {
        workerData: {
          appName: releaseData.appName,
          releaseStep: releaseData.releaseStep,
          organization: releaseData.organization,
          path: this.factoryThreadPath,
          dateStamp: HighResolutionTimestamp.dateStamp,
          timeStamp: HighResolutionTimestamp.timeStamp
        }
      });
      this.worker.on('exit', (exitCode) => {
        this.worker = null;
        //ddb.info(`Worker[${this.id}] exitCode:${exitCode}`, this.typeName);
        process.nextTick(cbExit, exitCode);
      });
      this.worker.on('error', (err) => {
        ddb.error('WORKER ERROR:', err);
        this.error = err;
      });
    }
  }

module.exports = WorkerCore;

}
else {
  Reflect.set(global, 'release-data@abstractor', {
    appName: workerData.appName,
    releaseStep: workerData.releaseStep,
    organization: workerData.organization
  });
  HighResolutionTimestamp.workerInit(workerData.dateStamp, workerData.timeStamp);
  const workerThread = new (require(workerData.path))();
  workerThread._init(parentPort);
}

