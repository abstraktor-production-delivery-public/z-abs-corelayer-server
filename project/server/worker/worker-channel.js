
'use strict';


class WorkerChannel {
  constructor(id, type, port) {
    this.id = id;
    this.type = type;
    this.port = port;
    this.responses = new Map();
    this.currentResponseId = 0;
    this.cbExitKill = null;
    this.exitType = WorkerChannel.CB_NONE;
    this.state = WorkerChannel.STATE_OPEN;
  }
  
  start() {
    this.state = WorkerChannel.STATE_OPEN;
  }
  
  stop(cbExit) {
    if(0 === this.responses.size) {
      cbExit(() => {
        this.state = WorkerChannel.STATE_STOPPED;
      });
    }
    else {
      this.cbExitKill = cbExit;
      this.exitType = WorkerChannel.CB_EXIT;
    }
  }
  
  kill(cbKill) {
    if(0 === this.responses.size) {
      cbKill(() => {
        this.state = WorkerChannel.STATE_KILLED;
      });
    }
    else {
      this.cbExitKill = cbKill;
      this.exitType = WorkerChannel.CB_KILL;
    }
  }
  
  dead(done) {
    this.responses.forEach((response) => {
      response.done();
    });
    process.nextTick(done);
  }
  
  command(msgId, done, transferList, ...params) {
    const responseId = this.currentResponseId++;
    this.responses.set(responseId, {
      msgId,
      type: 'request',
      done
    });
    this.port.postMessage({
      type: WorkerChannel.MSG_COMMAND,
      msgId: msgId,
      responseId: responseId,
      params: params
    }, transferList);
  }
  
  request(msgId, done, transferList, ...params) {
    if(WorkerChannel.STATE_OPEN === this.state) {
      const responseId = this.currentResponseId++;
      this.responses.set(responseId, {
        msgId,
        type: 'request',
        done
      });
      this.port.postMessage({
        type: WorkerChannel.MSG_REQUEST,
        msgId: msgId,
        responseId: responseId,
        params: params
      }, transferList);
    }
    else {
      ddb.warning(this.id, 'Channel not open(request).', msgId, params);
      process.nextTick(() => {
        done(new Error('Channel not open.'), msgId, params);
      });
    }
  }
  
  response(message, ...params) {
    this.port.postMessage({
      type: WorkerChannel.MSG_RESPONSE,
      responseId: message.responseId,
      msgId: message.msgId,
      params: params
    });
  }
  
  method(msgId, done, transferList, ...params) {
    if(WorkerChannel.STATE_OPEN === this.state) {
      const responseId = this.currentResponseId++;
      this.responses.set(responseId, {
        msgId,
        type: 'method',
        done
      });
      this.port.postMessage({
        type: WorkerChannel.MSG_METHOD,
        msgId: msgId,
        responseId: responseId,
        params: params
      }, transferList);
    }
    else {
      ddb.warning(this.id, 'Channel not open(method).', msgId, params);
      process.nextTick(() => {
        done(new Error('Channel not open(method).'));
      });
    }
  }
  
  message(msgId, transferList, ...params) {
    if(WorkerChannel.STATE_OPEN === this.state) {
      this.port.postMessage({
        type: WorkerChannel.MSG_MESSAGE,
        msgId: msgId,
        params: params
      }, transferList);
    }
    else {
      ddb.warning(this.id, 'Channel not open(message).', msgId, params);
      /*process.nextTick(() => {
        done(new Error('Channel not open.'));
      });*/
    }
  }
  
  handleResponse(message, cb) {
    const cbResponse = this.responses.get(message.responseId);
    if(cb) {
      cb(message.msgId, ...message.params);
    }
    if(cbResponse) {
      this.responses.delete(message.responseId);
      cbResponse.done(...message.params);
      if(this.cbExitKill) {
        if(0 === this.responses.size) {
          if(WorkerChannel.STATE_KILLED === this.state) {
          }
          this.cbExitKill(() => {
            this.state = this.exitType;
          });
          this.cbExitKill = null;
          this.exitType = WorkerChannel.CB_NONE;
        }    
      }
    }
  }
}  

WorkerChannel.MSG_COMMAND = 0;
WorkerChannel.MSG_REQUEST = 1;
WorkerChannel.MSG_RESPONSE = 2;
WorkerChannel.MSG_METHOD = 3;
WorkerChannel.MSG_MESSAGE = 4;

WorkerChannel.MSG_TYPES = ['COMMAND', 'REQUEST', 'RESPONSE', 'METHOD', 'MESSAGE'];

WorkerChannel.STATE_OPEN = 0;
WorkerChannel.STATE_STOPPED = 1;
WorkerChannel.STATE_KILLED = 2;

WorkerChannel.TYPE_MAIN = 0;
WorkerChannel.TYPE_THREAD = 1;
WorkerChannel.TYPES = ['MAIN', 'THREAD'];

WorkerChannel.CB_NONE = -1;
WorkerChannel.CB_EXIT = 0;
WorkerChannel.CB_KILL = 1;

module.exports = WorkerChannel;
