
'use strict';

const WorkerChannel = require('./worker-channel');


class WorkerThread {
  constructor() {
    this.id = -1;
    this.channel = null;
    this.parentPort = null;
    this.state = WorkerThread.STATE_NONE;
  }
  
  handleCommand(message) {
    if('start' === message.msgId) {
      this.state = WorkerThread.STATE_STARTED;
      this.id = message.params[0];
      this.channel.start(this.id);
      if(this.onStart) {
        this.channel.id = this.id;
        this.onStart(this.id, () => {
          this.channel.response(message);  
        });
      }
      else {
        process.nextTick(() => {
          this.channel.response(message);
        });
      }
    }
    else if('stop' === message.msgId) {
      this.channel.stop((cbStopped) => {
        if(this.onStop) {
          this.onStop(() => {
            this.state = WorkerThread.STATED_STOPPED;
            this.channel.response(message);
            cbStopped();
          });
        }
        else {
          this.state = WorkerThread.STATED_STOPPED;
          process.nextTick(() => {
            this.channel.response(message);
            cbStopped();
          });
        }
      });
    }
    else if('kill' === message.msgId) {
      this.state = WorkerThread.STATED_KILLED;
      this.channel.kill((cbKilled) => {
        this.parentPort.unref();
        this.parentPort = null;
        this.channel.response(message);
        cbKilled();
      });
    }
  }
  
  handleRequest(message) {
    const func = Reflect.get(this, message.msgId).bind(this);
    if(func) {
      func(...message.params, (...params) => {
        this.channel.response(message, ...params);
      });
    }
    else {
      throw new Error(`Not Implemented. '${message.msgId}'`);
    }
  }
  
  _init(parentPort) {
    this.parentPort = parentPort;
    this.channel = new WorkerChannel(-1, WorkerChannel.TYPE_THREAD, parentPort);
    parentPort.on('message', (message) => {
      if(WorkerChannel.MSG_COMMAND === message.type) {
        try {
          this.handleCommand(message);
        }
        catch(err) {
          ddb.error(err.message);
          this.channel.response(message, err);
        }
      }
      else if(WorkerChannel.MSG_REQUEST === message.type) {
        try {
          this.handleRequest(message);
        }
        catch(err) {
          ddb.error(err.message);
          this.channel.response(message, err);
        }
      }
      else if(WorkerChannel.MSG_RESPONSE === message.type) {
        try {
          this.channel.handleResponse(message);
        }
        catch(err) {
          ddb.error(err);
        }
      }
      else if(WorkerChannel.MSG_MESSAGE === message.type) {
        try {
          ddb.error('Not Implemented.');
        }
        catch(err) {
          ddb.error(err);
        }
      }
      else {
        ddb.warning('DROPPED message. type = ', WorkerChannel.MSG_TYPES[message.type]);
      }
    });
    parentPort.on('messageerror', (err) => {
      ddb.error('messageerror', err);
    });
  }
}

WorkerThread.STATE_NONE = 0;
WorkerThread.STATE_STARTED = 1;
WorkerThread.STATED_STOPPED = 2;
WorkerThread.STATED_KILLED = 3;
WorkerThread.STATES = ['NONE', 'STARTED', 'STOPPED', 'KILLED'];


module.exports = WorkerThread;
