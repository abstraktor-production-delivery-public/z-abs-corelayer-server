
'use strict';

const WorkerCore = require('./worker-core');


class WorkerPool {
  constructor() {
    if(1 === ++WorkerPool.global.instancesPool) {
      WorkerPool.global.intervalId = setInterval((workersQueue) => {
        workersQueue.forEach((workersTypeData, typeName) => {
          const now = process.hrtime.bigint();
          while(1 <= workersTypeData.queue.length) {
            let timestamp = workersTypeData.queue[0].timestamp;
            if(now - timestamp < WorkerPool.TIMEOUT_BIG) {
              break;
            }
            else {
              const workerMain = workersTypeData.queue.shift().workerMain;
              workerMain.kill(() => {
                WorkerPool.global.workersInstances.delete(workerMain.globalInstanceId);
              });
            }
          }
        });
      }, WorkerPool.TIMEOUT, WorkerPool.global.workersQueue);
    }
  }
  
  release() {
    if(0 === --WorkerPool.global.instancesPool) {
      clearInterval(WorkerPool.global.intervalId);
      WorkerPool.global.intervalId = -1;
      WorkerPool.global.workersQueue.forEach((workersTypeData, typeName) => {
        const now = process.hrtime.bigint();
        while(1 <= workersTypeData.queue.length) {
          const workerMain = workersTypeData.queue.shift().workerMain;
          workerMain.kill(() => {
            WorkerPool.global.workersInstances.delete(workerMain.globalInstanceId);
          });
        }
      });
    }
  }
  
  getWorker(typeName, factoryMain, factoryThreadPath) {
    let workersTypeData = WorkerPool.global.workersQueue.get(typeName);
    if(!workersTypeData) {
      workersTypeData = {
        instanceId: -1,
        queue: []
      };
      WorkerPool.global.workersQueue.set(typeName, workersTypeData);
    }
    if(0 === workersTypeData.queue.length) {
      const globalInstanceId = ++WorkerPool.global.instancesWorkes;
      const workerMain = new factoryMain();
      WorkerPool.global.workersInstances.set(globalInstanceId, workerMain);
      ++workersTypeData.instanceId;
      workerMain._init(typeName, workersTypeData.instanceId, globalInstanceId, this, new WorkerCore(typeName, workersTypeData.instanceId, factoryThreadPath, this));
      return workerMain;
    }
    else {
      const workerData = workersTypeData.queue.shift();
      return workerData.workerMain;
    }
  }
  
  store(workerMain) {
    const workersTypeData = WorkerPool.global.workersQueue.get(workerMain.typeName);
    if(workersTypeData) {
      workersTypeData.queue.push({
        workerMain: workerMain,
        timestamp: process.hrtime.bigint()
      });
    }
    else {
      ddb.error(`Can not reuse Worker not defined, typeName: '${workerMain.typeName}'`);
    }
  }
}

WorkerPool.global = {
  workersQueue: new Map(),
  workersInstances: new Map(),
  instancesPool: 0,
  instancesWorkes: 0,
  intervalId: -1
};


WorkerPool.TIMEOUT = 256000;
WorkerPool.TIMEOUT_BIG = BigInt(1000000 * WorkerPool.TIMEOUT);

module.exports = WorkerPool;
