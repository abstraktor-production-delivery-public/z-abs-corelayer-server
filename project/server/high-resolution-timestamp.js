
'use strict';

require('z-abs-corelayer-cs/clientServer/debug-dashboard/log');


class HighResolutionTimestamp {
  static date = new Date();
  static hrtime = process.hrtime.bigint();
  static dateStamp = HighResolutionTimestamp.date.getTime(); // LEGACY
  static milliSeconds = HighResolutionTimestamp.date.getMilliseconds();
  static dateStampNew = BigInt(HighResolutionTimestamp.dateStamp - HighResolutionTimestamp.milliSeconds) * 1000000n;
  static hrtimeNew = HighResolutionTimestamp.hrtime - BigInt(HighResolutionTimestamp.milliSeconds) * 1000000n;
  
  static getHighResolutionDate(timestamp) {
    if(!timestamp) {
      timestamp = process.hrtime.bigint();
    }
    return HighResolutionTimestamp.dateStampNew + (timestamp - HighResolutionTimestamp.hrtimeNew);
  }
  
  static getMilliseconds(hrtime) {
    return Number(hrtime / 1000000n);
  }
  
  static workerInit(dateStamp, timeStamp) {
    HighResolutionTimestamp.dateStamp = dateStamp;
    HighResolutionTimestamp.hrtime = timeStamp;
    ddb.setTimeCreator(HighResolutionTimestamp.getHighResolutionDate);
  }
}


ddb.setTimeCreator(HighResolutionTimestamp.getHighResolutionDate);

module.exports = HighResolutionTimestamp;
