
'use strict';

const PluginFactory = require('./plugin-factory');


class PluginComponent {
  static STATE_NONE = 0;
  static STATE_INITIALIZING = 1;
  static STATE_RUNNING = 2;
  static STATE_EXITING = 3;
  static STATE_DONE = 4;
  
  constructor(client) {
    this.componentName = this.constructor.name;
    this.pluginService = null;
    this.state = PluginComponent.STATE_NONE;
    this.cb = null;
  }
  
  initData(pluginService) {
    this.pluginService = pluginService;
  }
  
  init(cb) {
    this.cb = cb;
    this.state = PluginComponent.STATE_INITIALIZING;
    try {
      this.onInit();
    }
    catch(err) {
      this._logError('initialize', err);
      cb(err);
    }
  }
  
  exit(cb) {
    this.cb = cb;
    this.state = PluginComponent.STATE_EXITING;
    try {
      this.onExit();
    }
    catch(err) {
      this._logError('exit', err);
      cb(err);
    }
  }
  
  done(err) {
    const cb = this.cb;
    this.cb = null;
    process.nextTick(() => {
      if(PluginComponent.STATE_INITIALIZING === this.state) {
        if(err) {
          this._logError('initialize', err);
        }
        else {
          this._logSuccess('initialized');
        }
        this.state = PluginComponent.STATE_RUNNING;
      }
      else if(PluginComponent.STATE_EXITING === this.state) {
        if(err) {
          this._logError('exit', err);
        }
        else {
          this._logSuccess('exited');
        }
        this.state = PluginComponent.STATE_DONE;
      }
      else {
        ddb.error('WRONG STATE:');
      }
      cb();
    });
  }
  
  getService() {
    return this.pluginService;
  }

  
  _logSuccess(phase) {
    ddb.writelnTime(ddb.green('Component: '), this.componentName, ddb.green(' ' + phase + '.'));
  }
  
  _logError(phase, err) {
    ddb.writelnTime(ddb.red('Component: ') + this.componentName, 'Could not', ddb.red(' ' + phase + '.'), err);
  }
}


module.exports = PluginComponent;
