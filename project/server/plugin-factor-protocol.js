
'use strict';

const PluginFactory = require('./plugin-factory');


class PluginFactorProtocol {
  constructor() {
    this.pluginProtocolParams = [];
    this.pluginProtocolFactory = new PluginFactory('protocol', this.pluginProtocolParams, 'clientServer');
    this.protocols = [];
  }

  load(protocols, done) {
    this.pluginProtocolFactory.setPaths(protocols);
    this.pluginProtocolFactory.load(() => {
      this.protocols = this.pluginProtocolFactory.createAll();
      done();
    });
  }

  register(channelInput, channelOutput) {
    this.protocols.forEach((protocol) => {
      if(channelInput) {
        protocol.plugin.registerDeserializer(channelInput);
      }
      if(channelOutput) {
        protocol.plugin.registerSerializer(channelOutput);
      }
    });
  }
}

module.exports = PluginFactorProtocol;
