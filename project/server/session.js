
'use strict';


class Session {
  static PROCESS_STATUS_NOT_RUNNING = 0;
  
  constructor(sessionId, connectionData) {
    this.sessionId = sessionId;
    this.connectionData = connectionData;
    this.process = undefined;
    this.processStatus = Session.PROCESS_STATUS_NOT_RUNNING; // DUBBLE CODE WITH - DataResponse
    this.requests = new Map();
    this.connectionDataSessionCache = connectionData.sessionCache;
    this.sessionData = new Map();
    this.dataPluginsInternal = [];
    this.dataPluginsExternal = [];
    this.routes = [];
  }
}


module.exports = Session;
