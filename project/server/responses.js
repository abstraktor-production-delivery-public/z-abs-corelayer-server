
'use strict';

const CoreProtocolConst = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/core-protocol-const');


class Responses {
  constructor(id) {
    this.msgId = CoreProtocolConst.RESPONSE;
    this.id = id;
    this.responses = [];
  }
  
  add(response) {
    this.responses.push(response);
  }
}


module.exports = Responses;
