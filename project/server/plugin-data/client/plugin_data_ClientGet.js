
'use strict';

const PluginBaseMulti = require('../../plugin-base-multi');


class ClientStore extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
  }
  
  onRequest() {
    return this.expectAsynchResponseSuccess('Hejsan hoppsan.');
  }
}


module.exports = PluginsStore.export(ClientStore);
