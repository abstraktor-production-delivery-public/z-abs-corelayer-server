
'use strict';

const ActorPath = require('../../path/actor-path');
const ActorPathProject = require('../../path/actor-path-project');
const ActorPathData = require('../../path/actor-path-data');
const PluginBaseMulti = require('../../plugin-base-multi');
const Project = require('z-abs-corelayer-cs/clientServer/project');
const Fs = require('fs');
const Path = require('path');


class DialogFileGet extends PluginBaseMulti {
  constructor() {
    super(PluginBaseMulti.GET);
    this.project = new Project();
    this.pathFilters = [''];
    this.existingFilters = [''];
    this.rootName = '';
    this.hasOrganization = !!Reflect.get(global, 'release-data@abstractor')?.organization;
  }
  
  onRequest(rootName, pathFilters, existingFilters, type, projectType, plugin, workspaceName) {
    if('workspace' === type) {
      this.getWorkspaceFolders(ActorPath.getActorParentPath(), (workspaces) => {
        this.responsePartSuccess(workspaces);
      });
    }
    else {
      this.rootName = rootName;
      const isExternalPlugin = this.hasOrganization && !!plugin;
      const external = this.isExternal(this.rootName);
      this.pathFilters = this._adjustFilters(pathFilters, projectType, external, isExternalPlugin);
      this.existingFilters = this._adjustFilters(existingFilters, projectType, external, isExternalPlugin);
      const rootPath = this.getRootPath(this.rootName, projectType, isExternalPlugin, workspaceName);
      const relativeName = this.rootName.substring(2);
      const node = this.project.addRootFolder(!external ? Path.parse(rootPath).base : relativeName, []);
      this.project.type = projectType;
      this.getFilesTree(rootPath, relativeName, type, node, external, workspaceName, (err) => {
        if(!err) {
          this.responsePartSuccess(this.project);
        }
        else {
          this.responsePartError(`Could not get Dialog File: '${rootPath}'`);
        }
      });
    }
  } 
  
  getWorkspaceFolders(dir, done) {
    Fs.readdir(dir, (err, files) => {
      if(err) {
        return done(err);
      }
      let pendings = files.length;
      if(0 == pendings) {
        done([]);
        return;
      }
      const workspaces = [];
      files.forEach((file) => {
        const newDir = Path.resolve(dir, file);
        Fs.stat(newDir, (err, stat) => {
          if(stat && stat.isDirectory()) {
            if(file.startsWith('actor') || file.startsWith('node-') || file.startsWith('persistant-node-')) {
              workspaces.push(file);
            }
          }
          if(0 === --pendings) {
            done(workspaces);
          }
        });
      });
    });
  }
  
  getFilesTree(dir, relativeName, type, node, external, workspaceName, done) {
    Fs.readdir(dir, (err, files) => {
      if(err) {
        return done(err);
      }
      let pendings = files.length;
      if(0 == pendings) {
        done();
        return;
      }
      files.forEach((file) => {
        const newDir = Path.resolve(dir, file);
        const relativeDir = Path.relative(Path.resolve(this.getRelativePath(this.rootName, workspaceName)), dir).replace(new RegExp('[\\\\]', 'g'), '/');
        const relativeStart = `${relativeDir}/${file}`;
        Fs.stat(newDir, (err, stat) => {
          if(stat && stat.isDirectory()) {
            let found = false;
            this.pathFilters.forEach((folder) => {
              if(folder === relativeDir) {
                const foundFolderInFilter = this.existingFilters.find((fileFromExistingFilter) => {
                  return fileFromExistingFilter === relativeStart;
                });
                if(!foundFolderInFilter && 'folder' === type) {
                  this.project.addFolderToNode(file, `./${relativeName}`, [], node);
                }
              }
              else if(folder.startsWith(relativeStart) && (folder === relativeStart || '/' === folder[relativeStart.length])) {
                found = true;
                const newNode = this.project.addFolderToNode(file, `./${relativeName}`, [], node);
                this.getFilesTree(newDir, `${relativeName}/${file}`, type, newNode, external, workspaceName, (err) => {
                  if(0 === --pendings) {
                    done();
                  }
                });
              }
            });
            if(!found) {
              if(0 === --pendings) {
                done();
              }
            }
          }
          else if(stat && stat.isFile()) {
            const foundFolder = this.pathFilters.find((folderFromPathFilter) => {
              return folderFromPathFilter === relativeDir;
            });
            if(foundFolder) {
              const foundFileInFilter = this.existingFilters.find((fileFromExistingFilter) => {
                return fileFromExistingFilter === relativeStart;
              });
              if(!foundFileInFilter && 'file' === type) {
                this.project.addFileToNode(file, `./${relativeName}`, file.substr(file.lastIndexOf('.') + 1, file.length), node);
              }
            }
            if(0 === --pendings) {
              done();
            }
          }
        });
      });
    });
  }
  
  isExternal(pathName) {
    switch(pathName) {
      case './project':
      case './Actors-global':
      case './Actors-local':
      case './Stacks-global':
      case './Stacks-local':
        return false;
      default:
        return true;
    }
  }
  
  _adjustFilters(filters, projectType, external, isExternalPlugin) {
    const adjustedFilters = [];
    filters.forEach((filter) => {
        adjustedFilters.push(ActorPathProject.getRelativePath(filter, projectType, isExternalPlugin).replace(new RegExp('[\\\\]', 'g'), '/'));
    });
    return adjustedFilters;
  }
  
  getRootPath(pathName, projectType, isExternalPlugin, workspaceName) {
    switch(pathName) {
      case './project':
        return ActorPathProject.getCodeProjectFolder(null, null, workspaceName);
      case './Actors-global':
        return ActorPathData.getActorsGlobalFolder();
      case './Actors-local':
        return ActorPathData.getActorsLocalFolder();
      case './Stacks-global':
        return ActorPathData.getStacksGlobalFolder();
      case './Stacks-local':
        return ActorPathData.getStacksLocalFolder();
      default:
        return `${ActorPathProject.getRawPath(projectType, isExternalPlugin)}${pathName.substring(2)}${Path.sep}project`;
    }
  }
  
  getRelativePath(pathName, workspaceName) {
    switch(pathName) {
      case './project':
        return Path.normalize(`${ActorPathProject.getCodeProjectFolder(null, null, workspaceName)}/..`);
      case './Actors-global':
        return  Path.normalize(`${ActorPathData.getActorsGlobalFolder()}/..`);
      case './Actors-local':
        return Path.normalize(`${ActorPathData.getActorsLocalFolder()}/..`);
      case './Stacks-global':
        return Path.normalize(`${ActorPathData.getStacksGlobalFolder()}/..`);
      case './Stacks-local':
        return Path.normalize(`${ActorPathData.getStacksLocalFolder()}/..`);
      default:
        return '';
    }
  }
}


module.exports = DialogFileGet;
