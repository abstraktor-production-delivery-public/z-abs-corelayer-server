
'use strict';

const PluginBase = require('../../plugin-base');


class PlatformPing extends PluginBase {
  constructor() {
    super(PluginBase.GET);
  }
  
  onRequest() {
    return this.responsePartSuccess('ping');
  }
}


module.exports = PlatformPing;
 