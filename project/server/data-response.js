
'use strict';

const ResponseQueue = require('./response-queue');
const PluginFactory = require('./plugin-factory');
const Session = require('./session');
const SessionCache = require('./session-cache');
const ActorPathDist = require('./path/actor-path-dist');
const Logger = require('./log/logger');
const Decoder = require('./communication/core-protocol/decoder');
const Encoder = require('./communication/core-protocol/encoder');
const MessagePersistentInitResponse = require('./communication/messages/messages-s-to-c/message-persistent-init-response');
const MessagePersistentPublish = require('./communication/messages/messages-s-to-c/message-persistent-publish');
const ConnectionData = require('./node/connection-data');
const ConnectionDataIp = require('./node/connection-data-ip');
const IpSubscription = require('z-abs-corelayer-server/server/node/ip-subscription');
const ServiceInitRequest = require('./communication/messages/messages-s-to-s/service-init-request');
const ServiceInitResponse = require('./communication/messages/messages-s-to-s/service-init-response');
const ServiceOfferOn = require('./communication/messages/messages-s-to-s/service-offer-on');
const ServiceOfferOff = require('./communication/messages/messages-s-to-s/service-offer-off');
const Const = require('./node/const');
const ServiceExport = require('./service/service-export');
const ServiceManager = require('./service/service-manager');const Envelope = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/envelope');
const CoreProtocolConst = require('z-abs-corelayer-cs/clientServer/communication/core-protocol/core-protocol-const');
const ChildProcess = require('child_process');
const Path = require('path');


class DataResponse {
  static connectionId = 0;
  static basePath = './dist/Layers/z-abs-corelayer-server';
  static PROCESS_STATUS_NOT_RUNNING = 0;
  static PROCESS_STATUS_RUNNING = 1;
  static PROCESS_STATUS_CLOSING = 2;
  static _ =  {serverDatas: null};
    
  constructor(nodeName, nodeId, nodeType, node, externalServices) {
    this.nodeName = nodeName;
    this.nodeId = nodeId;
    this.nodeType = nodeType;
    this.node = node;
    this.externalServices = externalServices;
    this.ipSubscription = new IpSubscription();
    this.sessionCache = new SessionCache();
    this.pluginDataParams = [this];
    this.pluginDataFactory = new PluginFactory('data', this.pluginDataParams);
    this.serviceManager = new ServiceManager(this);
    this.persistenServiceSubscriptions = new Map();
    this.webServerConnectionDatas = new Map();
    this.persistentWsMessageQueue = new Map();
    this.workers = new Map();
  }
  
  init(pluginDatas, pluginServices, pluginComponents, serverDatas, workers, done) {
    this.workers = workers;
    DataResponse._.serverDatas = serverDatas;
    this.pluginDataFactory.setPaths(pluginDatas);
    let pendings = 2;
    this.pluginDataFactory.load((size) => {
      ddb.writelnTime(ddb.yellowBright('Loaded:   '), ddb.yellow(size), ' ', ddb.greenBright('data plugins from:'.padEnd(23 - size.toString().length)), pluginDatas.join(', '));
      if(0 === --pendings) {
        done();
      }
    }, ActorPathDist.getActorDistServerPluginDataPath());
    this.serviceManager.init(pluginComponents, pluginServices, () => {
      if(0 === --pendings) {
        done();
      }
    });
  }
  
  getNode() {
    return this.node;
  }
  
  startServices(cb) {
    this.serviceManager.startServices(cb);
  }
  
  stopServices(hasException, cb) {
    this.serviceManager.stopServices(hasException, cb);
  }
  
  getService(name) {
    return this.serviceManager.getService(name);
  }
  
  onConnectionConnected(connectionType, cbMessage) {
    if(Const.CONNECTION_TYPE_SERVER_IP === connectionType || Const.CONNECTION_TYPE_CLIENT_IP === connectionType) {
      return new ConnectionDataIp(`NODE-${DataResponse.connectionId}-CLIENT-IN`, `NODE-${DataResponse.connectionId}-CLIENT-OUT`, cbMessage);
    }
    else if(Const.CONNECTION_TYPE_SERVER_WEB === connectionType) {
      const connectionData = new ConnectionData(`NODE-${DataResponse.connectionId}-CLIENT-IN`, `NODE-${DataResponse.connectionId}-CLIENT-OUT`, cbMessage);
      this.webServerConnectionDatas.set(connectionData.id, {
        connectionData,
        persistenServiceSubscriptions: []
      });
      return connectionData;
    }
    else {
      ddb.error('Unknown connectionType:', connectionType);
    }
  }
  
  onConnectionClose(connectionType, connectionData) {
    if(Const.CONNECTION_TYPE_SERVER_WEB === connectionType) {
      const webServerConnectionData = this.webServerConnectionDatas.get(connectionData.id);
      const persistenServiceSubscriptions = webServerConnectionData.persistenServiceSubscriptions;
      persistenServiceSubscriptions.forEach((persistenServiceSubscription) => {
        this.persistenServiceSubscriptions.delete(persistenServiceSubscription);
      });
      this.webServerConnectionDatas.delete(connectionData.id);
    }
    if(Const.CONNECTION_TYPE_SERVER_IP === connectionType || Const.CONNECTION_TYPE_CLIENT_IP === connectionType) {
      this.externalServices.deleteNode(connectionData, connectionData.nodeType);
      const offerData = this.externalServices.getOfferData(connectionData.nodeName, connectionData.nodeSymbol);
      this.sendOfferOff(offerData, connectionData);
      this.ipSubscription.event(Const.EVENT_TYPE_CONNECTION, connectionData.name, Const.IP_EVENT_DISCONNECTED, connectionData.connectionAddress);
    }
    process.nextTick((connectionDataSessionCache) => {
      connectionDataSessionCache.forEach((connectionDataSession) => {
        let pendings = connectionDataSession.dataPluginsInternal.length;
        connectionDataSession.dataPluginsInternal.forEach((dataPlugin) => {
          dataPlugin.handleClose((err) => {
            process.nextTick((pendings, connectionDataSession) => {
              if(0 === --pendings) {
                this.clearSession(connectionDataSession.sessionId, (code) => {
                  LOG_ENGINE(Logger.ENGINE.MSG, `CLOSE sessionId: ${connectionDataSession.sessionId} code = ${code}`);
                });
              }
            }, pendings, connectionDataSession);
          });
        });
      });
    }, connectionData.sessionCache);
  }
  
  onMessage(data, connectionData) {
    connectionData.channelInput.onNewMessage(data, true, (coreMessage) => {
      if(!coreMessage) {
        return;
      }
      else if(!coreMessage.msg) {
        return;
      }
      if(CoreProtocolConst.APP <= coreMessage.msgId) {
        const session = this.sessionCache.getSession(coreMessage.msg.sessionId);
        if(session) {
          session.dataPluginsInternal.forEach((dataPlugin) => {
            dataPlugin.handleMessageFromClient(coreMessage.msg);
          });
        }
      }
      else if(CoreProtocolConst.REQUEST === coreMessage.msgId) {
        let routes = null;
        if(coreMessage.isServiceAction) {
          coreMessage.envelope.createRouteEnvelope();
          routes = coreMessage.envelope.routes;
        }
        this.handleRequests(coreMessage.msg, null, routes, (response, dataBuffers, debug, token) => {
          if(undefined === debug) {
            ddb.error('onMessage-1, undefined === debug');
          }
          if(coreMessage.isServiceAction) {
            response.isServiceAction = true;
          }
          connectionData.channelOutput.send(response, dataBuffers, debug, coreMessage.envelope);
        }, (msg, dataBuffers, debug, sessionId) => {
          if(undefined === debug) {
            ddb.error('onMessage-2, undefined === debug');
          }
          const session = this.sessionCache.getSession(sessionId);
          if(session) {
            let proxy = false;
            session.dataPluginsInternal.forEach((dataPlugin) => {
              if(dataPlugin.handleMessageToClient(msg, dataBuffers, (msgHandled, dataBuffersHandled) => {
                connectionData.channelOutput.send(msgHandled, dataBuffersHandled, debug, coreMessage.envelope);
              })) {
                proxy = true;
              }
            });
            if(!proxy) {
              connectionData.channelOutput.send(msg, dataBuffers, debug, coreMessage.envelope);
            }
          }
        }, connectionData);
      }
      else if(CoreProtocolConst.RESPONSE === coreMessage.msgId) {
        if(!coreMessage.isServiceAction) {
          ddb.error('DROPPED ServiceAction response, got a response that is not an ServiceAction: ', coreMessage);
        }
        else {
          const envelope = coreMessage.envelope;
          const requestData = this.serviceManager.popRequest(envelope.serviceName, envelope.shiftRoute());
          if(requestData) {
            requestData.pluginService.receiveResponseFromService(requestData.requestId, requestData.sessionId, coreMessage.envelope.serviceName, coreMessage.msg.responses, requestData.cb);
          }
          else {
            ddb.error('DROPPED ServiceAction response, could not find request: ', coreMessage);
          }
        }
      }
      else if(CoreProtocolConst.SERVICE_INIT_REQUEST === coreMessage.msgId) {
        connectionData.setServiceInit(coreMessage.msg.nodeName, coreMessage.msg.nodeId, coreMessage.msg.nodeType);
        this.externalServices.addNode(connectionData);
        const offerData = this.externalServices.getOfferData(connectionData.nodeName, connectionData.nodeSymbol);
        if(this.sendOfferOn(offerData, connectionData, false)) {
          const nodeData = this.externalServices.getNodeData(connectionData);
          const serviceExport = this.serviceManager.getServiceExport(this.nodeType, connectionData.nodeType);
          connectionData.channelOutput.send(new ServiceInitResponse(this.nodeName, this.nodeId, this.nodeType, serviceExport), null, false, null);
          nodeData.offered = true;
          nodeData.serviceExport = serviceExport;
        }
        else {
          connectionData.channelOutput.send(new ServiceInitResponse(this.nodeName, this.nodeId, this.nodeType, ServiceExport.EMPTY), null, false, null);
        }
        this.ipSubscription.event(Const.EVENT_TYPE_CONNECTION, connectionData.name, Const.IP_EVENT_CONNECTED, connectionData);
      }
      else if(CoreProtocolConst.SERVICE_INIT_RESPONSE === coreMessage.msgId) {
        connectionData.setServiceInit(coreMessage.msg.nodeName, coreMessage.msg.nodeId, coreMessage.msg.nodeType);
        this.externalServices.offerOn(connectionData, coreMessage.msg.serviceExport);
        this.externalServices.addNode(connectionData);
        const offerData = this.externalServices.getOfferData(connectionData.nodeName, connectionData.nodeSymbol);
        this.sendOfferOn(offerData, connectionData, true);
        this.ipSubscription.event(Const.EVENT_TYPE_CONNECTION, connectionData.name, Const.IP_EVENT_CONNECTED, connectionData);
        
      }
      else if(CoreProtocolConst.SERVICE_OFFER_ON === coreMessage.msgId) {
        this.externalServices.offerOn(connectionData, coreMessage.msg.serviceExport);
        const offerData = this.externalServices.getOfferData(connectionData.nodeName, connectionData.nodeSymbol);
        this.sendOfferOn(offerData, connectionData, true);
      }
      else if(CoreProtocolConst.SERVICE_OFFER_OFF === coreMessage.msgId) {
        this.externalServices.offerOff(connectionData, coreMessage.msg.serviceExport);
        const offerData = this.externalServices.getOfferData(connectionData.nodeName, connectionData.nodeSymbol);
        this.sendOfferOff(offerData, connectionData);
      }
      else if(CoreProtocolConst.PERSISTENT_INIT_REQUEST === coreMessage.msgId) {
        const name = coreMessage.msg.name;
        const subscribeServices = coreMessage.msg.subscribeServices;
        connectionData.persistantName = name;
        const webServerConnectionData = this.webServerConnectionDatas.get(connectionData.id);
        subscribeServices.forEach((subscribeService) => {
          webServerConnectionData.persistenServiceSubscriptions.push(subscribeService);
          this.serviceManager.subscripeOnLocalService(subscribeService);
          if(!this.persistenServiceSubscriptions.has(subscribeService)) {
            this.persistenServiceSubscriptions.set(subscribeService, [{
              name: name,
              connectionData: connectionData
            }]);
          }
          else {
            this.persistenServiceSubscriptions.get(subscribeService).push({
              name: name,
              connectionData: connectionData
            });
          }
        });
        webServerConnectionData.
        connectionData.channelOutput.send(new MessagePersistentInitResponse(), null, false, null);
      }
      else {
        ddb.error('DROPPED: ', coreMessage);
      }
    });
  }
  
  sendServiceInitRequest(connectionData) {
    connectionData.channelOutput.send(new ServiceInitRequest(this.nodeName, this.nodeId, this.nodeType), null, false, null);
  }
              
  sendOfferOn(offerData, connectionData, shallSendToOrigin) {
    if(offerData.offer) {
      if(!offerData.isDependent) {
        if(shallSendToOrigin) {
          const nodeData = this.externalServices.getNodeData(connectionData);
          if(!nodeData.offered) {
            const serviceExport = this.serviceManager.getServiceExport(this.nodeType, connectionData.nodeType);
            serviceExport.printOfferOn(offerData, connectionData.nodeName, connectionData.nodeId);
            connectionData.channelOutput.send(new ServiceOfferOn(serviceExport), null, false, null);
            nodeData.offered = true;
            nodeData.serviceExport = serviceExport;
          }
        }
      }
      else {
        this.externalServices.nodes.forEach((node) => {
          if(shallSendToOrigin || connectionData.nodeSymbol !== node.nodeSymbol) {
            const nodeData = this.externalServices.getNodeData(node);
            if(!nodeData.offered && offerData.isDependent) {
              const serviceExport = this.serviceManager.getServiceExport(this.nodeType, node.nodeType);
              serviceExport.printOfferOn(offerData, node.nodeName, node.nodeId);
              node.connectionData.channelOutput.send(new ServiceOfferOn(serviceExport), null, false, null);
              nodeData.offered = true;
              nodeData.serviceExport = serviceExport;
            }
          }
        });
      }
      return true;
    }
    else {
      return false;
    }
  }
  
  sendOfferOff(offerData, connectionData) {
    if(!offerData.offer) {
      if(!offerData.isDependent) {
        const nodeData = this.externalServices.getNodeData(connectionData);
        if(nodeData.offered) {
          nodeData.serviceExport.printOfferOff(offerData, connectionData.nodeName, connectionData.nodeId);
          connectionData.channelOutput.send(new ServiceOfferOff(nodeData.serviceExport), null, false, null);
          nodeData.offered = false;
          nodeData.serviceExport = null;
        }
      }
      else {
        this.externalServices.nodes.forEach((node) => {
          const nodeData = this.externalServices.getNodeData(node);
          if(nodeData.offered && offerData.isDependent) {
            nodeData.serviceExport.printOfferOff(offerData, node.nodeName, node.nodeId);
            node.connectionData.channelOutput.send(new ServiceOfferOff(nodeData.serviceExport), null, false, null);
            nodeData.offered = false;
            nodeData.serviceExport = null;
          }
        });
      }
    }
  }
  
  sendMessageToPersistentStore(message, storeName) {
    /*const connectionData = this.persistentWsConnections.get(storeName);
    if(connectionData) {
      cbMessage(message);
      return true;
    }
    else {
      if(this.persistentWsMessageQueue.has(storeName)) {
        const queue = this.persistentWsMessageQueue.get(storeName);
        queue.push(message);
      }
      else {
        this.persistentWsMessageQueue.set(storeName, [message]);
      }
      return false;
    }*/
  }
  
  forkWorkerProcess(name, sessionId, pluginPaths, debug, execArgv, done) {
    const workerData = this.workers.get(name);
    const protocols = workerData?.worker.protocols?.server ? workerData.worker.protocols.server : '[]';
    const protocolsJson = JSON.stringify(protocols);
    const pluginPathsJson = Array.isArray(pluginPaths) ? (JSON.stringify(pluginPaths)) : (pluginPaths ? (JSON.stringify([pluginPaths])) : '[]');
    const workerProcess = ChildProcess.fork(`${name}.js`, [Reflect.get(global, 'dynamicConfigStringified@abstractor'), ActorPathDist.getActorDistServerPluginDataPath(), pluginPathsJson, protocolsJson, ...execArgv], {execArgv: [], silent: false, stdio: ['ignore', 'inherit', 'ignore', 'ipc']});
    const session = this.sessionCache.getSession(sessionId);
    session.workerProcess = {
      process: workerProcess,
      connectionData: new ConnectionData(`NODE-${DataResponse.connectionId}-WORKER-IN`, `NODE-${DataResponse.connectionId}-WORKER-OUT`, workerProcess.send.bind(workerProcess))
    };
    pluginPaths.forEach((pluginPath) => {
      session.dataPluginsExternal.push(Path.parse(pluginPath).base);
    });
    const channelInput = session.workerProcess.connectionData.channelInput;
    const channelOutput = session.workerProcess.connectionData.channelOutput;
    if(workerData) {
      workerData.pluginFactoryProtocol.register(channelInput, channelOutput);
    }
    workerProcess.on('message', (msg) => {
      const data = Buffer.from(msg.data);
      channelInput.onNewMessage(data, true, (coreMessage) => {
      if(!coreMessage) {
        return;
      } 
      else if(!coreMessage.msg) {
        ddb.error('NODE-WORKER-IN dropped message, msgid: ', coreMessage.msgId);
        return;
      }
      if(CoreProtocolConst.APP <= coreMessage.msgId) {
        const recordRoute = coreMessage.envelope.recordRoutes.pop();
        const cbs = session.requests.get(recordRoute);
        let proxy = false;
        session.dataPluginsInternal.forEach((dataPlugin) => {
          proxy |= dataPlugin.handleMessageFromWorker(coreMessage.msg, coreMessage.dataBuffers, debug, cbs.cbMessage);
        });
        if(!proxy) {
          cbs.cbMessage(coreMessage.msg, coreMessage.dataBuffers, debug);
        }
      }
      else if(CoreProtocolConst.RESPONSE === coreMessage.msgId) {
        const cbs = session.requests.get(coreMessage.msg.id);
        cbs.cb(coreMessage.msg);
        cbs.cb = null;
      }
      else if(CoreProtocolConst.WORKER_INIT_REQUEST === coreMessage.msgId) {
        session.processStatus = DataResponse.PROCESS_STATUS_RUNNING;
        done(workerProcess.pid, Number.parseInt(coreMessage.msg.port));
      }
      });
    });
    workerProcess.on('exit', (code) => {
      session.process = undefined;
      session.processStatus = DataResponse.PROCESS_STATUS_NOT_RUNNING;
    });
  }
  
  killWorkerProcess(sessionId, done) {
    const session = this.sessionCache.getSession(sessionId);
    if(session && DataResponse.PROCESS_STATUS_RUNNING === session.processStatus) {  
      session.workerProcess.process.on('exit', (code) => {
        session.process = undefined;
        session.processStatus = DataResponse.PROCESS_STATUS_NOT_RUNNING;
        done(code);
      });
      session.processStatus = DataResponse.PROCESS_STATUS_CLOSING;
      session.workerProcess.process.kill();
    }
    else {
      done(-1);
    }
  }
  
  clearSession(sessionId, done) {
    const session = this.sessionCache.getSession(sessionId);
    if(session) {
      this.killWorkerProcess(sessionId, (code) => {
        session.connectionDataSessionCache.delete(sessionId);
        this.sessionCache.deleteSession(sessionId);
        done && done(code);
      });
    }
  }
  
  sendRequestToWorkerProcess(session, id, request, cb, cbMessage, cbProxyMessage) {
    session.requests.set(id, {
      cb: cb,
      cbMessage: cbMessage,
      cbProxyMessage: cbProxyMessage
    });
    session.workerProcess.connectionData.channelOutput.send(request, null, true, new Envelope(null, id));
  }
  
  sendMessageToWorkerProcess(session, msg) {
    session.workerProcess.connectionData.channelOutput.send(msg, null, true, null);
  }
  
  _createDataPlugin(name, done) {
    let dataPlugin = this.pluginDataFactory.create(name);
    if(dataPlugin) {
      process.nextTick(done, dataPlugin);
    }
    else {
      this.pluginDataFactory.load(() => {
        dataPlugin = this.pluginDataFactory.create(name);
        done(dataPlugin);
      }, ActorPathDist.getActorDistServerPluginDataPath());
    }
  }
  
  _handleRequestLocal(request, dataPlugin, queue, cbMessage) {
    try {
      dataPlugin.handleRequest(request, (response) => {
        if(dataPlugin.tokenOut) {
          queue.setToken(dataPlugin.tokenOut);
        }
        queue.done(response);
      }, cbMessage);
    }
    catch(err) {
      LOG_ERROR(Logger.ERROR.CATCH, `Data Plugin exception: '${request.name}'. Message: ${err}`, err);
      queue.done({name: request.name, index: request.index, result: { code: 'error', msg: 'Data Plugin exception: ' + request.name }});
    }
  }
  
  publish(dataPluginName, subscriptionName, cb, ...params) {
    this._createDataPlugin(dataPluginName, (dataPlugin) => {
      try {
        dataPlugin.handleRequest({
          index: 0,
          sessionId: null,
          params: params
        }, (response) => {
          const subscriptions = this.persistenServiceSubscriptions.get(subscriptionName);
          if(subscriptions) {
            subscriptions.forEach((subscription) => {
              subscription.connectionData.channelOutput.send(new MessagePersistentPublish(subscription.name, response), null, false, null);
            });
          }    
        }, null);
      }
      catch(err) {
        cb(err);
      }
    });
  }
  
  handleRequests(rawRequest, token, routes, cbRequest, cbMessage, connectionData) {
    const queue = new ResponseQueue((responses, cb, token) => {
      cb(responses, token);
    }, (result, token) => {
      cbRequest(result, null, false, token);
    }, rawRequest.id);
    rawRequest.requests.forEach((request) => {
      queue.add();
      this._createDataPlugin(request.name, (dataPlugin) => {
        if(!dataPlugin) {
          LOG_ERROR(Logger.ERROR.ERR, `Unknown Data Plugin: ${request.name}.`, undefined);
          queue.done({name: request.name, index: request.index, result: { code: 'error', msg: 'Unknown Data Plugin: ' + request.name }});
          return;
        }
        else {
          dataPlugin.setTokenIn(token);
          if(!request.sessionId) {
            if(0 !== dataPlugin.authorization.length) {
              let authorization = null;
              const configService = this.getService('config');
              if(configService) {
                const configAuthorization = configService.getConfig('authorization', 'server');
                ddb.writelnTime('Config.Authorization', configAuthorization);
                if(configAuthorization) {
                  const userService = this.getService('user');
                  if(userService) {
                    authorization = userService.getAuthorization(token);
                  }
                }
              }
              if(!authorization || -1 === dataPlugin.authorization.indexOf(authorization)) {
                queue.done({name: request.name, index: request.index, result: { code: 'error', msg: 'Not Authorized: ' + request.name }});
                return;
              }
            }
            this._handleRequestLocal(request, dataPlugin, queue, (msg, dataBuffers, debug) => {
              cbMessage(msg, dataBuffers, debug, request.sessionId);
            });
          }
          else {
            let session = this.sessionCache.getSession(request.sessionId);
            if(!session)
            {
              session = this.sessionCache.createSession(request.sessionId, connectionData);
              if(routes) {
                session.routes = routes;
              }
              if(connectionData.sessionCache) {
                connectionData.sessionCache.set(request.sessionId, session);
              }
            }
            const externalFound = session.dataPluginsExternal.find((dataPlugin) => {
              return dataPlugin.dataName;
            });
            if(externalFound) {
              this.sendRequestToWorkerProcess(session, dataPlugin.instanceId, request, (response) => {
                queue.done(response);
              }, (msg, dataBuffers, debug) => {
                cbMessage(msg, dataBuffers, debug, request.sessionId);
              });
            }
            else {
              session.dataPluginsInternal.push(dataPlugin);
              this._handleRequestLocal(request, dataPlugin, queue, (msg, dataBuffers, debug) => {
                cbMessage(msg, dataBuffers, debug, request.sessionId);
              });
            }
          }
        }
      });
    });
  }
  
  _handleRest(method, url, contentType, body, token, cbResult) {
    const restApiService = this.getService('rest-api');
    if(restApiService) {
      restApiService.request(method, url, contentType, body, token, cbResult);
    }
    else {
      process.nextTick(() => {
        cbResult(501, 'Not Implemented', null, null);
      });
    }
  }
  
  _logParameters(parameters) {
    let logParameters = parameters.map((parameter) => {
      if(typeof parameter === 'string') {
        return `'${parameter.replace(/\r/g, '\\r').replace(/\n/g, '\\n')}'`;
      }
      else {
        return `${parameter}`;
      }
    });
    return logParameters.join(', ');
  }
}


module.exports = DataResponse;
