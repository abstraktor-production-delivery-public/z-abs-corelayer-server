
'use strict';

const PluginBase = require('./plugin-base');
const ResponseQueue = require('./response-queue');
const Logger = require('./log/logger');
const Os = require('os');
const Sudo = 'win32' === Os.platform() ? require('sudo-prompt') : require('sudo-js');
const Fs = require('fs');
const Path = require('path');
const ChildProcess = require('child_process');


class PluginBaseMulti extends PluginBase {
  constructor(restType, ...authorization) {
    super(restType, ...authorization);
    this.responseQueue = null;
    this.index = 0;
    this.responseCbs = [];
  }
  
  asynchReadTextFile(file, done) {
    this.expectAsynchResponseTemp();
    this.readLock(file, (unlock) => {
      Fs.readFile(file, 'utf8', (err, data) => {
        unlock();
        this._safeCallback(done, err, data);
        this.unExpectAsynchResponseTemp();
      });
    });
  }
  
  asynchReadTextFileResponse(file, done) {
    const index = this.expectAsynchResponse();
    this.asynchReadTextFile(file, (err, text) => {
      this._safeCallback(done, err, text);
      if(!err) {
        this.asynchResponseSuccess(index, text);
      }
      else {
        this.asynchResponseError(`Could not read file: \'${file}\'. error: ${err}`, index);
      }
    });
  }
  
  asynchReadFilePromise(file) {
    return new Promise((resolve, reject) => {
      this.readLock(file, (unlock) => {
        Fs.readFile(file, (err, data) => {
          unlock();
          this._safeJsonParseCallback((err, data) => {
            if(!err) {
              resolve(data);
            }
            else {
              reject(err);
            }
          }, err, file, data);
        });
      });
    });
  }
  
  asynchReadFile(file, done) {
    this.expectAsynchResponseTemp();
    this.readLock(file, (unlock) => {
      Fs.readFile(file, (err, data) => {
        unlock();
        if(undefined !== done) {
          this._safeJsonParseCallback(done, err, file, data);
        }
        this.unExpectAsynchResponseTemp();
      });
    });
  }
  
  asynchReadFileResponse(file, done, outerIndex = -1) {
    const index = -1 === outerIndex ? this.expectAsynchResponse() : outerIndex;
    this.asynchReadFile(file, (err, object) => {
      this._safeCallback(done, err, object);
      if(!err) {
        this.asynchResponseSuccess(index, object);
      }
      else {
        this.asynchResponseError(`Could not read file: \'${file}\'. error: ${err}`, index);
      }
    });
  }
  
  asynchWriteTextFile(file, data, done) {
    this.expectAsynchResponseTemp();
    this.writeLock(file, (unlock) => {
      Fs.writeFile(file, data, (err) => {
        unlock();
        this._safeCallback(done, err, data);
        this.unExpectAsynchResponseTemp();
      });
    });
  }
  
  asynchWriteTextFileResponse(file, data, returnValue, done) {
    const index = this.expectAsynchResponse(); // Should only be used if there i s a return value.
    this.asynchWriteTextFile(file, data, (err) => {
      this._safeCallback(done, err, data);
      if(!err) {
        this.asynchResponseSuccess(index, returnValue, 'asynchWriteTextFileResponse');
      }
      else {
        this.asynchResponseError(`Could not save the file: '${file}'.`, index);
      }
    });
  }
  
  asynchWriteFile(file, data, done) {
    this.expectAsynchResponseTemp();
    this.writeLock(file, (unlock) => {
      Fs.writeFile(file, JSON.stringify(data, null, 2), (err) => {
        unlock();
        this._safeCallback(done, err, data);
        this.unExpectAsynchResponseTemp();
      });
    });
  }
  
  asynchWriteFileResponse(file, data, returnValue, done) {
    let index = 0;
    if(returnValue) {
      index = this.expectAsynchResponse(); // Should only be used if there is a return value.
    }
    this.asynchWriteFile(file, data, (err) => {
      this._safeCallback(done, err, data);
      if(!err) {
        this.asynchResponseSuccess(index, returnValue ? data : undefined);
      }
      else {
        this.asynchResponseError(`Could not save the file: '${file}'.`, index);
      }
    });
  }
  
  _safeCallback(done, err, data) {
    try {
      done && done(err, data);
    }
    catch(e) {
      LOG_ERROR(Logger.ERROR.CATCH, 'Exception in safe callback: ', e, e.stack);
    }
  }
  
  _safeJsonParse(data, onError) {
    try {
      return JSON.parse(data);
    }
    catch(e) {
      LOG_ERROR(Logger.ERROR.CATCH, 'Exception in safe JSON parse: ', e, e.stack);
      onError(e);
    }
  }
  
  _safeJsonParseCallback(done, err, file, data) {
    if(!err) {
      try {
        this._safeCallback(done, err, JSON.parse(data));
      }
      catch(e) {
        const text = `Exception in safe JSON parse callback, file: '${file}'.`;
        LOG_ERROR(Logger.ERROR.CATCH, text, e, e.stack);
        this._safeCallback(done, e);
      }
    }
    else {
      this._safeCallback(done, err);
    }
  }
  
  _asynchMkdir(file, done) {
    this.expectAsynchResponseTemp();
    Fs.mkdir(file, (err) => {
      this._safeCallback(done, err);
      this.unExpectAsynchResponseTemp();
    });
  }
  
  asynchMkdir(file, done, recursive = false) {
    this.expectAsynchResponseTemp();
    this.writeLock(file, (unlock) => {
      this._asynchMkdirImpl(file, (err) => {
        unlock();
        done(err);
        this.unExpectAsynchResponseTemp();
      }, recursive);
    });
  }
  
  // DONE
  _asynchMkdirImpl(path, done, recursive) {
    this.expectAsynchResponseTemp();
    if(recursive) {
      Fs.access(path, Fs.F_OK, (err) => {
        if(!err) {
          done();
          this.unExpectAsynchResponseTemp();
        }
        else if('ENOENT' === err.code) {
          const parentPath = path.substring(0, path.lastIndexOf(Path.sep));
          this._asynchMkdirImpl(parentPath, (err) => {
            this._asynchMkdir(path, (err) => {
              done(err);
              this.unExpectAsynchResponseTemp();
            });
          }, true);
        }
        else {
          done(err);
          this.unExpectAsynchResponseTemp();
        }
      });
    }
    else {
      this._asynchMkdir(path, (err) => {
        done(err);
        this.unExpectAsynchResponseTemp();
      });
    }
  }
  
  // DONE
  asynchMkdirResponse(path, done, recursive = false) {
    const index = this.expectAsynchResponse();
    this.asynchMkdir(path, (err) => {
      if(undefined !== done) {
        done(err);
      }
      if(!err) {
        this.asynchResponseSuccess(index);
      }
      else {
        this.asynchResponseError(`Could not create directory: '${path}'.`, index);
      }
    }, recursive);
  }
  
  asynchRmdirResponse(path, recursive, force) {
    const index = this.expectAsynchResponse();
    this.writeLock(path, (unlock) => {
      Fs.rm(path, {recursive: !!recursive, force: !!force}, (err) => {
        unlock();
        if(!err) {
          this.asynchResponseSuccess(index);
        }
        else {
          this.asynchResponseError(`Could not remove directory: '${path}'.`, index);
        }
      });
    });
  }
  
  asynchRmFileResponse(file) {
    const index = this.expectAsynchResponse();
    this.writeLock(file, (unlock) => {
      Fs.unlink(file, (err) => {
        unlock();
        if(!err) {
          this.asynchResponseSuccess(index);
        }
        else {
          this.asynchResponseError(`Could not remove file: '${file}'.`, index);
        }
      });
    });
  }
  
  asynchRename(oldPath, newPath, cb) {
    this.expectAsynchResponseTemp();
    this.writeLock(oldPath, (unlock) => {
      Fs.rename(oldPath, newPath, (err) => {
        unlock();
        if(cb) {
          this._safeCallback(cb, err);
        }
        this.unExpectAsynchResponseTemp();
      });
    });
  }
  
  asynchRenameResponse(oldPath, newPath, cb) {
    const index = this.expectAsynchResponse();
    this.asynchRename(oldPath, newPath, (err) => {
      if(cb) {
        this._safeCallback(cb, err);
      }
      if(!err) {
        this.asynchResponseSuccess(index);
      }
      else {
        this.asynchResponseError(`Could not rename oldPath: '${oldPath}' to newPath '${newPath}'.`, index);
      }
    });
  }
  
  asynchChildProcess(command, done) {
    this.expectAsynchResponseTemp();
    ChildProcess.exec(command, (err, stdout) => {
      this._safeCallback(done, err, stdout);
      this.unExpectAsynchResponseTemp();
    });
  }
  
  asynchChildProcessResponse(command) {
    const index = this.expectAsynchResponse();
    this.asynchChildProcess(command, (err, stdout, stderr) => {
      if(!err) {
        this.asynchResponseSuccess(index, stdout);
      }
      else {
        this.asynchResponseError(stdout, index);
      }
    }); 
  }
  
  asynchSudoProcess(command, password, done) {
    this.expectAsynchResponseTemp(); 
    if('win32' === Os.platform()) {
      Sudo.exec(command, {
        name: 'ActorJs'
      }, (err, stdout, stderr) => {
        this._safeCallback(done, err, {
          stdout: stdout,
          stderr: stderr
        });
        this.unExpectAsynchResponseTemp();
      });
    }
    else {
      Sudo.setPassword(password);
      const options = {check: false, withResult: true};
      Sudo.exec(command.split(' '), options, (err, pid, result) => {
        this._safeCallback(done, err, {
          stdout: result.stdout,
          stderr: result.stderr
        });
        this.unExpectAsynchResponseTemp();
      });
    }
  }
  
  expectAsynchResponseSuccess(value) {
    const index = this.expectAsynchResponse();
    process.nextTick(() => {
      this.asynchResponseSuccess(index, value);
    });
  }
  
  expectAsynchResponseError(message) {
    const index = this.expectAsynchResponse();
    process.nextTick(() => {
      this.asynchResponseError(message, index);
    });
  }
  
  expectAsynchResponseTemp() {
    this.responseQueue.add();
  }
  
  expectAsynchResponse() {
    this.responseQueue.add();
    return this.index++;
  }
  
  unExpectAsynchResponseTemp() {
    this.responseQueue.remove();
  }
  
  asynchResponseSuccess(index, value) {
    this.responseQueue.done({result: true, value: value, index: index});
  }
  
  asynchResponseError(message, index) {
    this.responseQueue.done({result: false, message: message, index: index});
  }
  
  addResponseCb(cb) {
    this.responseCbs.push(cb);
  }
  
  handleRequest(requestData, cb, cbMessage) {
    this.responseQueue = new ResponseQueue(this.callback.bind(this), cb, this.constructor.name);
    super.handleRequest(requestData, cb, cbMessage);
  }
    
  // TODO: add the externalCb parameter???
  callback(results) {
    this.responseCbs.forEach((cb) => {
      cb();
    });
    const responseResult = results.responses.every((response) => {
      return response.result;
    });
    const responseValues = results.responses.filter((response) => {
      return undefined !== response.value;
    });
    if(responseResult) {
      if(0 === responseValues.length) {
        return this.responsePartSuccess();
      }
      else if(1 === responseValues.length) {
        return this.responsePartSuccess(responseValues[0].value);
      }
      else {
        responseValues.sort((a, b) => {
          if(a.index > b.index) {
            return 1;
          }
          else if(a.index < b.index) {
            return -1;
          }
          return 0;
        });
        const responseValueValues = responseValues.map((item) => {
          return item.value;
        });
        return this.responsePartSuccess(responseValueValues);
      }
    }
    else {
      results.responses.sort((a, b) => {
        if(a.index > b.index) {
          return 1;
        }
        else if(a.index < b.index) {
          return -1;
        }
        return 0;
      });
      const messages = results.responses.map((response) => {
        return response.message;
      });
      return this.responsePartError(messages);
    }
  }
}


module.exports = PluginBaseMulti;
