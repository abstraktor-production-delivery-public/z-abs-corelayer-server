
'use strict';

const Session = require('./session');


class SessionCache {
  constructor() {
    this.cache = new Map();
  }
  
  createSession(sessionId, connectionData) {
    const session = new Session(sessionId, connectionData);
    this.cache.set(sessionId, session);
    return session;
  }
  
  deleteSession(sessionId) {
    this.cache.delete(sessionId);
  }
  
  getSession(sessionId) {
    return this.cache.get(sessionId);
  }
  
  setSessionData(sessionId, name, data) {
    const session = this.getSession(sessionId);
    session.sessionData.set(name, data);
    return data;
  }
  
  getSessionData(sessionId, name) {
    const session = this.getSession(sessionId);
    return session ? session.sessionData.get(name) : undefined;
  }
}


module.exports = SessionCache;
