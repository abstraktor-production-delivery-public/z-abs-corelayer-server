
'use strict';


class PluginLock {
  constructor() {
    this.lockedReads = new Map();
    this.lockedWrites = new Map();
    this.queuedReads = new Map();
    this.queuedWrites = new Map();
  }
  
  readLock(name, cb) {
    if(!this._emptyLockedWrite(name)) {
      this._addQueuedRead(name, cb);
    }
    else if(!this._emptyQueuedWrite(name)) {
      this._addQueuedRead(name, cb);
    }
    else {
      this._addLockedRead(name);
      process.nextTick(() => {
        cb(() => {
          this._readLockDone(name);
        });
      });
    }
  }
  
  writeLock(name, cb) {
    if(!this._emptyLockedWrite(name)) {
      this._addQueuedWrite(name, cb);
    }
    else if(!this._emptyLockedRead(name)) {
      this._addQueuedWrite(name, cb);
    }
    else {
      this._addLockedWrite(name);
      process.nextTick(() => {
        cb(() => {
          this._writeLockDone(name);
        });
      });
    }
  }
  
  _writeLockDone(name) {
    this._removeLockedWrite(name);
    if(!this._emptyQueuedWrite(name)) {
      this.writeLock(name, this._removeQueuedWrite(name));
    }
    else {
      while(!this._emptyQueuedRead(name)) {
        this.readLock(name, this._removeQueuedRead(name));
      }
    }
  }
  
  _readLockDone(name) {
    this._removeLockedRead(name);
    if(this._emptyLockedRead(name)) {
      if(!this._emptyQueuedWrite(name)) {
        this.writeLock(name, this._removeQueuedWrite(name));
      }
    }
  }
  
  _addLocked(name, map) {
    if(map.has(name)) {
      const q = map.get(name);
      ++q.size;
    }
    else {
      map.set(name, {
        size: 1
      });
    }
  }
  
  _removeLocked(name, map) {
    const q = map.get(name);
    if(0 === --q.size) {
      map.delete(name);
    }
  }
  
  _addQueued(name, cb, map) {
    if(map.has(name)) {
      const q = map.get(name);
      q.cbs.push(cb);
      ++q.size;
    }
    else {
      map.set(name, {
        cbs: [cb],
        size: 1
      });
    }
  }
  
  _removeQueued(name, map) {
    const q = map.get(name);
    if(0 === --q.size) {
      map.delete(name);
    }
    return q.cbs.splice(0, 1)[0];
  }
  
  _empty(name, map) {
    const q = map.get(name);
    return undefined === q;
  }
  
  _addLockedRead(name) {
    this._addLocked(name, this.lockedReads);
  }
  
  _removeLockedRead(name) {
    this._removeLocked(name, this.lockedReads);
  }
  
  _emptyLockedRead(name) {
    return this._empty(name, this.lockedReads);
  }
  
  _addLockedWrite(name) {
    this._addLocked(name, this.lockedWrites);
  }
  
  _removeLockedWrite(name) {
    this._removeLocked(name, this.lockedWrites);
  }
  
  _emptyLockedWrite(name) {
    return this._empty(name, this.lockedWrites);
  }
  
  _addQueuedRead(name, cb) {
    this._addQueued(name, cb, this.queuedReads);
  }
  
  _removeQueuedRead(name) {
    return this._removeQueued(name, this.queuedReads);
  }
  
  _emptyQueuedRead(name) {
    return this._empty(name, this.queuedReads);
  }

  _addQueuedWrite(name, cb) {
    this._addQueued(name, cb, this.queuedWrites);
  }
  
  _removeQueuedWrite(name) {
    return this._removeQueued(name, this.queuedWrites);
  }
  
  _emptyQueuedWrite(name) {
    return this._empty(name, this.queuedWrites);
  }
}

module.exports = new PluginLock();
