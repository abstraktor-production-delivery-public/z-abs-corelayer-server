
'use strict';

const PluginLock = require('./plugin-lock');
const ActionResponseError = require('z-abs-corelayer-cs/clientServer/communication/action-response-error');
const ActionResponseSuccess = require('z-abs-corelayer-cs/clientServer/communication/action-response-success');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');
const ChildProcess = require('child_process');


class PluginBase {
  static instance = 0;
  static ADD = 'Add';
  static DELETE = 'Delete';
  static GET = 'Get';
  static UPDATE = 'Update';
  
  constructor(restType, ...authorization) {
    this.dataName = this.constructor.name;
    this.restType = restType;
    this.authorization = authorization;
    this.requestData = null;
    this.cb = null;
    this.cbMessage = null;
    this.dataResponse = null;
    this.tokenIn = null;
    this.tokenOut = null;
    this._id = ++PluginBase.instance;
  }
  
  initData(dataResponse) {
    this.dataResponse = dataResponse;
  }
  
  readLock(name, cb) {
    PluginLock.readLock(name, cb);
  }
  
  writeLock(name, cb) {
    PluginLock.writeLock(name, cb);
  }
    
  setTokenIn(token) {
    this.tokenIn = token;
  }
  
  setTokenOut(token) {
    this.tokenOut = token;
  }
  
  responsePartSuccess(data) {
    if(this.cb) {
      this.cb(new ActionResponseSuccess(this.dataName, this.requestData.index, data));
      this.cb = null;
    }
    else {
      ddb.warning(`responsePartSuccess[${this._id}]:`, this.constructor.name, this.cb);
    }
  }
  
  responsePartError(msg) {
    if(this.cb) {
      this.cb(new ActionResponseError(this.dataName,this.requestData.index, msg));
      this.cb = null;
    }
    else {
      ddb.warning(`responsePartSuccess[${this._id}]:`, this.constructor.name, this.cb);
    }
  }
  
  forkWorkerProcess(name, pluginPath, debug, execArgv, done) {
    this.dataResponse.forkWorkerProcess(name, this.requestData.sessionId, pluginPath, debug, execArgv, (pid, port) => {
      try {
        if(done) {
          done(pid, port);
        }
      }
      catch(err) {
        ddb.error('Exception in forkWorkerProcess callback: ', err, err.stack);
      }
    });
  }
  
  killWorkerProcess(done) {
    this.dataResponse.killWorkerProcess(this.requestData.sessionId, (code) => {
      try {
        if(done) {
          done(code);
        }
      }
      catch(err) {
        ddb.error('Exception in killWorkerProcess callback: ', err, err.stack);
      }
    });
  }
  
  sendRequestToWorkerProcess(request, cb) {
    const session = this.dataResponse.sessionCache.getSession(this.requestData.sessionId);
    const id = GuidGenerator.create();
    request.setIds(id, this.requestData.sessionId);
    this.dataResponse.sendRequestToWorkerProcess(session, id, request, cb, this.cbMessage);
  }
  
  sendMessageToWorkerProcess(msg) {
    const session = this.dataResponse.sessionCache.getSession(this.requestData.sessionId);
    this.dataResponse.sendMessageToWorkerProcess(session, msg);
  }
  
  getNode() {
    return this.dataResponse.getNode();
  }
  
  getService(name) {
    return this.dataResponse.getService(name);
  } 
  
  getSession() {
    return this.dataResponse.sessionCache.getSession(this.requestData.sessionId);
  }
  
  setSessionData(name, data) {
    this.dataResponse.sessionCache.setSessionData(this.requestData.sessionId, name, data);
    return data;
  }
  
  getSessionData(name) {
    return this.dataResponse.sessionCache.getSessionData(this.requestData.sessionId, name);
  }
  
  clearSession(done) {
    this.dataResponse.clearSession(this.requestData.sessionId, done);
  }
  
  sendMessage(msg, dataBuffers, debug) {
    this.cbMessage(msg, dataBuffers, debug);
  }
  
  handleRequest(requestData, cb, cbMessage) {
    if(this.cb) {
      // Make an error callback
      ddb.error(`handleRequest[${this._id}]:`, this.constructor.name, this.cb);
    }
    this.requestData = requestData;
    this.cb = cb;
    this.cbMessage = cbMessage;
    try {
      if(!this.onRequest) {
        return ddb.warning(`${this.constructor.name}.onRequest is not implemented.`);
      }
      this.onRequest(...requestData.params);
    }
    catch(err) {
      ddb.error(`Error in ${this.dataName}.onRequest()`, err, err.stack);
      return this.responsePartError(`${this.dataName}.onRequest exception: ${err}`);
    }
  }
  
  handleMessageFromClient(msg) {
    if(this.onMessageFromClient && typeof this.onMessageFromClient === 'function') {
      try {
        this.onMessageFromClient(msg);
      }
      catch(err) {
        ddb.error(`Error in ${this.dataName}.onMessageFromClient()`, err, err.stack);
      }
      return true;
    }
    return false;
  }
  
  handleMessageToClient(msg, dataBuffers, cbMessage) {
    if(this.onMessageToClient && typeof this.onMessageToClient === 'function') {
      try {
        this.onMessageToClient(msg, dataBuffers, cbMessage);
      }
      catch(err) {
        ddb.error(`Error in ${this.dataName}.onMessageToClient()`, err, err.stack);
      }
      return true;
    }
    return false;
  }
  
  handleMessageFromWorker(msg, dataBuffers, debug, cbMessage) {
    if(this.onMessageFromWorker && typeof this.onMessageFromWorker === 'function') {
      try {
        this.onMessageFromWorker(msg, dataBuffers, debug, cbMessage);
      }
      catch(err) {
        ddb.error(`Error in ${this.dataName}.onMessageFromWorker()`, err, err.stack);
      }
      return true;
    }
    else {
      return false;
    }
  }

  handleMessageFromParent(msg, cbMessage) {
    if(msg && this.onMessageFromParent && typeof this.onMessageFromParent === 'function') {
      try {
        this.onMessageFromParent(msg, cbMessage);
      }
      catch(err) {
        ddb.error(`Error in ${this.dataName}.onMessageFromParent()`, err, err.stack);
      }
      return true;
    }
    else {
      return false;
    }
  }
  
  handleClose(done) {
    if(this.onClose && typeof this.onClose === 'function') {
      try {
        this.onClose((e) => {
          done(e);
        });
      }
      catch(err) {
        ddb.error(`Error in ${this.dataName}.onClose()`, err, err.stack);
        done(err);
      }
    }
    else {
      done();
    }
  }
}


module.exports = PluginBase;
